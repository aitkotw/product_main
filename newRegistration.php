<?php
//TODO : need to impliment redirect and link expire with entry deletion
include 'app/constants.php';

//When form is submitted by user after filling details
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
   // var_dump($_POST); die();
   $name = clean($_POST['name']);
   $phone = clean($_POST['phone']);
   $dob = clean($_POST['dob']);
   $gstno = clean($_POST['gstno']);
   $email = clean($_POST['email']);
   $add = clean($_POST['add']);
   $state = clean($_POST['state']);
   $city = clean($_POST['city']);
   $zip = clean($_POST['zip']);
   $tandc = clean($_POST['checkbox']);

   if($tandc == 'true'){
      $regNo = clean($_POST['regno']);

      //Make Connection Start ==================================================
      $servername = NEW_CUSTOMER_REGISTRATION_HOST;
      $username = NEW_CUSTOMER_REGISTRATION_USERNAME;
      $password = NEW_CUSTOMER_REGISTRATION_PASSWORD;

      try {
         $conn = new PDO("mysql:host=$servername;dbname=".NEW_CUSTOMER_REGISTRATION_DATABASE, $username, $password);
         // set the PDO error mode to exception
         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         //echo "Connected successfully";
         if(empty($dob)){

            if(empty($zip)){
               $sql = "INSERT INTO `customer` (`name`, `phone`, `email`, `dob`, `gstno`, `address`, `city`, `state`, `zip`, `terms`) VALUES ('$name', '$phone', '$email', NULL, '$gstno', '$add', '$city', '$state', NULL, 1);";
            }else{
               $sql = "INSERT INTO `customer` (`name`, `phone`, `email`, `dob`, `gstno`, `address`, `city`, `state`, `zip`, `terms`) VALUES ('$name', '$phone', '$email', NULL, '$gstno', '$add', '$city', '$state', '$zip', 1);";
            }

         } else {

            if(empty($zip)){
               $sql = "INSERT INTO `customer` (`name`, `phone`, `email`, `dob`, `gstno`, `address`, `city`, `state`, `zip`, `terms`) VALUES ('$name', '$phone', '$email', '$dob', '$gstno', '$add', '$city', '$state', NULL, 1);";
            } else {
               $sql = "INSERT INTO `customer` (`name`, `phone`, `email`, `dob`, `gstno`, `address`, `city`, `state`, `zip`, `terms`) VALUES ('$name', '$phone', '$email', '$dob', '$gstno', '$add', '$city', '$state', '$zip', 1);";
            }

         }
         
         $conn->exec($sql); 
         
         //TODO: Disable url link
         $sql = "UPDATE `custRegistration` SET `active` = '0' WHERE `custRegistration`.`regno` = '$regNo';";
         $conn->exec($sql); 
         
         header('Location: newRegistration.php?mode=success');

         }
      catch(PDOException $e)
         {
         echo "Connection failed: " . $e->getMessage();
         //header('Location: newRegistration.php?mode=fail');
         }
      //Connection Ended ============================================================

   } else {
      header('Location: 404/');
   }

} else if (isset($_GET['ref']) && !empty($_GET['ref'])) {
    
    $refVal = clean($_GET['ref']);

    //Make Connection Start ==================================================
    $servername = NEW_CUSTOMER_REGISTRATION_HOST;
    $username = NEW_CUSTOMER_REGISTRATION_USERNAME;
    $password = NEW_CUSTOMER_REGISTRATION_PASSWORD;
    //declaring regNo url and value initialization 
    $regNo = 0;

    try {
            $conn = new PDO("mysql:host=$servername;dbname=".NEW_CUSTOMER_REGISTRATION_DATABASE, $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Connected successfully";
            $stmt = $conn->prepare("SELECT * FROM `custRegistration` WHERE `cryptedValue` = '$refVal'");
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $regNo = $result['regno'];
            $linkStatus = $result['active'];
            $timeStamp = $result['timestamp'];
            $currTime = time();

            $diff = $currTime - $timeStamp;

            if ($diff >= 86400 || $linkStatus == 0) {
               //TODO: Link not valid page
               header('Location: 404/'); 
            } 
       }
    catch(PDOException $e)
            {
               echo "Connection failed: " . $e->getMessage();
            }
  //Connection Ended ============================================================
  
}
//It is for customer messages like success entry or URL error
else if(isset($_GET['mode'])){

   //Do Nothing

}
//where there is no parameter attached to URL
else {
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 Not Found</h1>";
    echo "The page that you have requested could not be found.";
    exit();
}


function clean($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
?>
<!DOCTYPE html>
<html>
<head>
<title>New Registration</title>
 <!-- Latest compiled and minified CSS -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style> 
        .paneladd{
            background-color: #007bff;
            color: #fff;
            margin-top: 30px;
            padding: 10px;
        }

        /* Customize the label (the container) */
         .chkContainer {
         display: block;
         position: relative;
         padding-left: 35px;
         margin-bottom: 12px;
         cursor: pointer;
         font-size: 18px;
         -webkit-user-select: none;
         -moz-user-select: none;
         -ms-user-select: none;
         user-select: none;
         }

         /* Hide the browser's default checkbox */
         .chkContainer input {
         position: absolute;
         opacity: 0;
         cursor: pointer;
         height: 0;
         width: 0;
         }

         /* Create a custom checkbox */
         .checkmark {
         position: absolute;
         top: 0;
         left: 0;
         height: 25px;
         width: 25px;
         background-color: #eee;
         }

         /* On mouse-over, add a grey background color */
         .chkContainer:hover input ~ .checkmark {
         background-color: #ccc;
         }

         /* When the checkbox is checked, add a blue background */
         .chkContainer input:checked ~ .checkmark {
         background-color: #2196F3;
         }

         /* Create the checkmark/indicator (hidden when not checked) */
         .checkmark:after {
         content: "";
         position: absolute;
         display: none;
         }

         /* Show the checkmark when checked */
         .chkContainer input:checked ~ .checkmark:after {
         display: block;
         }

         /* Style the checkmark/indicator */
         .chkContainer .checkmark:after {
         left: 9px;
         top: 5px;
         width: 5px;
         height: 10px;
         border: solid white;
         border-width: 0 3px 3px 0;
         -webkit-transform: rotate(45deg);
         -ms-transform: rotate(45deg);
         transform: rotate(45deg);
         }

         @media only screen and (min-width: 576px) {

            #tandcBtn{
               margin-left : -35px;
            }

         }

    </style>
</head>

<body>

<div class="row">
   <div class="container">
      <div class="col-sm-12 shadow-lg">

         <div class="row paneladd">
            <h2 class="mx-auto"><?php echo COMPANY_NAME; ?></h2>
         </div>   
      
         <div class="row">
            <div class="container px-3 pt-5 pb-3">
               <form name="custRegistration" method="post" action="" onsubmit="if(document.getElementById('checkbox').checked) { return true; } else { alert('please select checkbox to agree to our T&C'); return false; }">
               
                  <?php
                  if (isset($_GET['mode'])) {

                     if($_GET['mode'] == 'success'){?>
                        <div class="alert container d-block alert-success" id="error_div">
                           Registration Completed Successfully!!
                        </div>
                     <?php
                     }else if($_GET['mode'] == 'fail'){?>
                        <div class="alert container d-block alert-danger" id="error_div">
                           Something Went Wrong!!
                        </div>
                     <?php
                     } 
                  } ?>

                  <div class="form-group form-row">
                     <div class="col-sm-6">
                        <label for="inputAddress">Enter Full Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                     </div>

                     <div class="col-sm-6">
                        <label for="inputAddress">Contact No.</label>
                        <input type="phone" class="form-control" name="phone" pattern=".{10,10}" title="Must contain 10 Numbers" required placeholder="Enter Phone No.">
                     </div>
                  </div>

                  <div class="form-group form-row">
                     <div class="col-sm-4">
                        <label for="inputAddress">Email Address</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter Email Address">
                     </div>

                     <div class="col-sm-4">
                        <label for="gst">Date of Birth</label>
                        <input type="date" class="form-control" name="dob" placeholder="Enter GST No.">
                     </div>

                     <div class="col-sm-4">
                        <label for="gst">GSTIN No.</label>
                        <input type="text" class="form-control" name="gstno" placeholder="Enter GST No.">
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="inputAddress">Address</label>
                     <input type="text" class="form-control" name="add" required title = "Address is requied" placeholder="1234 Main St">
                  </div>

                  <div class="form-group form-row">
                     <div class="col">
                        <input type="text" class="form-control" name="city" placeholder="City">
                     </div>

                     <div class="col">
                        <select class="form-control" name = "state" id="sel1">
                           <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                           <option value="Andhra Pradesh">Andhra Pradesh</option>
                           <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                           <option value="Assam">Assam</option>
                           <option value="Bihar">Bihar</option>
                           <option value="Chandigarh">Chandigarh</option>
                           <option value="Chhattisgarh">Chhattisgarh</option>
                           <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                           <option value="Daman and Diu">Daman and Diu</option>
                           <option value="Delhi">Delhi</option>
                           <option value="Goa">Goa</option>
                           <option value="Gujarat">Gujarat</option>
                           <option value="Haryana">Haryana</option>
                           <option value="Himachal Pradesh">Himachal Pradesh</option>
                           <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                           <option value="Jharkhand">Jharkhand</option>
                           <option value="Karnataka">Karnataka</option>
                           <option value="Kerala">Kerala</option>
                           <option value="Lakshadweep">Lakshadweep</option>
                           <option value="Madhya Pradesh">Madhya Pradesh</option>
                           <option value="Maharashtra" selected= "selected">Maharashtra</option>
                           <option value="Manipur">Manipur</option>
                           <option value="Meghalaya">Meghalaya</option>
                           <option value="Mizoram">Mizoram</option>
                           <option value="Nagaland">Nagaland</option>
                           <option value="Orissa">Orissa</option>
                           <option value="Pondicherry">Pondicherry</option>
                           <option value="Punjab">Punjab</option>
                           <option value="Rajasthan">Rajasthan</option>
                           <option value="Sikkim">Sikkim</option>
                           <option value="Tamil Nadu">Tamil Nadu</option>
                           <option value="Tripura">Tripura</option>
                           <option value="Uttaranchal">Uttaranchal</option>
                           <option value="Uttar Pradesh">Uttar Pradesh</option>
                           <option value="West Bengal">West Bengal</option>
                        </select>
                     </div>

                     <div class="col">
                        <input type="text" class="form-control" placeholder="Zip Code" pattern=".{6,6}" name="zip" title="Please Enter Valid zip code">
                     </div>
                  </div>

                  <div class="form-group mt-4">
                     <div class="row">
                        <div class="col-sm-5">
                           <label class="chkContainer">Check here to indicate you've read and agree to 
                              <input type="checkbox" id = "checkbox" name = "checkbox" value = "true">
                              <span class="checkmark"></span>
                           </label>
                        </div>

                        <div class="col-sm-2">
                           <button type="button" class="btn btn-link" id = "tandcBtn"  data-toggle="modal" data-target="#tandcModel" style = "padding:0px;">Terms and Condition</button> 
                        </div>
                     </div>   
                  </div>

                  <!-- The Modal -->
                  <div class="modal" id="tandcModel">
                     <div class="modal-dialog" style = "max-width: 900px;">
                        <div class="modal-content">
                           <!-- Modal Header -->
                           <div class="modal-header">
                           <h4 class="modal-title">Terms and Conditions</h4>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>

                           <!-- Modal body -->
                           <div class="modal-body">
                              <div class="list-group">
                                 <a href="#" class="list-group-item list-group-item-action ">We will contact you though means of email, phone or text messages to deliever certain services that you have requested.</a>   
                              </div> 
                           </div>

                           <!-- Modal footer -->
                           <div class="modal-footer"></div>
                        </div>
                     </div>
                  </div>

                  <?php
                     if (isset($_GET['ref']) && !empty($_GET['ref'])){
                        echo '<input type="hidden" name="regno" value="'.$regNo.'">';
                     }
                  ?>

                  <div class="form-group d-flex justify-content-center">
                     <button type="submit" class="btn btn-primary col-sm-4 btn-block">Submit </button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

</div>
</body>
</html>