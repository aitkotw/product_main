<?php 
  class Database {
    // DB Params
    private $host = 'localhost';
    private $db_name = MAIN_DATABASE_NAME;
    private $username = MAIN_DATABASE_USERNAME;
    private $password = MAIN_DATABASE_PASSWORD;
    private $conn;

    // DB Connect
    public function connect() {
      $this->conn = null;

      try { 
        $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e) {
        //echo 'Connection Error: ' . $e->getMessage();
        header("content-type: application/json");
        $errorMsg = json_encode(['error' => ['status'=>CONNECTION_ERROR_DATABASE, 'message'=>"Connection Error"]]);
        echo $errorMsg; exit;
      }

      return $this->conn;
    }
  }
?>