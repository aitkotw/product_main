<?php 
  class DatabaseCust {
    // DB Params
    private $host = DATABASE_HOST_NAME;
    private $db_name = CUST_DATABASE_NAME;
    private $username = CUST_DATABASE_USERNAME;
    private $password = CUST_DATABASE_PASSWORD;
    private $conn;

    // DB Connect
    public function connect() {
      $this->conn = null;

      try { 
        $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e) {
        //echo 'Connection Error: ' . $e->getMessage();
        header("content-type: application/json");
        $errorMsg = json_encode(['error' => ['status'=>CONNECTION_ERROR_DATABASE, 'message'=>"Connection Error"]]);
        echo $errorMsg; exit;
      }

      return $this->conn;
    }
  } 
?>