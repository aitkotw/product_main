<?php
include '../res/rest.php';

class Product_API extends Rest{

    protected $name;
    protected $amount;
    protected $spec;
    protected $desc;

    public function getAllProduct(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM product"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getAllProductArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getAllProductArray[$row['prod_id']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getAllProductArray);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function addNewProduct(){
        $name = $this->param['name'];
        $amount = $this->param['amount'];
        $spec = $this->param['spec'];
        $desc = $this->param['desc'];
        $hsn_no = $this->param['hsn_no'];
        $type = $this->param['type'];
        $gstrate = $this->param['gstrate'];

        if($this->checkProdNameExixts() == true){
            $this->returnResponse(SUCCESS_RESPONSE, 'exist');
        } else{

            try {
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $query = "INSERT INTO `product` (`prod_id`, `prod_name`, `prod_type`, `gstrate`, `hsn_no`, `spec`, `prod_desc`, `price`, `prod_img`) VALUES (NULL, '$name', '$type', '$gstrate', '$hsn_no', '$spec', '$desc', '$amount', NULL);"; 
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    $this->returnResponse(SUCCESS_RESPONSE, 'true');
                } else{
                    $this->returnResponse(SUCCESS_RESPONSE, 'false');
                }
            } catch (\Throwable $th) {
                //throw $th;
                $this->throwError(SOMETHING_WENT_WRONG, 'Unable to add product');
              
            }

        }

    }

    public function checkProdNameExixts(){
        $name = $this->param['name'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT `prod_name` FROM `product` WHERE `prod_name` = '$name';"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return true;
            } else{
                return false;
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to get product name');
        }

    }

    public function deleteProductByID(){
        $prodID = $this->param['prodID']; 
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "DELETE FROM `product` WHERE `product`.`prod_id` = $prodID"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $this->returnResponse(SUCCESS_RESPONSE, "true");
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'false');
        }
    }

    public function addNewVendor(){
        $name = $this->clean($this->param['name']);
        $phone = $this->clean($this->param['phone']);
        $gst = $this->clean($this->param['gst']);
        $address = $this->clean($this->param['address']);

        if($this->checkVendorNameExists() == true){
            $this->returnResponse(SUCCESS_RESPONSE, 'exist');
        } else{

            try {
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $query = "INSERT INTO `vendor` (`vid`, `vname`, `vphone`, `vgst`, `vaddress`) VALUES (NULL, '$name', '$phone', '$gst', '$address');"; 
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    $this->returnResponse(SUCCESS_RESPONSE, 'true');
                } else{
                    $this->returnResponse(SUCCESS_RESPONSE, 'false');
                }
            } catch (\Throwable $th) {
                //throw $th;
                $this->throwError(SOMETHING_WENT_WRONG, 'SomethingWentWrong');
            }

        }

        
    }

    private function checkVendorNameExists(){
        $name = $this->param['name'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT `vname` FROM `vendor` WHERE `vname` = '$name';"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return true;
            } else{
                return false;
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to get product name');
        }

    }

    public function getAllVendor(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM vendor"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getAllVendorArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getAllVendorArray[$row['vid']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getAllVendorArray);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

}
