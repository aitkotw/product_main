<?php
include '../res/rest.php';

class Customer_API extends Rest{

    public function getCustDetail(){
        $cardno = $this->clean($this->param["cardno"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `cust` WHERE `cardno` = ?"; 
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $cardno);
            $stmt->execute();
            $custDetailArray = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->returnResponse(SUCCESS_RESPONSE, $custDetailArray);
            
        } catch (\Throwable $th) {
            //throw $th;
            //$this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get customer Detail');
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get customer Detail');
        }

    }

    public function getAddressBook(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `cust`"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $addressBookArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $addressBookArray[$row['cardno']] = $row;
            }
            
            //print_r($addressBookArray);
            $this->returnResponse(SUCCESS_RESPONSE, $addressBookArray);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get All customer Detail');
        }
    }

    public function getAddressBookCount(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `cust`"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            // $addressBookArray = array();
            // while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //     $addressBookArray[$row['cardno']] = $row;
            // }
            $count = $stmt->rowCount();
            //print_r($addressBookArray);
            $this->returnResponse(SUCCESS_RESPONSE, $count);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Total No. customer Detail');
        }
    }

    public function AddNewCustomer(){

            $cardno = $this->clean($this->param["cardno"]);
            $name = $this->clean($this->param["name"]);
            $email = $this->clean($this->param["email"]);
            $dob = $this->clean($this->param["dob"]);
            $phone = $this->clean($this->param["phone"]);
            $gstno = $this->clean($this->param["gstno"]);
            $address = $this->clean($this->param["add"]);
            $city = $this->clean($this->param["city"]);
            $state = $this->clean($this->param["state"]);
            $zip = $this->clean($this->param["zip"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `cust` (`cardno`, `name`, `dob`, `phone`, `email`, `gstno`, `address`, `city`, `state`, `zipcode`) VALUES ('$cardno', '$name', '$dob', '$phone', '$email', '$gstno', '$address', '$city', '$state', '$zip');";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $this->returnResponse(SUCCESS_RESPONSE, "true");

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, $th);
        }
    }

    public function cardNoExists(){
        $cardno = $this->clean($this->param["cardno"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust WHERE `cardno` = '$cardno'";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
            
                $this->returnResponse(SUCCESS_RESPONSE, "true");

            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
            }


        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getMaxCardNo(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT MAX(cardno)FROM cust";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $order_grpNo = $stmt->fetchColumn();
                $this->returnResponse(SUCCESS_RESPONSE, $order_grpNo);
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
                exit;
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //Only accessible within class
    private function maxCardNo(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT MAX(cardno)FROM cust";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $cardno = $stmt->fetchColumn();
                return $cardno;
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //Overview for pending review page
    public function newCustRegistered(){
        try {
            $this->connCust->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `customer`"; 
            $stmt = $this->connCust->prepare($query);
            $stmt->execute();
            $newCustDtl = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $newCustDtl[$row['regno']] = $row;
            }
            
            $this->returnResponse(SUCCESS_RESPONSE, $newCustDtl);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get New Cust Detail '.$th);
        }
    }

    public function delNewCustRegistered(){
        $regNo = $this->clean($this->param["regno"]);
        try {
            $this->connCust->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "DELETE FROM `customer` WHERE `customer`.`regno` = $regNo"; 
            $stmt = $this->connCust->prepare($query);
            $stmt->execute();
            $this->returnResponse(SUCCESS_RESPONSE, "true");
        } catch (\Throwable $th) {
            //throw $th;
            $this->returnResponse(SUCCESS_RESPONSE, "false");
        }
    }

    public function addRegisteredCustomer(){
        $regNo = $this->clean($this->param["regno"]);
        $cardnoMax = $this->maxCardNo();
        $cardno = $cardnoMax + 1;

        try {

            $this->connCust->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `customer` WHERE `regno` = $regNo"; 
            $stmt = $this->connCust->prepare($query);
            $stmt->execute();
            $custRegistrationDtl = $stmt->fetch(PDO::FETCH_ASSOC);

            $name = $this->clean($custRegistrationDtl["name"]);
            $email = $this->clean($custRegistrationDtl["email"]);
            $phone = $this->clean($custRegistrationDtl["phone"]);
            $gstno = $this->clean($custRegistrationDtl["gstno"]);
            $dob = $this->clean($custRegistrationDtl["dob"]);
            $address = $this->clean($custRegistrationDtl["address"]);
            $city = $this->clean($custRegistrationDtl["city"]);
            $state = $this->clean($custRegistrationDtl["state"]);
            $zip = $this->clean($custRegistrationDtl["zip"]);

            try {
                
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $query = "INSERT INTO `cust` (`cardno`, `name`, `dob`, `phone`, `email`, `gstno`, `address`, `city`, `state`, `zipcode`) VALUES ('$cardno', '$name', '$dob', '$phone', '$email', '$gstno', '$address', '$city', '$state', '$zip');";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();

                $my_cust_Dtl = array();
                $my_cust_Dtl[] = $cardno;
                $my_cust_Dtl[] = $name;
                
                $this->returnResponse(SUCCESS_RESPONSE, $my_cust_Dtl);
                
            } catch (\Throwable $th) {
                //throw $th;
                $this->returnResponse(SUCCESS_RESPONSE, "false");
            }

            
        } catch (\Throwable $th) {
            //throw $th;
            $this->returnResponse(SUCCESS_RESPONSE, "false");
        }

    }

    public function deleteCustomerWithCardNo(){
        $cardno = $this->clean($this->param["cardno"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod WHERE cardno = '$cardno'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() < 1) { 

                try {
                    $query = "DELETE FROM `cust` WHERE `cust`.`cardno` = $cardno"; 
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute();
                    if($stmt->rowCount() > 0){
                        $this->returnResponse(SUCCESS_RESPONSE, "true");
                    } else{
                        $this->returnResponse(SUCCESS_RESPONSE, "false");
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                    $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot Delete customer ');
                }

            } else{
                $this->returnResponse(SUCCESS_RESPONSE, "false");
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot find customer ');
        }

    }

    public function updateCustomerDtl(){
        $cardno = $this->clean($this->param["cardno"]);
        $name = $this->clean($this->param["name"]);
        $dob = $this->clean($this->param["dob"]);
        $email = $this->clean($this->param["email"]);
        $phone = $this->clean($this->param["phone"]);
        $gstno = $this->clean($this->param["gstno"]);
        $address = $this->clean($this->param["address"]);
        $city = $this->clean($this->param["city"]);
        $state = $this->clean($this->param["state"]);
        $zip = $this->clean($this->param["zip"]);

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "UPDATE `cust` SET `name` = '$name', `dob` = '$dob', `phone` = '$phone', `email` = '$email', `gstno` = '$gstno', `address` = '$address', `city` = '$city', `state` = '$state', `zipcode` = '$zip' WHERE `cust`.`cardno` = $cardno;";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $this->returnResponse(SUCCESS_RESPONSE, "true");

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Customer Update fail '.$th);
        }

    }
    
}

?>