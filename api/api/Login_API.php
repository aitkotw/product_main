<?php
include '../res/rest.php';

    class Login_API extends Rest{

        protected $email;
        protected $password;
        

        public function userExist(){

            $email = $this->clean($this->param['email']);

            try {
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $query = "SELECT * FROM `user` WHERE `email` = ?"; 
                $stmt = $this->conn->prepare($query);
                $stmt->bindParam(1, $email);
                $stmt->execute();
                //$this->returnResponse(SUCCESS_RESPONSE, "Email Id Exists");
                if ($stmt->rowCount() > 0) {
                    return true;
                } else {
                    return false;
                }
                

            } catch (\Throwable $th) {
                throw $th;
                $this->throwError(USER_NOT_EXIST, "Email Does Not Exist");
                //return false;

            }
            $this->conn->close();
        }

        public function login_user(){
            $email = $this->clean($this->param['email']);
            $password = $this->clean($this->param['password']);

            try {
                if($this->userActivation() === true){

                    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $query = "SELECT * FROM `user` WHERE `email` = '$email'"; 
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute();
                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                    $passhash = $result["passhash"];
                    if (password_verify($password, $passhash)) {
                        $_SESSION['email'] = $email;
                        //$this->returnResponse(SUCCESS_RESPONSE, "User LogIn Success");
                        //Now Instansiate JWT Here
                        $paylod = [
                            'iat' => time(),
                            'iss' => 'localhost',
                            'exp' => time() + (60*60*24),
                            'email' => $email
                        ];
                        
                        $token = JWT::encode($paylod, SECRETE_KEY);
                        //echo $token;

                        if($this->storeBrowserDtl()){
                            $this->returnResponse(SUCCESS_RESPONSE, $token);
                        } else {
                            $this->throwError(INVALID_USER_PASS, "unable to store logs"); 
                        }
                        

                    } else{
                        $this->throwError(INVALID_USER_PASS, "User Password is Invalid");
                    }

                } 
                 else {
                    $this->throwError(INVALID_USER_PASS, "User is not Active");
                }
                
            } catch (\Throwable $th) {
                throw $th;
                $this->throwError(INVALID_USER_PASS, "Password Does Not Match");
            }

        }

        private function storeBrowserDtl(){
            $client_ip = $this->clean($this->param['client_ip']);
            $browser_dtl = $this->clean($this->param['browser_dtl']);

            $country = $city = "";

            if($client_ip !== 'UNKNOWN'){

                if($this->is_connected() === true){
                    $location = file_get_contents('http://api.ipstack.com/'.$client_ip.'?access_key=37e575ef08c6fb75c5295ba3e0176bc1');
                    $location_val = json_decode($location, true);
                    $country = $location_val['country_name'];
                    $city = $location_val['city'];
                } else {
                    return false;
                }        

            }
            

            try {
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $query = "INSERT INTO `logs` (`id`, `ip_address`, `browser`, `country`, `city`, `time`) VALUES (NULL, '$client_ip', '$browser_dtl', '$country', '$city', NOW());"; 
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    return true;
                } else {
                    return false;
                }

            } catch (\Throwable $th) {
                //throw $th;
                return false;
            }

        }

        public function userActivation(){
            $email = $this->clean($this->param['email']);
            try {
                $query = "SELECT * FROM `user` WHERE `email` = '$email' AND `status` = '1'"; 
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    return true;
                } else {
                    return false;
                }
                
            } catch (\Throwable $th) {
                //throw $th;
                //$this->throwError(INVALID_USER_PASS, "User status can not be checked");
            }
            
        }

    }


?>