<?php
include '../res/rest.php';

class FkBill_API extends Rest{

    public function getfk_AllOrder(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `fak_invoice` INNER JOIN cust ON fak_invoice.cardno = cust.cardno  ORDER BY `id` DESC"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getAllOrderArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getAllOrderArray[$row['id']] = $row;
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getAllOrderArray);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Order Detail');
        }
    }

    public function getIvcItemDtl(){
        $ivcID = $this->param['ivcID'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `fak_invoiceItem` LEFT JOIN product ON fak_invoiceItem.prodID = product.prod_id WHERE ivcID = $ivcID"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getIvcItem = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getIvcItem[$row['id']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getIvcItem);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to get Invoice Items');
        }
    }

    public function removePreviousOrder(){
        $ivcID = $this->param['ivcID'];

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "DELETE FROM `fak_invoiceItem` WHERE `ivcID` = '$ivcID'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            // if ($stmt->rowCount() > 0) {
            //     $this->returnResponse(SUCCESS_RESPONSE, 'true');
            // } else {
            //     $this->returnResponse(SUCCESS_RESPONSE, 'false');
            // }
            $this->returnResponse(SUCCESS_RESPONSE, 'true');
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to delete previous details');
        }
        
    }

    public function updateExisitngInvoice(){
        $discount = $this->clean($this->param["discount"]);
        $amount = $this->clean($this->param["amount"]);
        $ivcID = $this->clean($this->param["ivcID"]);

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "UPDATE `fak_invoice` SET `discount` = $discount, `ivcAmt` = '$amount' WHERE `id` = '$ivcID';";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $this->returnResponse(SUCCESS_RESPONSE, "true");
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to add Invoice');
        }

    }

    public function addNewInvoiceItems(){
        $ivcID = $this->clean($this->param["ivcID"]);
        $prodID = $this->clean($this->param["prodID"]);
        $prodName = $this->clean($this->param["prodName"]);
        $prodDesc = $this->clean($this->param["prodDesc"]);
        $qty = $this->clean($this->param["qty"]);
        $prodAmt = $this->clean($this->param["prodAmt"]);
        $gstRate = $this->clean($this->param["gstRate"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `fak_invoiceItem` (`id`, `ivcID`, `prodID`, `prodName`, `prodDesc`, `qty`, `prodAmt`, `gstRate`, `modItem`) VALUES (NULL, '$ivcID', '$prodID', '$prodName', '$prodDesc', '$qty', '$prodAmt', '$gstRate', '0');";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            // if ($stmt->rowCount() > 0) {
            //     if($this->addNew_fakInvoiceItems()){
            //         $this->returnResponse(SUCCESS_RESPONSE, "true");
            //     }
            // }
            if ($stmt->rowCount() > 0) {
                $this->returnResponse(SUCCESS_RESPONSE, "true");
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to add Invoice');
        }
    }


    public function getCustDetailForIVC(){
        $ivcID = $this->param['ivcID'];
        $cardno = $this->param['cardno'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `fak_invoice` INNER JOIN `cust` ON fak_invoice.cardno = cust.cardno WHERE fak_invoice.cardno = $cardno AND fak_invoice.id = $ivcID "; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $custBillDetail = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->returnResponse(SUCCESS_RESPONSE, $custBillDetail);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to Card Number Details');
        }

    }

    public function lockBillNo(){
        $ivcID = $this->param['ivcID'];

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "UPDATE `fak_invoice` SET `ivcStatus` = 1 WHERE `id` = $ivcID "; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $this->returnResponse(SUCCESS_RESPONSE, "true");
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to Card Number Details');
        }
    }

    public function getIvcDtl(){
        $ivcID = $this->clean($this->param["ivcID"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `fak_invoice` WHERE `id` = $ivcID"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getIvcDtl = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getIvcDtl[$row['id']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getIvcDtl);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to get Invoice Items');
        }
    }


}