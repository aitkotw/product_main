<?php
include '../res/rest.php';

class Vendor_API extends Rest{

    public function getManvoGRPNo(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT MAX(vogrpNo)FROM vendorOrder"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $vOrder_grpNo = $stmt->fetchColumn();
                $this->returnResponse(SUCCESS_RESPONSE, $vOrder_grpNo);
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
                exit;
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get vGroup Number');
            exit;
        }
    }

    public function addVendorOrder(){
        $vID = $this->clean($this->param['vID']);
        $vName = $this->clean($this->param['vName']);
        $productno = $this->clean($this->param['productno']);
        $prod_name = $this->clean($this->param['prod_name']);
        $qty = $this->clean($this->param['qty']);
        $vpdate = $this->clean($this->param['vpdate']);
        $groupno = $this->clean($this->param['groupno']);
        $amt = $this->clean($this->param['amt']);

        $vpdate = date('Y-m-d', strtotime($vpdate));
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `vendorOrder` (`voID`, `vogrpNo`, `vID`, `vName`, `prodID`, `prodName`, `voDate`, `qty`, `amt`) VALUES (NULL, '$groupno', '$vID', '$vName', '$productno', '$prod_name', '$vpdate', '$qty', '$amt');"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            
            if ($stmt->rowCount() > 0 && $this->addStockValue($productno, $qty) == true) {
                $this->returnResponse(SUCCESS_RESPONSE, 'true');
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, $th);
            exit;
        }
    }

    private function addStockValue($productno, $qty){
        $query = "UPDATE `product` SET `stock` = stock + $qty WHERE `product`.`prod_id` = $productno; ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return true;
    }

    public function getAllOrderVendor(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `vendorOrder` ORDER BY voID DESC"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getAllProductArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getAllProductArray[$row['voID']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getAllProductArray);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


}