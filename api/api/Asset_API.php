<?php
include '../res/rest.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../res/phpmailer/Exception.php';
require '../res/phpmailer/PHPMailer.php';
require '../res/phpmailer/SMTP.php';

class Asset_API extends Rest{
    

    protected $phone;
    protected $message;
    protected $emailto;
    protected $email_message;

    public function sendInvoiceMail(){
        $emailto = $this->param['emailto'];
        $email_message = $this->param['message'];

        try {
            //TODO : Mail setup for client
            $mail = new PHPMailer;
            $mail->isSMTP(); 
            $mail->SMTPDebug = 0; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages
            $mail->Host = "smtp.gmail.com"; // use $mail->Host = gethostbyname('smtp.gmail.com'); // if your network does not support SMTP over IPv6
            $mail->Port = 587; // TLS only
            $mail->SMTPSecure = 'tls'; // ssl is depracated
            $mail->SMTPAuth = true;
            $mail->Username = 'dplanethack@gmail.com';
            $mail->Password = 'abhishek1111';
            $mail->setFrom('akdron@tech.com', 'abhishek');
            $mail->addAddress($emailto, 'Abhishek');
            $mail->Subject = 'PHPMailer GMail SMTP test';
            $mail->msgHTML($email_message); //$mail->msgHTML(file_get_contents('contents.html'), __DIR__); //Read an HTML message body from an external file, convert referenced images to embedded,
            $mail->AltBody = 'HTML messaging not supported';
            // $mail->addAttachment('images/phpmailer_mini.png'); //Attach an image file

            if(!$mail->send()){
                //echo "Mailer Error: " . $mail->ErrorInfo;
                $this->returnResponse(SOMETHING_WENT_WRONG, 'Unable to send Mail');
            } else {
                $this->returnResponse(SOMETHING_WENT_WRONG, 'true');
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SUCCESS_RESPONSE, 'Unable to Send Email');
        }

    }

    public function sendRegistrationSMS(){
        $phone = $this->param['phone'];
        $mySaltKey = $this->genrateRandString(10);
        $newUnixTime = time();
        $urlVal = crypt($newUnixTime, $mySaltKey);
        try {
            $this->connCust->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `custRegistration` (`regno`, `cryptedValue`, `keySalt`, `timestamp`, `active`) VALUES (NULL,'$urlVal', '$mySaltKey', '$newUnixTime', '1');"; 
            $stmt = $this->connCust->prepare($query);
            $stmt->execute();

            $messageAttached = 'Please follow the link below to get registered '.ROOT_SERVER.'newRegistration.php?ref='.$urlVal;
            $registrationDtl = array();
            $registrationDtl[] = $phone;
            $registrationDtl[]  = $messageAttached;

            $this->returnResponse(SUCCESS_RESPONSE, $registrationDtl);
   
        } catch(\Throwable $th) {
            //throw $th;
            $this->throwError(SUCCESS_RESPONSE, 'false');            
        }

    }

    public function sendSMS(){
        $phone = $this->param['phone'];
        $message = $this->param['message'];
        try {

            	// Authorisation details.
                $username = "blabla@akdron.com";
                $hash = "3cfdd7629a742e1111ca0badfe945444554a3ff2d42a9cb0a8d5d5b9f7661063";

                // Config variables. Consult http://api.textlocal.in/docs for more info.
                $test = "0";

                // Data for text message. This is the text message data.
                $sender = "TXTLCL"; // This is who the message appears to be from.
                $numbers = '91'.$phone; // A single number or a comma-seperated list of numbers
                $message = $message;
                // 612 chars or less
                // A single number or a comma-seperated list of numbers
                $message = urlencode($message);
                $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
                $ch = curl_init('http://api.textlocal.in/send/?');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch); // This is the result from the API -------------------------------------IMPORTANT--------------------------
                curl_close($ch);
            
            $this->returnResponse(SUCCESS_RESPONSE, $result);//----------------------------------------------------IMPORTANT-------------------------
            //Temparory fix for SMS
            //$obj = array('status' => 'success');
            //$this->returnResponse(SUCCESS_RESPONSE, json_encode($obj));

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to send SMS');
        }
    }

}

?>