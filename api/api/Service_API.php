<?php
include '../res/rest.php';

class Service_API extends Rest{

    public function pendingService(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod INNER JOIN cust ON cust.cardno = cust_prod.cardno WHERE serv_date < NOW() ORDER BY serv_date ASC"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $pendingServiceArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $pendingServiceArray[$row['orderno']] = $row;
            }
            $this->returnResponse(SUCCESS_RESPONSE, $pendingServiceArray);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Service Detail '.$th);
        }

    }

    public function pendingRenewal(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod INNER JOIN cust ON cust.cardno = cust_prod.cardno WHERE renew_date < NOW() ORDER BY renew_date ASC "; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $pendingRenewalArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $pendingRenewalArray[$row['orderno']] = $row;
            }
            $this->returnResponse(SUCCESS_RESPONSE, $pendingRenewalArray);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Renewal Detail');
        }
        
    }

    public function pendingServiceRenewal(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod WHERE renew_date < NOW() AND serv_date < NOW()"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $pendingServiceRenewalArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $pendingServiceRenewalArray[$row['orderno']] = $row;
            }
            $this->returnResponse(SUCCESS_RESPONSE, $pendingServiceRenewalArray);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Renewal Detail');
        }
    }

    public function pendingServiceCount(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod WHERE serv_date < NOW()"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $count = $stmt->rowCount();
            $this->returnResponse(SUCCESS_RESPONSE, $count);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Total No. customer Detail');
        }
    }

    public function pendingRenewalCount(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod WHERE renew_date < NOW()"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $count = $stmt->rowCount();
            $this->returnResponse(SUCCESS_RESPONSE, $count);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Total No. customer Detail');
        }
    }

    public function pendingServiceRenewalCount(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod WHERE renew_date < NOW() AND serv_date < NOW()"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $count = $stmt->rowCount();
            $this->returnResponse(SUCCESS_RESPONSE, $count);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Total No. customer Detail');
        }
    }

    public function update_service(){
        $orderno = $this->clean($this->param['orderno']);
        $cardno = $this->clean($this->param['cardno']);
        $prod_id = $this->clean($this->param['prod_id']);
        $serv_date = $this->clean($this->param['serv_date']);
        $prod_name = $this->clean($this->param['prod_name']);
        $pdate = $this->clean($this->param['pdate']);
        $new_serv_date = date('Y-m-d', strtotime("+3 months", strtotime($serv_date)));
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "UPDATE `cust_prod` SET `serv_date` = '$new_serv_date' WHERE `cust_prod`.`orderno` = $orderno;"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {

                //Add updated service to service table
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $query = "INSERT INTO `services` (`servid`, `cardno`, `orderno`, `prod_name`, `serv_date`, `serv_done_date`) VALUES (NULL, '$cardno', '$orderno', '$prod_name', '$serv_date', now());"; 
                $stmt = $this->conn->prepare($query);
                $stmt->execute();


                $this->returnResponse(SUCCESS_RESPONSE, "true");
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to renew Service');
        }
    }

    public function serv_date_From_OrderNo(){
        $orderno = $this->clean($this->param['orderno']);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT `serv_date` FROM  `cust_prod` WHERE `orderno` = '$orderno'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                 $serv_date = $stmt->fetchColumn();
                 return $serv_date;
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
                exit;
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to get Service Date');
        }
    }

    public function update_renew(){
        $orderno = $this->clean($this->param['orderno']);
        $cardno = $this->clean($this->param['cardno']);
        $prod_id = $this->clean($this->param['prod_id']);
        $serv_date = $this->clean($this->param['serv_date']);
        $prod_name = $this->clean($this->param['prod_name']);
        $pdate = $this->clean($this->param['pdate']);
        $renew_date = $this->clean($this->param['renew_date']);
        $new_renew_date = date('Y-m-d', strtotime("+12 months", strtotime($renew_date)));
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "UPDATE `cust_prod` SET `renew_date` = '$new_renew_date' WHERE `cust_prod`.`orderno` = $orderno;"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {

                //Add updated renewal to renewal table
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $query = "INSERT INTO `renewals` (`renewid`, `cardno`, `orderno`, `prod_name`, `renew_date`, `renew_done_date`) VALUES (NULL, '$cardno', '$orderno', '$prod_name', '$renew_date', now());"; 
                $stmt = $this->conn->prepare($query);
                $stmt->execute();


                $this->returnResponse(SUCCESS_RESPONSE, "true");
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to update Renewal');
        }

    }

    public function renew_date_From_OrderNo(){
        $orderno = $this->clean($this->param['orderno']);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT `renew_date` FROM  `cust_prod` WHERE `orderno` = '$orderno'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                 $renew_date = $stmt->fetchColumn();
                 return $renew_date;
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
                exit;
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to get Renew Date');
        }
    }

    public function getCompletedService(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `services` ORDER BY `servid` DESC"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $serviceCopletedDtl = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $serviceCopletedDtl[$row['servid']] = $row;
            }
            
            //print_r($serviceCopletedDtl);
            $this->returnResponse(SUCCESS_RESPONSE, $serviceCopletedDtl);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Completed Service Detail');
        }
    }

    public function getCompletedRenew(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `renewals` ORDER BY `renewid` DESC"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $renewCopletedDtl = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $renewCopletedDtl[$row['renewid']] = $row;
            }
            
            //print_r($renewCopletedDtl);
            $this->returnResponse(SUCCESS_RESPONSE, $renewCopletedDtl);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Completed renew Detail');
        }
    }

    public function custServCompleted(){
        $cardno = $this->clean($this->param['cardno']);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM services WHERE cardno = $cardno ORDER BY servid DESC "; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $cusrServCompleted = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $cusrServCompleted[$row['servid']] = $row;
            }
            
            //print_r($cusrServCompleted);
            $this->returnResponse(SUCCESS_RESPONSE, $cusrServCompleted);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Service Completed Detail');
        }
    }

    public function custRenewCompleted(){
        $cardno = $this->clean($this->param['cardno']);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM renewals WHERE cardno = $cardno ORDER BY renewid DESC "; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $cusrRenewCompleted = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $cusrRenewCompleted[$row['renewid']] = $row;
            }
            
            //print_r($cusrRenewCompleted);
            $this->returnResponse(SUCCESS_RESPONSE, $cusrRenewCompleted);

        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Renew Completed Detail');
        }
    }

}
?>