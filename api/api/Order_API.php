<?php
include '../res/rest.php';

class Order_API extends Rest{

    protected $orderno;
    protected $searchTerm;
    protected $cardno;
    protected $productno;
    protected $pdate;

    public function getAllOrder(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `invoice` INNER JOIN cust ON invoice.cardno = cust.cardno  ORDER BY `id` DESC"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getAllOrderArray = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getAllOrderArray[$row['id']] = $row;
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getAllOrderArray);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Order Detail');
        }
    }

    //Need to remove this since same function exists in product api
    // public function getAllProduct(){
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "SELECT * FROM product"; 
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         $getAllProductArray = array();
    //         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    //             $getAllProductArray[$row['prod_id']] = $row; 
    //         }
    //         $this->returnResponse(SUCCESS_RESPONSE, $getAllProductArray);
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //     }
    // }

    public function addNewOrder(){
        $cardno = $this->param['cardno'];
        $productno = $this->param['productno'];
        $pdate = $this->param['pdate'];
        $groupno = $this->param['groupno'];
        $qty = $this->param['qty'];
        $totalAmt = $this->param['totalAmt'];
        $discountVal = $this->param['discountVal'];
        $finaltotal = $this->param['finaltotal'];
        $paymentType = $this->param['paymentType'];

        $prod_name = $this->prodNameFromID();
        $serv_date = date('Y-m-d', strtotime("+3 months", strtotime($pdate)));
        $renew_date =  date('Y-m-d', strtotime("+12 months", strtotime($pdate)));
        $pdate = date('Y-m-d', strtotime($pdate));
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `cust_prod` (`orderno`, `order_grp`, `cardno`, `prod_id`, `qty`, `prod_name`, `pdate`, `total`, `discount`, `finalAmt`, `paymentType`, `renew_date`, `serv_date`) VALUES (NULL, '$groupno', '$cardno', '$productno', '$qty', '$prod_name', '$pdate', '$totalAmt', '$discountVal', '$finaltotal', '$paymentType', '$renew_date', '$serv_date')"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            
            if ($stmt->rowCount() > 0 && $this->removeStockAfterOrder($productno, $qty) == true) {
                $this->returnResponse(SUCCESS_RESPONSE, 'true');
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, $th);
            exit;
        }
    }

    //Function to delete modified entried 
    public function removePreviousOrder(){
        $ivcID = $this->param['ivcID'];

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "DELETE FROM `invoiceItem` WHERE `ivcID` = '$ivcID'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            // if ($stmt->rowCount() > 0) {
            //     $this->returnResponse(SUCCESS_RESPONSE, 'true');
            // } else {
            //     $this->returnResponse(SUCCESS_RESPONSE, 'false');
            // }
            $this->returnResponse(SUCCESS_RESPONSE, 'true');
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to delete previous details');
        }
        
    }

    // private function checkModOrderExists($orderGRP){
    //     $query = "SELECT * FROM `modOrder` WHERE `order_grp` = '$orderGRP'"; 
    //     $stmt = $this->conn->prepare($query);
    //     $stmt->bindParam(1, $orderGRP);
    //     $stmt->execute();
    //     if ($stmt->rowCount() > 0) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // public function addNewModOrder(){
    //     $cardno = $this->param['cardno'];
    //     $productno = $this->param['productno'];
    //     $orderGRP = $this->param['orderGRP'];
    //     $qty = $this->param['qty'];
    //     $amount = $this->param['amount'];
    //     $name = $this->param['name'];

    //     $prod_name = $this->prodNameFromID();

    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "INSERT INTO `modOrder` (`oModNo`, `cardno`, `name`, `order_grp`, `prod_id`, `prod_name`, `qty`, `date`, `amount`) VALUES (NULL, '$cardno', '$name', '$orderGRP', '$productno', '$prod_name', '$qty', NOW(), '$amount'); "; 
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
            
    //         // if ($stmt->rowCount() > 0 && $this->removeStockAfterOrder($productno, $qty) == true) {
    //         //     $this->returnResponse(SUCCESS_RESPONSE, 'true');
    //         // }
    //         if ($stmt->rowCount() > 0) {
    //             $this->returnResponse(SUCCESS_RESPONSE, 'true');
    //         }
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, $th);
    //         exit;
    //     }
    // }

    public function addNewCustomOrder(){
        $cardno = $this->param['cardno'];
        $name = $this->param['name'];
        $dob = $this->param['dob'];
        $billno = $this->param['billno'];
        $pdate = $this->param['pdate'];
        $gstin = $this->param['gstin'];
        $prod_id = $this->param['productno'];
        $qty = $this->param['qty'];
        $amount = $this->param['amount'];
        $phone = $this->param['phone'];
        $address = $this->param['address'];
        $pdate = date('Y-m-d', strtotime($pdate));
        $prod_name = $this->prodNameFromID();

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `customOrder` (`id`, `cardno`, `name`, `dob`, `billNo`, `billDate`, `gstin`, `phone`, `address`, `prod_id`, `qty`, `amount`) VALUES (NULL, '$cardno', '$name', '$dob', '$billno', '$pdate', '$gstin', '$phone', '$address', '$prod_id', '$qty', '$amount');"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            
            // if ($stmt->rowCount() > 0 && $this->removeStockAfterOrder($productno, $qty) == true) {
            //     $this->returnResponse(SUCCESS_RESPONSE, 'true');
            // }
            if ($stmt->rowCount() > 0) {
                $this->returnResponse(SUCCESS_RESPONSE, 'true');
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, $th);
            exit;
        }
    }

    private function removeStockAfterOrder($productno, $qty){
        $query = "UPDATE `product` SET `stock` = stock - $qty WHERE `product`.`prod_id` = $productno; ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return true;
    }

    //Get Max Group no from database
    // public function getInvoiceReferenceID(){
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "SELECT MAX(reference_id)FROM invoice"; 
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         if ($stmt->rowCount() > 0) {
    //             $reference_id = $stmt->fetchColumn();
    //             $this->returnResponse(SUCCESS_RESPONSE, $reference_id);
    //         } 
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, $th);
    //         exit;
    //     }
    // }removeStockAfterOrder

    //Get Max Group no from database for modded page
    // public function getMaxGRPModno(){
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "SELECT MAX(oModNo)FROM modOrder"; 
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         if ($stmt->rowCount() > 0) {
    //             $order_grpNo = $stmt->fetchColumn();
    //             $this->returnResponse(SUCCESS_RESPONSE, $order_grpNo);
    //         } else {
    //             $this->returnResponse(SUCCESS_RESPONSE, "false");
    //             exit;
    //         }
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Group Number');
    //         exit;
    //     }
    // }

    public function prodNameFromID(){
        $productno = $this->param['productno'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT `prod_name` from product where `prod_id` = $productno "; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $prod_name = $stmt->fetchColumn();
                return $prod_name;
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
                exit;
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get product Name');
            exit;
        }
    }

    public function orderExists(){
        $orderno = $this->param['orderno'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM cust_prod WHERE `orderno` = '$orderno'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $this->returnResponse(SUCCESS_RESPONSE, "Orderno Exists");
            } 
            
        } catch (\Throwable $th) {
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Order Detail');
        }
    }

    // public function orderAutoLoad(){
    //     $searchTerm = $this->param['searchTerm'];
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "SELECT `prod_name` FROM product WHERE prod_name LIKE '%".$searchTerm."%'"; 
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         $searchResult = array();
    //         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    //             $searchResult[] = $row;
    //         }
    //         $this->returnResponse(SUCCESS_RESPONSE, $searchResult);
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Order Detail');
    //     }
    // }

    public function getOrderWihIvcNo(){
        $ivcID = $this->param['ivcID'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `cust_prod` INNER JOIN product on cust_prod.prod_id = product.prod_id WHERE `order_grp` = '$groupno'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getOrderWithGrp = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getOrderWithGrp[$row['orderno']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getOrderWithGrp);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Order Detail');
        }
    }

    // public function getModOrderWihGrpno(){
    //     $groupno = $this->param['groupno'];
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "SELECT * FROM modOrder INNER JOIN product on modOrder.prod_id = product.prod_id WHERE `order_grp` = '$groupno'"; 
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         $getOrderWithGrp = array();
    //         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    //             $getOrderWithGrp[$row['oModNo']] = $row; 
    //         }
    //         $this->returnResponse(SUCCESS_RESPONSE, $getOrderWithGrp);
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Order Detail');
    //     }
    // }

    // public function getCustomOrderWihBillno(){
    //     $billNo = $this->param['groupno'];
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "SELECT * FROM `customOrder` INNER JOIN product on customOrder.prod_id = product.prod_id WHERE `billNo` = '$billNo'"; 
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         $getOrderWithGrp = array();
    //         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    //             $getOrderWithGrp[$row['id']] = $row; 
    //         }
    //         $this->returnResponse(SUCCESS_RESPONSE, $getOrderWithGrp);
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Error Cannot get Order Detail');
    //     }
    // }

    public function getCustOrder(){
        $cardno = $this->param['cardno'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `cust_prod` WHERE `cardno` = $cardno"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $custOrder = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $custOrder[$row['orderno']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $custOrder);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to get Order Details');
        }
    }

    public function getCustDetailForIVC(){
        $ivcID = $this->param['ivcID'];
        $cardno = $this->param['cardno'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `invoice` INNER JOIN `cust` ON invoice.cardno = cust.cardno WHERE invoice.cardno = $cardno AND invoice.id = $ivcID "; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $custBillDetail = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->returnResponse(SUCCESS_RESPONSE, $custBillDetail);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to Card Number Details');
        }

    }

    public function getIvcItemDtl(){
        $ivcID = $this->param['ivcID'];
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `invoiceItem` LEFT JOIN product ON invoiceItem.prodID = product.prod_id WHERE ivcID = $ivcID"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getIvcItem = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getIvcItem[$row['id']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getIvcItem);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to get Invoice Items');
        }
    }

    public function getOrderDtlFromOrderNo(){
        $orderno = $this->clean($this->param["orderno"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `cust_prod` WHERE `orderno` = '$orderno'"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $OrderDtl = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->returnResponse(SUCCESS_RESPONSE, $OrderDtl);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to get Order Details');
        }
    }

    // public function updateIvcGenrated(){
    //     $orderno = $this->clean($this->param["orderno"]);
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "UPDATE `cust_prod` SET `ivc_genrated` = '1' WHERE `orderno` = $orderno; ";
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         if ($stmt->rowCount() > 0) {
            
    //             $this->returnResponse(SUCCESS_RESPONSE, "true");

    //         } else {
    //             $this->returnResponse(SUCCESS_RESPONSE, "false");
    //         }
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Unable to update Ivc');
    //     }
    // }

    // public function updateModIvcGenrated(){
    //     $oModNo = $this->clean($this->param["oModNo"]);
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "UPDATE `modOrder` SET `orderModBill` = '1' WHERE `oModNo` = $oModNo; ";
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         if ($stmt->rowCount() > 0) {
            
    //             $this->returnResponse(SUCCESS_RESPONSE, "true");

    //         } else {
    //             $this->returnResponse(SUCCESS_RESPONSE, "false");
    //         }
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Unable to update Ivc');
    //     }
    // }

    // public function updateCustomIvcGenrated(){
    //     $billID = $this->clean($this->param["id"]);
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "UPDATE `customOrder` SET `billGenrated` = '1' WHERE `customOrder`.`id` = $billID; ";
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         if ($stmt->rowCount() > 0) {
            
    //             $this->returnResponse(SUCCESS_RESPONSE, "true");

    //         } else {
    //             $this->returnResponse(SUCCESS_RESPONSE, "false");
    //         }
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Unable to update Ivc');
    //     }
    // }

    // public function updateNoGstIvcGenrated(){
    //     $orderno = $this->clean($this->param["orderno"]);
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "UPDATE `cust_prod` SET `gst_ivc_genrated` = '1' WHERE `orderno` = $orderno; ";
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         if ($stmt->rowCount() > 0) {
            
    //             $this->returnResponse(SUCCESS_RESPONSE, "true");

    //         } else {
    //             $this->returnResponse(SUCCESS_RESPONSE, "false");
    //         }
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Unable to update Nogst Ivc');
    //     }
    // }

    public function chkBillExists(){
        $billNo = $this->clean($this->param["billNo"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `customOrder` WHERE `billNo` = $billNo AND `billLock` = 1";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            //$this->removePreviousCustOrder();
            if ($stmt->rowCount() > 0) {
                $this->returnResponse(SUCCESS_RESPONSE, "true");

            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to update Nogst Ivc');
        }
    }

    //Function to delete modified entried 
    // public function removePreviousCustOrder(){
    // $billNo = $this->param['billNo'];
    //     $query = "DELETE FROM `customOrder` WHERE `billNo` = '$billNo'";
    //     $stmt = $this->conn->prepare($query);
    //     $stmt->execute();
    //     if ($stmt->rowCount() > 0) {
    //         $this->returnResponse(SUCCESS_RESPONSE, 'true');
    //     } else {
    //         $this->throwError(SOMETHING_WENT_WRONG, 'unable to delete previous details');
    //     }
    // }


    // public function lockCustomBill(){
    //     $billNo = $this->clean($this->param["billNo"]);
    //     try {
    //         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //         $query = "UPDATE `customOrder` SET `billLock` = 1 WHERE `customOrder`.`billNo` = $billNo;";
    //         $stmt = $this->conn->prepare($query);
    //         $stmt->execute();
    //         if ($stmt->rowCount() > 0) {
    //             $this->returnResponse(SUCCESS_RESPONSE, "true");
    //         } else {
    //             $this->returnResponse(SUCCESS_RESPONSE, "false");
    //         }
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         $this->throwError(SOMETHING_WENT_WRONG, 'Unable to lock Bill');
    //     }
    // }

    public function addNewInvoice(){
        $cardno = $this->clean($this->param["cardno"]);
        $pdate = $this->clean($this->param["pdate"]);
        $discount = $this->clean($this->param["discount"]);
        $amount = $this->clean($this->param["amount"]);
        $paymentType = $this->clean($this->param["paymentType"]);
        $ivcID = $this->clean($this->param["ivcID"]);

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `invoice` (`id`, `cardno`, `date`, `discount`, `ivcAmt`, `status`, `dueDate`, `paid`, `outstanding`, `paymentType`) VALUES ($ivcID, '$cardno', '$pdate', '$discount', '$amount', 'pending', '$pdate', NULL, '$amount', '$paymentType');";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                if($this->addfak_invoice()){
                    $this->returnResponse(SUCCESS_RESPONSE, "true");
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to add Invoice');
        }
    }

    //function to add dublicate entry in fak_invoice table
    private function addfak_invoice(){
        $cardno = $this->clean($this->param["cardno"]);
        $pdate = $this->clean($this->param["pdate"]);
        $discount = $this->clean($this->param["discount"]);
        $amount = $this->clean($this->param["amount"]);
        $paymentType = $this->clean($this->param["paymentType"]);
        $ivcID = $this->clean($this->param["ivcID"]);

        $query = "INSERT INTO `fak_invoice` (`id`, `cardno`, `date`, `discount`, `ivcAmt`, `status`, `dueDate`, `paid`, `outstanding`, `paymentType`) VALUES ($ivcID, '$cardno', '$pdate', '$discount', '$amount', 'pending', '$pdate', NULL, '$amount', '$paymentType');";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        }
    }

    //Main entry for invoice Item
    public function addNewInvoiceItems(){
        $ivcID = $this->clean($this->param["ivcID"]);
        $prodID = $this->clean($this->param["prodID"]);
        $prodName = $this->clean($this->param["prodName"]);
        $prodDesc = $this->clean($this->param["prodDesc"]);
        $qty = $this->clean($this->param["qty"]);
        $prodAmt = $this->clean($this->param["prodAmt"]);
        $gstRate = $this->clean($this->param["gstRate"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "INSERT INTO `invoiceItem` (`id`, `ivcID`, `prodID`, `prodName`, `prodDesc`, `qty`, `prodAmt`, `gstRate`, `modItem`) VALUES (NULL, '$ivcID', '$prodID', '$prodName', '$prodDesc', '$qty', '$prodAmt', '$gstRate', '0');";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                if($this->addNew_fakInvoiceItems()){
                    $this->returnResponse(SUCCESS_RESPONSE, "true");
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to add Invoice');
        }
    }

    //function to add dublicate entry in fak_invoiceItem table
    private function addNew_fakInvoiceItems(){
        $ivcID = $this->clean($this->param["ivcID"]);
        $prodID = $this->clean($this->param["prodID"]);
        $prodName = $this->clean($this->param["prodName"]);
        $prodDesc = $this->clean($this->param["prodDesc"]);
        $qty = $this->clean($this->param["qty"]);
        $prodAmt = $this->clean($this->param["prodAmt"]);
        $gstRate = $this->clean($this->param["gstRate"]);

        $query = "INSERT INTO `fak_invoiceItem` (`id`, `ivcID`, `prodID`, `prodName`, `prodDesc`, `qty`, `prodAmt`, `gstRate`, `modItem`) VALUES (NULL, '$ivcID', '$prodID', '$prodName', '$prodDesc', '$qty', '$prodAmt', '$gstRate', '0');";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        }
    }

    public function getCurrentIvcID(){
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT MAX(id) FROM invoice";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $ivcID = $stmt->fetchColumn();
                $this->returnResponse(SUCCESS_RESPONSE, $ivcID);
            } 
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, $th);
            exit;
        }
    }

    public function chkIvcExists(){
        $ivcID = $this->clean($this->param["ivcID"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `invoice` WHERE `id` = '$ivcID'";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $this->returnResponse(SUCCESS_RESPONSE, "true");
            } else {
                $this->returnResponse(SUCCESS_RESPONSE, "false");
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(USER_NOT_EXIST, "Something Not working");
        }
    }

    public function updateExisitngInvoice(){
        $discount = $this->clean($this->param["discount"]);
        $amount = $this->clean($this->param["amount"]);
        $ivcID = $this->clean($this->param["ivcID"]);

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "UPDATE `invoice` SET `discount` = $discount, `ivcAmt` = '$amount' WHERE `id` = '$ivcID';";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $this->returnResponse(SUCCESS_RESPONSE, "true");
            }
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'Unable to add Invoice');
        }

    }

    public function getIvcDtl(){
        $ivcID = $this->clean($this->param["ivcID"]);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `invoice` WHERE `id` = $ivcID"; 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $getIvcDtl = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $getIvcDtl[$row['id']] = $row; 
            }
            $this->returnResponse(SUCCESS_RESPONSE, $getIvcDtl);
        } catch (\Throwable $th) {
            //throw $th;
            $this->throwError(SOMETHING_WENT_WRONG, 'unable to get Invoice Items');
        }
    }



}
?>