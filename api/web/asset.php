<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  
  include '../api/Asset_API.php';

  $asset = new Asset_API();

  $serviceName = $asset->clean($asset->serviceName);
  
  switch ($serviceName) {
      case 'sendInvoiceMail':
          $asset->sendInvoiceMail();
        break;
      case 'sendSMS':
          $asset->sendSMS();
        break;
      case 'sendRegistrationSMS':
          $asset->sendRegistrationSMS();
        break;  
        
      default:
          $asset->throwError(API_DOST_NOT_EXIST, 'Invalid Service Requested');
        break;
  }

  ?>