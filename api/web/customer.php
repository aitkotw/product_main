<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  
  include '../api/Customer_API.php';

  $customer = new Customer_API();

  $serviceName = $customer->clean($customer->serviceName);
  
  switch ($serviceName) {
      case 'getCustDetail':
            $customer->getCustDetail();
          break;
      case 'getAddressBook':
            $customer->getAddressBook();
          break;
      case 'getAddressBookCount':
          $customer->getAddressBookCount();
        break;
      case 'AddNewCustomer':
          $customer->AddNewCustomer();
        break;  
      case 'cardNoExists':
          $customer->cardNoExists();
        break; 
      case 'getMaxCardNo':
          $customer->getMaxCardNo();
        break; 
      case 'newCustRegistered':
          $customer->newCustRegistered();
        break; 
      case 'delNewCustRegistered':
          $customer->delNewCustRegistered();
        break; 
      case 'addRegisteredCustomer':
          $customer->addRegisteredCustomer();
        break; 
      case 'deleteCustomerWithCardNo':
          $customer->deleteCustomerWithCardNo();
        break; 
      case 'updateCustomerDtl':
          $customer->updateCustomerDtl();
        break; 
      default:
          $customer->throwError(API_DOST_NOT_EXIST, 'Invalid Service Requested');
        break;
  }

  ?>