<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  
  include '../api/Order_API.php';

  $order = new Order_API();

  $serviceName = $order->clean($order->serviceName);
  
  switch ($serviceName) {
      case 'getAllOrder':
            $order->getAllOrder();
          break;
      case 'orderExists':
          $order->orderExists();
        break; 
      case 'addNewOrder':
          $order->addNewOrder();
        break;  
      case 'getMaxGRPno':
          $order->getMaxGRPno();
        break;      
      case 'orderAutoLoad':
          $order->orderAutoLoad();
        break;  
      case 'getAllProduct':
          $order->getAllProduct();
        break;
      case 'getOrderWihIvcNo':
          $order->getOrderWihIvcNo();
        break;
      case 'getCustOrder':
          $order->getCustOrder();
        break;
      case 'getCustDetailForIVC':
          $order->getCustDetailForIVC();
        break;
      case 'getOrderDtlFromOrderNo':
          $order->getOrderDtlFromOrderNo();
        break;
      case 'updateIvcGenrated':
          $order->updateIvcGenrated();
        break; 
      case 'updateNoGstIvcGenrated':
          $order->updateNoGstIvcGenrated();
        break; 
      case 'getInvoiceReferenceID':
          $order->getInvoiceReferenceID();
        break;  
      case 'addNewModOrder':
          $order->addNewModOrder();
        break; 
      case 'removePreviousOrder':
          $order->removePreviousOrder();
        break;
      case 'getModOrderWihGrpno':
          $order->getModOrderWihGrpno();
        break;  
      case 'updateModIvcGenrated':
          $order->updateModIvcGenrated();
        break;
      case 'addNewCustomOrder':
          $order->addNewCustomOrder();
        break;
      case 'getCustomOrderWihBillno':
          $order->getCustomOrderWihBillno();
        break;
      case 'updateCustomIvcGenrated':
          $order->updateCustomIvcGenrated();
        break;
      case 'chkBillExists':
          $order->chkBillExists();
        break;
      case 'removePreviousCustOrder':
          $order->removePreviousCustOrder();
        break;
      case 'lockCustomBill':
          $order->lockCustomBill();
        break;
      case 'addNewInvoice':
          $order->addNewInvoice();
        break;
      case 'addNewInvoiceItems':
          $order->addNewInvoiceItems();
        break;
      case 'getInvoiceIDfromRefID':
          $order->getInvoiceIDfromRefID();
        break;
      case 'getIvcItemDtl':
          $order->getIvcItemDtl();
        break;
      case 'getCurrentIvcID':
          $order->getCurrentIvcID();
        break;
      case 'chkIvcExists':
          $order->chkIvcExists();
        break;
      case 'updateExisitngInvoice':
          $order->updateExisitngInvoice();
        break;
      case 'getIvcDtl':
          $order->getIvcDtl();
        break;
      
      default:
          $order->throwError(API_DOST_NOT_EXIST, 'Invalid Service Requested');
          break;
  }
  
  ?>