<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  
  include '../api/FkBill_API.php';

  $fkBill = new FkBill_API();

  $serviceName = $fkBill->clean($fkBill->serviceName);
  
  switch ($serviceName) {
    case 'getfk_AllOrder':
            $fkBill->getfk_AllOrder();
        break;
    case 'getIvcItemDtl':
            $fkBill->getIvcItemDtl();
        break;
    case 'removePreviousOrder':
            $fkBill->removePreviousOrder();
        break;
    case 'updateExisitngInvoice':
            $fkBill->updateExisitngInvoice();
        break;
    case 'addNewInvoiceItems':
            $fkBill->addNewInvoiceItems();
        break;
        case 'getCustDetailForIVC':
            $fkBill->getCustDetailForIVC();
        break;
        case 'lockBillNo':
            $fkBill->lockBillNo();
        break;
        case 'getIvcDtl':
            $fkBill->getIvcDtl();
        break;

    default:
            $fkBill->throwError(API_DOST_NOT_EXIST, 'Invalid Service Requested');
        break;
  }
