<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  
  include '../api/Service_API.php';

  $service = new Service_API();

  $serviceName = $service->clean($service->serviceName);
  
  switch ($serviceName) {
      case 'pendingService':
            $service->pendingService();
          break;
      case 'pendingRenewal':
            $service->pendingRenewal();
          break;
      case 'pendingServiceRenewal':
            $service->pendingServiceRenewal();
          break;
      case 'pendingServiceCount':
            $service->pendingServiceCount();
          break;
      case 'pendingRenewalCount':
            $service->pendingRenewalCount();
          break;
      case 'pendingServiceRenewalCount':
            $service->pendingServiceRenewalCount();
          break;  
      case 'update_service':
            $service->update_service();
          break;  
      case 'update_renew':
            $service->update_renew();
          break;    
      case 'getCompletedService':
            $service->getCompletedService();
          break; 
      case 'getCompletedRenew':
            $service->getCompletedRenew();
          break;      
      case 'custServCompleted':
            $service->custServCompleted();
          break;      
      case 'custRenewCompleted':
            $service->custRenewCompleted();
          break;
      default:
          $service->throwError(API_DOST_NOT_EXIST, 'Invalid Service Requested');
          break;
  }

  ?>