<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  include '../api/Login_API.php';

  $login_api = new Login_API();

  if ($login_api->userExist() == true) {
    $login_api->login_user();
  } else {
    $login_api->throwError(USER_NOT_EXIST, "Email Does Not Exist");
  }
  

  ?>