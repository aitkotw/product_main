<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  
  include '../api/Vendor_API.php';

  $vendor = new Vendor_API();

  $serviceName = $vendor->clean($vendor->serviceName);
  
  switch ($serviceName) {
    case 'getManvoGRPNo':
          $vendor->getManvoGRPNo();
        break;  
    case 'addVendorOrder':
          $vendor->addVendorOrder();
        break;  
    case 'getAllOrderVendor':
          $vendor->getAllOrderVendor();
        break;     

    default:
          $vendor->throwError(API_DOST_NOT_EXIST, 'Invalid Service Requested');
        break;
  }
  ?>