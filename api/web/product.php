<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  
  include '../api/Product_API.php';

  $product = new Product_API();

  $serviceName = $product->clean($product->serviceName);

  switch ($serviceName) {
      case 'getAllProduct':
            $product->getAllProduct();
        break;
      case 'addNewProduct':
            $product->addNewProduct();
        break;
      case 'deleteProductByID':
            $product->deleteProductByID();
        break;
      case 'addNewVendor':
            $product->addNewVendor();
        break;
      case 'getAllVendor':
            $product->getAllVendor();
        break;
      
      default:
          $product->throwError(API_DOST_NOT_EXIST, 'Invalid Service Requested');
        break;
  }

?>