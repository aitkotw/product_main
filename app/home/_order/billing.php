<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';
?>

<?php
    if (isset($_GET['mode'])) {
        if($_GET['mode'] == 'successLock'){
            echo showResponseSuccess('Bill Lock Updated Successfully');
        } 
    } 
?>

<div class="table-responsive">
    <table id="myTable" class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>Bill No.</th>
        <th>Card No.</th>
        <th>Name</th>
        <th>Date</th>
        <th>Amount</th>
        <th>Update</th>
        <th>Invoice</th>
      </tr>
    </thead>
 <tbody>

 <?php
$json_request = array("None"=>"none");
$my_Response_Order = request_api(FKBILL_API_PAGE ,"getfk_AllOrder", $json_request); 
//var_dump($my_Response_Order);
foreach ($my_Response_Order as $key => $value) {

   $date = date("d/m/Y", strtotime($my_Response_Order[$key]['date']));
   if($my_Response_Order[$key]['ivcStatus'] == "1"){
    echo  '<tr class="table-success"><td class="ivcID">'.$my_Response_Order[$key]['id'].'</td><td class="getCNo userCardNo">'.$my_Response_Order[$key]['cardno'].'</td><td class="getName">'.$my_Response_Order[$key]['name'].'</td><td>'.$date.'</td><td class="">'.$my_Response_Order[$key]['ivcAmt'].'</td><td style = "color:#82c91e; font-size:22px;">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-lock"></i></td><td><button type="button"  data-toggle="modal" data-target="#myModal" class="btn btn-primary use-address-invoice">Print Bill</button></td></tr>';
   } else {
    echo  '<tr class="table-success"><td class="ivcID">'.$my_Response_Order[$key]['id'].'</td><td class="getCNo userCardNo">'.$my_Response_Order[$key]['cardno'].'</td><td class="getName">'.$my_Response_Order[$key]['name'].'</td><td>'.$date.'</td><td class="">'.$my_Response_Order[$key]['ivcAmt'].'</td><td><button type="button" class="btn btn-info editOrder">Edit</button></td><td><button type="button"  data-toggle="modal" data-target="#myModal" class="btn btn-primary use-address-invoice">Print Bill</button></td></tr>';
   }
   
}
?>

<script>

//Modify order by redirecting to modify order page
$(".editOrder").click(function() {

var $row = $(this).closest("tr");    // Find the row
var $ivcID = $row.find(".ivcID").text(); // Find the text
var $name = $row.find(".getName").text(); // Find the text
var $cardno = $row.find(".getCNo").text(); // Find the text

var $ivcID = window.btoa($ivcID);
var $name = window.btoa($name);
var $cardno = window.btoa($cardno);

window.location.href = "../fkBill/updateBill.php?ivc="+$ivcID+"&name="+$name+"&cardno="+$cardno;

});

$(".use-address-invoice").click(function() {

// alert(GlobCardNoGST+" "+GlobalivcID);
var $row = $(this).closest("tr");    // Find the row
GlobCardNoGST = $row.find(".getCNo").text(); // Find the text
GlobalivcID = $row.find(".ivcID").text(); // Find the text

 GlobCardNoGST = window.btoa(GlobCardNoGST);
 GlobalivcID = window.btoa(GlobalivcID);
 billType =  window.btoa("gst");
 
var data = [GlobCardNoGST, GlobalivcID, billType];
var myJSONData = JSON.stringify(data); 
var myJSONData = window.btoa(myJSONData);
//alert(myJSONData);
window.location.href = "../fkBill/genIvc.php?vals="+myJSONData;

});

</script>
<?php include '../include/footer.php'; ?>