<?php
include '../res/genral.php';
if(isset($_SESSION['ivcCardno']) || isset($_SESSION['ivcOrderGrpno'])){
    unset($_SESSION['ivcCardno']); 
    unset($_SESSION['ivcOrderGrpno']); 
  }
include '../include/header.php'; 
include '../include/menu.php';

?>
<!------------------------------------------------------------------------------------------------------------------------------------------------->
<div class = "container-fluid row mt-4">

  <div class = "col-sm-6">
    <button type="button" class="btn btn-primary btn-block" id="customOrderPlaced" ><i class="fas fa-cart-plus">&nbsp;&nbsp;&nbsp;</i>Quick Order</button>
  </div>
  <div class = "col-sm-6">
    <button type="button" class="btn btn-info btn-block" id="newAMCreated" ><i class="fas fa-user-clock ">&nbsp;&nbsp;&nbsp;</i>New AMC</button>
  </div>

</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------->

<div class = "row mt-4">
    <div class = "col-sm-2 my-auto">

      <div class = "ml-3">

        <select id="mySelect" onchange="myFunction()">
          <option value="cardno">Cardno</option>
          <option value="orderid">Order ID</option>
          <option value="name">Name</option>
        </select>

      </div>
    
  

    </div>
  
    <div class = "col-sm-10">

        <div class=""> <input type="text" id="mySearchInput" onkeyup="mySearchFunctionNew()" placeholder="Search for Orders.."></div>
    
    </div>
</div>

<div class="table-responsive">
    <table id="myTable" class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>Bill No.</th>
        <th>Card No.</th>
        <th>Name</th>
        <th>Date</th>
        <th>Amount</th>
        <th>Invoice</th>
      </tr>
    </thead>
 <tbody>

 <?php
$json_request = array("None"=>"none");
$my_Response_Order = request_api(ORDER_API_PAGE ,"getAllOrder", $json_request); 

foreach ($my_Response_Order as $key => $value) {

   $date = date("d/m/Y", strtotime($my_Response_Order[$key]['date']));
   if($my_Response_Order[$key]['ivcStatus'] == "1"){
    echo  '<tr class="table-info"><td class="ivcID">'.$my_Response_Order[$key]['id'].'</td><td class="getCNo userCardNo">'.$my_Response_Order[$key]['cardno'].'</td><td class="getName">'.$my_Response_Order[$key]['name'].'</td><td>'.$date.'</td><td class="">'.$my_Response_Order[$key]['ivcAmt'].'</td><td><button type="button"  data-toggle="modal" data-target="#myModal" class="btn btn-primary use-address-invoice">Print Bill</button></td></tr>';
   } else {
    echo  '<tr class="table-info"><td class="ivcID">'.$my_Response_Order[$key]['id'].'</td><td class="getCNo userCardNo">'.$my_Response_Order[$key]['cardno'].'</td><td class="getName">'.$my_Response_Order[$key]['name'].'</td><td>'.$date.'</td><td class="">'.$my_Response_Order[$key]['ivcAmt'].'</td><td><button type="button"  data-toggle="modal" data-target="#myModal" class="btn btn-primary use-address-invoice">Print Bill</button></td></tr>';
   }
   
}
?>
<!------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Bill type</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class = "row">
          <div class = "col-sm-6">
            <button type="button" class="btn btn-block btn-info use_for_gst" data-dismiss="modal">GST</button>
          </div>
          <div class = "col-sm-6">
            <button type="button" class="btn btn-block btn-info use_for_non_gst" data-dismiss="modal">No-GST</button>
          </div>
          <!-- <div class = "col-sm-4">
            <button type="button" class="btn btn-block btn-info use_for_modBill" data-dismiss="modal">Modified</button>
          </div> -->
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------->

</tbody></table></div>
<!------------------------------------------------------------------------------------------------------------------------------------------------->

<script>


$(".userCardNo").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var text = $row.find(".getCNo").text(); // Find the text
  var text = window.btoa(text);
  window.location.href = "../_customer/cust_detail.php?cardno="+text;

});

$(".use-address").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var $text = $row.find(".getCNo").text(); // Find the text

  window.location.href = "new_order.php?cardno="+$text;

});

//Modify order by redirecting to modify order page
$(".editOrder").click(function() {

var $row = $(this).closest("tr");    // Find the row
var $ivcID = $row.find(".ivcID").text(); // Find the text
var $name = $row.find(".getName").text(); // Find the text
var $cardno = $row.find(".getCNo").text(); // Find the text

var $ivcID = window.btoa($ivcID);
var $name = window.btoa($name);
var $cardno = window.btoa($cardno);

window.location.href = "modifyOrder.php?ivc="+$ivcID+"&name="+$name+"&cardno="+$cardno;

});

//Variable to store Cardno and Group no for GST/Non-GST bill
var GlobCardNoGST, GlobalivcID, billType;

$(".use-address-invoice").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  GlobCardNoGST = $row.find(".getCNo").text(); // Find the text
  GlobalivcID = $row.find(".ivcID").text(); // Find the text

});

$(".use_for_gst").click(function() {

  // alert(GlobCardNoGST+" "+GlobalivcID);

   GlobCardNoGST = window.btoa(GlobCardNoGST);
   GlobalivcID = window.btoa(GlobalivcID);
   billType =  window.btoa("gst");
  var data = [GlobCardNoGST, GlobalivcID, billType];
  var myJSONData = JSON.stringify(data); 
  var myJSONData = window.btoa(myJSONData);
  //alert(myJSONData);
  window.location.href = "../generateInvoice.php?vals="+myJSONData;

});

$(".use_for_non_gst").click(function() {

  GlobCardNoGST = window.btoa(GlobCardNoGST);
  GlobalivcID = window.btoa(GlobalivcID);
  billType =  window.btoa("nogst");
  var data = [GlobCardNoGST, GlobalivcID, billType];
  var myJSONData = JSON.stringify(data); 
  var myJSONData = window.btoa(myJSONData);
  //alert("Need to impliment");

  window.location.href = "../generateInvoice.php?vals="+myJSONData;

});

// $(".use_for_modBill").click(function() {

// GlobCardNoGST = window.btoa(GlobCardNoGST);
// GlobalivcID = window.btoa(GlobalivcID);
// billType =  window.btoa("gst");

// var data = [GlobCardNoGST, GlobalivcID, billType];
// var myJSONData = JSON.stringify(data); 
// var myJSONData = window.btoa(myJSONData);
// //alert("Need to impliment");

// window.location.href = "../modifyInvoice.php?vals="+myJSONData;

// });

// var processing, offset = 0;

// $(document).ready(function(){

//   $(document).scroll(function(e){

//     // grab the scroll amount and the window height
//     var scrollAmount = $(window).scrollTop();
//     var documentHeight = $(document).height();

//     // calculate the percentage the user has scrolled down the page
//     var scrollPercent = (scrollAmount / documentHeight) * 100;

//     if(scrollPercent > 70) {
//         // run a function called doSomething
//        doSomething();
//     }

//     function doSomething() { 
//       if (processing)
//       return false;

//       processing = true;
//       offset = offset +1;
//         // do something when a user gets 70% of the way down my page
//         alert($(document).height());
//       processing = false;
//     }

// });

// });


// Search Script Start // =====================================================================================================================

var mySearchArrayIndex = 2;

function myFunction() {
  var x = document.getElementById("mySelect").value;
  if(x == "orderid"){
    mySearchArrayIndex = 0;
  } else if(x == "name"){
    mySearchArrayIndex = 3;
  } else if(x == "cardno"){
    mySearchArrayIndex = 2;
  }
}


function mySearchFunctionNew() {
  // Declare variables
  var input, filter, table, tr, td, txtValue;
  input = document.getElementById("mySearchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don\'t match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[mySearchArrayIndex];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

document.getElementById("customOrderPlaced").onclick = function () {
    location.href = "customOrder.php";
    //alert('New Order');
};

document.getElementById("newAMCreated").onclick = function () {
    //location.href = "add_new.php";
    alert('new AMC');
};
</script>

<?php include '../include/footer.php'; ?>