<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';

$ivcID = $invoiceItemDtl = "";

if(isset($_GET['ivc']) && !empty($_GET['ivc']) && isset($_GET['name']) && !empty($_GET['name']) ){
    //Access Detsils of order and show 
      $ivcID = clean(base64_decode($_GET['ivc']));
      $name = clean(base64_decode($_GET['name']));
      $cardno = clean(base64_decode($_GET['cardno']));

      $json_request = array("ivcID"=>"$ivcID");
      $invoiceItemDtl = request_api(ORDER_API_PAGE ,"getIvcItemDtl", $json_request);

      $json_request = array("ivcID"=>"$ivcID");
      $ivcDtl = request_api(ORDER_API_PAGE ,"getIvcDtl", $json_request);

      //var_dump($invoiceItemDtl);
      

} else {
    header('Location: ../404.php');
}

//Actual execution after form submission 
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $cardno = clean($_POST['cardno']);
    $name = clean($_POST['name']);

   //  var_dump($_POST);
   //  die();

    $ivcID = clean(base64_decode($_GET['ivc']));
   
    //Delete any previous entry to cause override
    $json_request = array("ivcID"=>"$ivcID");
    $chkDelOrder = request_api(ORDER_API_PAGE ,"removePreviousOrder", $json_request);

    if($chkDelOrder == 'true'){

      //$pdate = clean($_POST['pdate']);
      //$billType = clean($_POST['gstRadioBtn']);
      $totalAmt = clean($_POST['totalAmt']);
      $discountVal = clean($_POST['discountVal']);
      $finaltotal = clean($_POST['finaltotal']);
      //$paymentType = clean($_POST['paymentType']);

      //Convert pDate to database Date format
      // $pdate  = strtotime($pdate);
      // $pdate = date('Y-m-d',$pdate);

      $prodID = ($_POST['prodNo']);
      $prodName = $_POST['prodName'];
      $prodSpec = $_POST['prodSpec'];
      $prodAmt = $_POST['prodAmt'];
      $prodGSTAmt = $_POST['prodGST'];
      $prodGstRate = $_POST['prodGstRate'];
      $prodQty = $_POST['prodQty'];
      $prodTotal = $_POST['prodTotal'];
      //$ivcID = clean($_POST['ivcID']);

      //Update Existing Invoice table with new Data
      $json_request = array("discount"=>"$discountVal", "amount"=>"$finaltotal", "ivcID"=>"$ivcID" );
      $myGrpNo = request_api(ORDER_API_PAGE ,"updateExisitngInvoice", $json_request);

      //Add invoiceItems to table
      $arrayLength = count($prodID);
            
      for ($i=0; $i < $arrayLength; $i++) { 
            //Access each value with $name[$i] and enter values to database

            // Insert Values into Invoice Items table
            $json_request = array("ivcID"=>"$ivcID", "prodID"=>"$prodID[$i]", "prodName"=>"$prodName[$i]", "prodDesc"=>"$prodSpec[$i]", "qty"=>"$prodQty[$i]", "prodAmt"=>"$prodAmt[$i]", "gstRate"=>"$prodGstRate[$i]");
            $addInvoiceItems = request_api(ORDER_API_PAGE ,"addNewInvoiceItems", $json_request);
            
      }

      if($addInvoiceItems === 'true'){

         //die('Success');
         
         $_SESSION['ivcCardno'] = $_SESSION['ivcID'] = $_SESSION['billType'] = "";

         $_SESSION['ivcCardno'] = $cardno;
         $_SESSION['ivcID'] = $ivcID;
         $_SESSION['billType'] = 'gst';

         //if ($billType == 'gst') {
         header('Location: ../generateInvoice.php?&mode=success_order&billType=gst');
         //} else { 
         //header('Location: ../generateInvoice.php?&mode=success_order&billType=nogst');
        // }
         
      }else{
         // var_dump($addInvoiceItems);
         // die();
         //die('fail');
         header('Location: customOrder.php?mode=orderFail');
      }

    }

}
?>



<div class="row">
   
   <div class="container" style="max-width:1500px;">

      <div class="col-sm-12 shadow-lg">

         <div class="row paneladdorder">
            <h2 class="mx-auto">Order Update</h2>
         </div>   
         <div class="row">
            <div class="container px-3 pt-5 pb-3" style = "max-width:1480px;">
               <form name="loginForm" method="post" onsubmit="return validateMyForm();" action="">
                  
                  <?php
                     if (isset($_GET['mode'])) {
                        if($_GET['mode'] == 'cust_success'){
                           echo showResponseSuccess('Customer Added Successfully, Please Add Order!!');
                        } else if($_GET['mode'] == 'fail'){
                           echo showResponseFail('Something went wrong, Unable to add Order!!');                  
                        }
                     } 
                  ?>
<!----------------------------------Customer Details Start ---------------------------------------->
                  <div class="form-group form-row">
                     <div class="col-sm-6">
                        <label for="inputAddress">Card No.</label>
                        <input class="form-control" type="text" pattern=".{6,}" title="Please Enter 6 digit card No." disabled value="<?php echo $cardno; ?>" >
                        <input type="hidden" name="cardno" value="<?php echo $cardno; ?>" />
                     </div>

                     <div class="col-sm-6">
                        <label for="inputAddress">First Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter First Name" disabled value="<?php echo $name; ?>">
                        <input type="hidden" name="name" value="<?php echo $name; ?>">
                     </div>
                  </div>
<!----------------------------------//Customer Details Ends ---------------------------------------->

                  
<!--------------------------------------- All Product Model Starts---------------------------------------------------------->

                   <!-- The Modal -->
                  <div class="modal" id="myModal">
                     <div class="modal-dialog" style="max-width: 1400px;">
                        <div class="modal-content">
                        
                        <!-- Modal Header -->
                        <div class="modal-header">
                           <h4 class="modal-title">Select Product</h4>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        <?php

                           $json_request = array("None"=>"none");
                           $my_Response_Prod = request_api(PRODUCT_API_PAGE ,"getAllProduct", $json_request); 

                           //var_dump($my_Response_Prod);
                           echo '<div class="table-responsive">
                              <table id="myTable" class="table table-hover">
                              <thead class="thead-dark">
                                 <tr>
                                 <th>ID</th>
                                 <th>Name</th>
                                 <th>Specs</th>
                                 <th>Desc</th>
                                 <th>Price</th>
                                 <th>Stock</th>
                                 <th>Qty</th>
                                 <th>Select</th>
                                 </tr>
                              </thead>
                           <tbody>';

                           foreach ($my_Response_Prod as $key => $value) {

                              $myButtonVal;
                              if ($my_Response_Prod[$key]['stock'] == 0) {
                                    $myButtonVal = '<td><button type="button" disabled class="btn btn-danger use-address">Select</button></td>';
                              } else {
                                    $myButtonVal = '<td><button type="button" class="btn btn-primary use-address">Select</button></td>';
                              }
                                 
                              echo  '<tr class="table-success"><td class="getGSTRate" style = "display:none">'.$my_Response_Prod[$key]['gstrate'].'</td><td class="getPNo userCardNo">'.$my_Response_Prod[$key]['prod_id'].'</td><td class="getpname">'.$my_Response_Prod[$key]['prod_name'].'</td><td class="getpspec">'.$my_Response_Prod[$key]['spec'].'</td><td class="getpdesc">'.$my_Response_Prod[$key]['prod_desc'].'</td><td class="getpprice">'.$my_Response_Prod[$key]['price'].'</td><td class="stock">'.$my_Response_Prod[$key]['stock'].'</td><td class="" style="width: 40px;"><input type="text" class="form-control getpqty" value="01" id="usr" maxlength="2" style="padding: 0px;"></td>'.$myButtonVal.'</tr>';

                           }

                           echo '</tbody></table></div>';

                        ?>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                        
                        </div>
                     </div>
                  </div>
<!--------------------------------------- //All Product Model Ends---------------------------------------------------------->

<!----------------------------------  Custom Product Model Starts -------------------------------------------------------->


                  <!-- The Modal -->
                  <div class="modal" id="myCustomProduct">
                     <div class="modal-dialog" style = "max-width: 700px;">
                        <div class="modal-content">

                           <!-- Modal Header -->
                           <div class="modal-header bg-info" style = "color:#fff;">
                           <h4 class="modal-title">Add Custom Product</h4>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>

                           <!-- Modal body -->
                           <div class="modal-body">
   
                              <div class="form-row">
                                 <div class="form-group col-md-12">
                                    <label for="custProdName">Product Name</label>
                                    <input type="text" class="form-control" id="custProdName" autocomplete="off"  placeholder="Product Name">
                                 </div>
                                 <div class="form-group col-md-12">
                                    <label for="custProdDesc">Description</label>
                                    <input type="text" class="form-control" id="custProdDesc" autocomplete="off" placeholder="Product Description">
                                 </div>
                              </div>

                              <div class="form-row">
                                 <div class="form-group col-md-4">
                                    <label for="custProdQty">Quantity</label>
                                    <input type="number" class="form-control" min=1 id="custProdQty" value="1">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label for="custProdAmt">Price</label>
                                    <input type="text" class="form-control" id="custProdAmt" autocomplete="off" placeholder="Enter Product Amount">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label for="custProdGST">GST Rate (%)</label>
                                    <input type="text" class="form-control" id="custProdGST" value="0">
                                 </div>
                              </div>                              
                           </div>

                           <!-- Modal footer addCustomOrderToTable -->
                           <div class="modal-footer">
                              <button type="button" onClick = "validateCustomForm()" class="btn btn-primary">Submit</button>
                          
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                           </div>

                        </div>
                     </div>
                  </div>


<!-------------------------------  //Custom Product Model ends  --------------------------------------------------->

<!------------------------------------ Model for Discount Start--------------------------------------->
                  
                  <!-- The Modal -->
                  <div class="modal" id="myModaldiscount">
                     <div class="modal-dialog">
                        <div class="modal-content">

                           <!-- Modal Header -->
                           <div class="modal-header">
                           <h4 class="modal-title">Discount</h4>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>

                           <!-- Modal body -->
                           <div class="modal-body">
   
                                 <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Amount</label>
                                    <div class="col-sm-10">
                                       <input type="text" class="form-control" autocomplete="off" pattern="^[0-9]*$" id = "modelDiscountVal" title="Must contain 10 Numbers" required value="0">
                                    </div>
                                 </div>
                              
                           </div>

                           <!-- Modal footer -->
                           <div class="modal-footer">
                              <button type="button" onClick = "addDiscountVal()" class="btn btn-primary">Submit</button>
                          
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                           </div>

                        </div>
                     </div>
                  </div>
<!----------------------------- //Model for discount End ------------------------------------>


<!-------------------------------------Table Default Structure Starts ------------------------------------->
                  <div><br><hr><br>
                  <table class="table table-hover" id = "myOrderTable">
                     <thead class = "table-success">
                        <tr>
                        <th style = "display:none;">ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>UnitPrice</th>
                        <th>GST</th>
                        <th>Subtotal</th>
                        <th>Action</th>
                        </tr>
                     </thead>
                     <tbody style = "background-color:#e8f3f6;">
                           
                     </tbody>
                  </table>
                           
                  </div><br><hr><br>
<!-------------------------------------//Table Default Structure Ends ------------------------------------->

                  <div class="form-group form-row">
                     <div class ="col-sm-2">
                        <button type="button" class="btn btn-info m-2" data-toggle="modal" data-target="#myModal">Select Product</button>
                     </div>
                     <div class ="col-sm-2">
                        <button type="button" class="btn btn-primary m-2" data-toggle="modal" data-target="#myCustomProduct">Add Product</button>
                     </div>
                     <div class ="col-sm-2">
                        <button type="button" class="btn btn-warning m-2" id = "discountButton" disabled data-toggle="modal" data-target="#myModaldiscount">Add Discount</button>
                     </div>
                     <div class ="col-sm-3">
                           
                     </div>

                     <div class ="container mx-auto my-auto col-sm-3" style = "background-color : #d1e3fa">
                        <div class="container mx-auto my-auto form-group form-row">
                              <div class="container mx-auto my-auto row ">
                                 <label for="staticEmail" class="col-sm-5 col-form-label">Amount:</label>
                                 <div class="col-sm-7">
                                    <input type="text" readonly class="form-control-plaintext" id="totalAmt" style = "font-weight: bold;" value="0">
                                    <input type="hidden" name = "totalAmt" id="totalAmtVal" value="0" />
                                 </div>
                              </div>
                        </div>

                        <div class="container mx-auto my-auto form-group form-row" id = "discountDiv" style = "display : ">
                              <div class="container mx-auto my-auto row ">
                                 <label for="staticEmail" class="col-sm-5 col-form-label">Discount:</label>
                                 <div class="col-sm-7">
                                    <input type="text" readonly class="form-control-plaintext" id="discountVal" style = "font-weight: bold;" value="0">
                                    <input type="hidden" name = "discountVal" id="discountValVal" value="0" />
                                 </div>
                              </div>
                        </div>
                        <div class="container mx-auto my-auto form-group form-row border border-right-0 border-left-0  border-dark border-bottom-0">
                              <div class="container mx-auto my-auto form-group row">
                                 <label for="staticEmail" class="col-sm-5 col-form-label">Total(Rs):</label>
                                 <div class="col-sm-7">
                                    <input type="text" readonly class="form-control-plaintext" id="finaltotal" style = "font-weight: bold;" value="0">
                                    <input type="hidden" name = "finaltotal" id="finaltotalVal"  value="0" />
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>   
                  <hr>
                  <div class="form-group d-flex justify-content-center">   
                     <button type="submit" class="btn btn-lg btn-success col-sm-2 p-1">Add Order</button>
                  </div>
               </form>
            </div>

         </div>
      </div>

   </div>

</div>


<!----------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------- Existing order Auto load from Database Start ---------------------------------------------->
   <?php

      foreach ($invoiceItemDtl as $key => $value) {

         echo '<script>
                  
                  var pNo = "'.$invoiceItemDtl[$key]['prodID'].'"; // Find the text
                  var pName = "'.$invoiceItemDtl[$key]['prodName'].'"; // Find the text
                  var pSpec = "'.$invoiceItemDtl[$key]['prodDesc'].'"; // Find the text
                  var pGSTRate = parseInt('.$invoiceItemDtl[$key]['gstRate'].'); // Find the text
                  var pDesc = "'.$invoiceItemDtl[$key]['prodDesc'].'"; // Find the text
                  var pprice = parseInt('.$invoiceItemDtl[$key]['prodAmt'].'); // Find the text
                  var pQty = parseInt('.$invoiceItemDtl[$key]['qty'].'); // Find the text

                  if(pGSTRate != 0){
                     //var pGST = pGSTRate/100 * (pprice * pQty);
                     var pGST = parseInt((pprice * pQty - ((pprice* pQty) * (100/(100 + pGSTRate)))).toFixed(2));
                     //pGST = Math.round(pGST * 100) / 100;
                  } else {
                     var pGST = 0;
                  }

                  var index = $("#myOrderTable tbody tr:last-child").index();
                  
                  var row = \'<tr>\' +
                        \'<td style = "display:none;"> \'+pNo+\' </td>\' +
                        \'<td> \'+pName+\' </td>\' +
                        \'<td>\'+pSpec+\' </td>\' +
                        \'<td>\'+pQty+\' </td>\' +
                        \'<td>\'+(((pprice * pQty) - pGST)/pQty).toFixed(2)+\' </td>\' +
                        \'<td>\'+pGST+\' </td>\' +
                        \'<td class = "rowTotalAmt">\'+((pQty * pprice))+\' </td>\' +
                     \'<td><button type="button" class="btn btn-danger deleteRow">Delete</button></td>\' +
         
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodNo[]" value="\'+pNo+\'"> </td>\' +
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodName[]" value="\'+pName+\'"></td>\' +
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodSpec[]" value="\'+pSpec+\'"></td>\' +
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodQty[]" value="\'+pQty+\'"></td>\' +
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodAmt[]" value="\'+pprice+\'"></td>\' +
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodGST[]" value="\'+pGST+\'"></td>\' +
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodTotal[]" value="\'+((pQty * pprice))+\'"></td>\' +
                     \'<td style = "display:none;"> <input type="hidden" id="custId" name="prodGstRate[]" value="\'+pGSTRate+\'"></td>\' +
                  \'</tr>\';
                  $("#myOrderTable").append(row);
                  
                  //var totalVal = parseInt(document.getElementById("totalAmt").value);
                  //var finalVal = +totalVal + +((pprice*pQty));
                  
                  //document.getElementById("totalAmt").value = finalVal;
                  //document.getElementById("finaltotal").value = finalVal;
                  $(\'#discountButton\').removeAttr(\'disabled\');         
         </script>';
         
      }

      foreach($ivcDtl as $key => $value){
         
         echo '<script>
               document.getElementById("totalAmt").value = "'.($ivcDtl[$key]['ivcAmt'] + $ivcDtl[$key]['discount']).'";
               document.getElementById("discountVal").value = '.$ivcDtl[$key]['discount'].';
               document.getElementById("finaltotal").value = '.$ivcDtl[$key]['ivcAmt'].';
         </script>';
      }

      
   ?>
<!----------------------------------- //Existing order Auto load from Database Ends ---------------------------------------------->

<script>
   //Add Order table row from product model
   $(".use-address").click(function() {

      var $row = $(this).closest("tr");    // Find the row 
      var pNo = $row.find(".getPNo").text(); // Find the text
      var pName = $row.find(".getpname").text(); // Find the text
      var pSpec = $row.find(".getpspec").text(); // Find the text
      var pGSTRate = parseInt($row.find(".getGSTRate").text()); // Find the text
      var pDesc = $row.find(".getpdesc").text(); // Find the text
      var pprice = parseInt($row.find(".getpprice").text()); // Find the text
      var pQty = parseInt($row.find(".getpqty").val()); // Find the text

      if(pGSTRate != 0){
         //var pGST = pGSTRate/100 * (pprice * pQty);
         var pGST = (pprice * pQty - ((pprice* pQty) * (100/(100 + pGSTRate)))).toFixed(2);
         //pGST = Math.round(pGST * 100) / 100;
      } else {
         var pGST = 0;
      }

      $('#myModal').modal('toggle');

      // if(serverValPost.includes(pNo)){
      //    alert('Selected Product Already Exists');
      // }else{  
            
            var index = $("#myOrderTable tbody tr:last-child").index();
            
            var row = '<tr>' +
                  '<td style = "display:none;"> '+pNo+' </td>' +
                  '<td> '+pName+' </td>' +
                  '<td>'+pSpec+' </td>' +
                  '<td>'+pQty+' </td>' +
                  '<td>'+(((pprice * pQty) - pGST)/pQty).toFixed(2)+' </td>' +
                  '<td>'+pGST+' </td>' +
                  '<td class = "rowTotalAmt">'+((pQty * pprice))+' </td>' +
               '<td><button type="button" class="btn btn-danger deleteRow">Delete</button></td>' +

               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodNo[]" value="'+pNo+'"> </td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodName[]" value="'+pName+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodSpec[]" value="'+pSpec+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodQty[]" value="'+pQty+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodAmt[]" value="'+pprice+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodGST[]" value="'+pGST+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodTotal[]" value="'+((pQty * pprice))+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodGstRate[]" value="'+pGSTRate+'"></td>' +
            '</tr>';
            $("#myOrderTable").append(row);
            
            var totalVal = document.getElementById("totalAmt").value;
            var finalVal = +totalVal + +((pprice*pQty));
            //finalVal = Math.round(finalVal * 100) / 100;
            document.getElementById("totalAmt").value = finalVal;
            var discount = document.getElementById("discountVal").value;
            document.getElementById("finaltotal").value = finalVal - discount;
            $('#discountButton').removeAttr('disabled');
      // }

   });

   // function resetModalForm(){
   //    $('#custProdName').val('');
   // }

   //Delete Order table row on button click --IMP
   $(document).on("click", ".deleteRow", function(){
      var totalVal = document.getElementById("totalAmt").value;
      var finalVal = document.getElementById("finaltotal").value;

      var $row = $(this).closest("tr");    // Find the row
      var currentProdAmt = $row.find(".rowTotalAmt").text(); // Find the text

      document.getElementById("totalAmt").value = totalVal - currentProdAmt;
      document.getElementById("finaltotal").value = finalVal - currentProdAmt;

      $(this).parents("tr").remove();

      var index = $("#myOrderTable tbody tr:last-child").index();
      if(index < 0){
            document.getElementById("discountVal").value = 0;
            document.getElementById("totalAmt").value = 0;
            document.getElementById("finaltotal").value = 0;
            $('#discountButton').attr('disabled','disabled');
      }
   });

   function validateMyForm(){

      if($('#myOrderTable tr').length <= 1){
         alert('Please Select a Product');
         return false;
      } else if($('#myOrderTable tr').length > 1) {
         // var val = document.getElementById('myHiddenVal');
         // var valqty = document.getElementById('myHiddenValqty');

         // var myJSON = JSON.stringify(serverValPost);
         // var myJSONqty = JSON.stringify(serverValPostqty);

         // val.value = myJSON;
         // valqty.value = myJSONqty;

         document.getElementById("totalAmtVal").value = document.getElementById("totalAmt").value; 
         document.getElementById("discountValVal").value = document.getElementById("discountVal").value; 
         document.getElementById("finaltotalVal").value = document.getElementById("finaltotal").value; 
         return true;
      }
   }

   //Prevent Enter Button from subbmitting the form
   $(document).ready(function() {
   $(window).keydown(function(event){
      if(event.keyCode == 13) {
         event.preventDefault();
         return false;
      }
   });
   });


   //Add Discount from Discount Model
   function addDiscountVal(){
      var x = document.getElementById("discountDiv");
      if (x.style.display === "none") {
      x.style.display = "block";
      }
      var discountVal = document.getElementById('modelDiscountVal').value;
      document.getElementById("discountVal").value = discountVal;
      $('#myModaldiscount').modal('hide');

      var totalVal = document.getElementById("totalAmt").value;
      document.getElementById("finaltotal").value = totalVal - discountVal;
      //alert(discountVal);
   }

   function validateCustomForm(){

      var prodName = document.getElementById("custProdName").value;
      var pSpec = document.getElementById('custProdDesc').value;
      var prodQty = document.getElementById("custProdQty").value;
      var prodAmt = document.getElementById("custProdAmt").value;
      var peodGST = document.getElementById("custProdGST").value;

      if(prodName == ""){
         $("#custProdName").addClass("is-invalid");
         $("#custProdName").focus();
         return false;
      } else {
         $("#custProdName").removeClass("is-invalid");
      }
      if(prodQty == "" || prodQty <= 0){
         $("#custProdQty").addClass("is-invalid");
         $("#custProdQty").focus();
         return false;
      } else {
         $("#custProdQty").removeClass("is-invalid");
      }
      if(prodAmt == "" || isNaN(prodAmt)){
         $("#custProdAmt").addClass("is-invalid");
         $("#custProdAmt").focus();
         return false;
      } else {
         $("#custProdAmt").removeClass("is-invalid");
      }
      if(peodGST == "" || isNaN(peodGST)){
         $("#custProdGST").addClass("is-invalid");
         $("#custProdGST").focus();
         return false;
      } else {
         $("#custProdGST").removeClass("is-invalid");
      } 

      //Function to actually add values into order table
      addCustomOrderToTable();

      //Reset Modal form Values after order table entry
      $('#custProdName').val('');
      $('#custProdDesc').val('');
      $('#custProdQty').val('1');
      $('#custProdAmt').val('');
      $('#custProdGST').val('0');
   }

   //Add Custom Product from Model
   function addCustomOrderToTable(){

      var pName = document.getElementById('custProdName').value;
      var pSpec = document.getElementById('custProdDesc').value;
      var pQty = parseInt(document.getElementById('custProdQty').value);
      var pprice = parseInt(document.getElementById('custProdAmt').value);
      var pGSTRate = parseInt(document.getElementById('custProdGST').value);

      if(pGSTRate != 0){
         //var pGST = pGSTRate/100 * (pprice * pQty);
         var pGST = (pprice * pQty - ((pprice* pQty) * (100/(100 + pGSTRate)))).toFixed(2);
         //pGST = Math.round(pGST * 100) / 100;
      } else {
         var pGST = 0;
      }

      $('#myCustomProduct').modal('toggle');
      var pNo = "customOrder";
      // if(serverValPost.includes(pNo)){
      //    alert('Selected Product Already Exists');
      // }else{  

            var index = $("#myOrderTable tbody tr:last-child").index();
            
            var row = '<tr>' +
                  '<td style = "display:none;"> '+pNo+' </td>' +
                  '<td> '+pName+' </td>' +
                  '<td>'+pSpec+' </td>' +
                  '<td>'+pQty+' </td>' +
                  '<td>'+(((pprice * pQty) - pGST)/pQty).toFixed(2)+' </td>' +
                  '<td>'+pGST+' </td>' +
                  '<td class = "rowTotalAmt">'+((pQty * pprice))+' </td>' +
               '<td><button type="button" class="btn btn-danger deleteRow">Delete</button>' +

               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodNo[]" value="'+pNo+'"> </td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodName[]" value="'+pName+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodSpec[]" value="'+pSpec+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodQty[]" value="'+pQty+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodAmt[]" value="'+pprice+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodGST[]" value="'+pGST+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodTotal[]" value="'+((pQty * pprice))+'"></td>' +
               '<td style = "display:none;"> <input type="hidden" id="custId" name="prodGstRate[]" value="'+pGSTRate+'"></td>' +
            '</tr>';
            $("#myOrderTable").append(row);
            
            var totalVal = document.getElementById("totalAmt").value;
            var finalVal = +totalVal + +((pprice*pQty) );
            //finalVal = Math.round(finalVal * 100) / 100;
            document.getElementById("totalAmt").value = finalVal;
            var discount = document.getElementById("discountVal").value;
            document.getElementById("finaltotal").value = finalVal - discount;
            $('#discountButton').removeAttr('disabled');
      // }

   }
</script>

<?php include '../include/footer.php'; ?>