<?php
include 'res/genral.php';
include 'include/header.php'; 
include 'include/menu.php';

if(isset($_GET['mode']) && $_GET['mode'] === 'success_order'){

    if(isset($_SESSION['ivcCardno']) && isset($_SESSION['ivcID'])){

        $cardno = $_SESSION['ivcCardno'];
        $ivcID = $_SESSION['ivcID'];
        $billType = $_SESSION['billType'];

        //header('Location: domInvoice.php?val='.$ivcID.'');

        echo '<script>
                var myWindow = window.open("domInvoice.php?val='.base64_encode($ivcID).'", "_Blank"); 
                </script>';

    } else{
        header('Location: 404.php');
    }

} else if (isset($_GET['vals'])){

    $myValJSON = json_decode(base64_decode($_GET['vals']));
    $cardno = base64_decode($myValJSON[0]);
    $ivcID = base64_decode($myValJSON[1]);
    $billType = base64_decode($myValJSON[2]);

    $_SESSION['ivcCardno'] = $cardno;
    $_SESSION['ivcID'] = $ivcID;
    $_SESSION['billType'] = $billType;

    // echo $billType;
    // die();
    header('Location: generateInvoice.php?mode=success_order');

} else{
    header('Location: 404.php');
}

?>

<!--TODO Need to finish and add invoiceMod page-->
<div class = "container">

    <div class = "container d-flex justify-content-center" style="padding-top:190px; color:#fff;">
        <!-- Print Status-->
        <div class="card card-1 bg-success" id="getPrint">
            
            <h3 class="text-center mt-4 pb-1"><p class="mt-5">Print</p></h3>
            <p style="text-align:center; margin-right:30px;"><i class="fas fa-print fa-5x mr-4" style="width:24px; height:24px"></i></p>
            <div class= "text-center mt-5">
                <!-- <button type="button" class="btn myCustButton"  data-toggle="modal" data-target="#printModel">Print Now</button> -->
                <?php echo '<a target = "_blank" href="'.APP_SERVER.'invoice/invoice.php?val='.base64_encode($cardno.'='.$ivcID.'='.$billType).'.pdf"><button type="button" class="btn myCustButton" >Print Now</button></a>'; ?>
            </div>

        </div>
    </div>

</div>