<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'constants.php';

//Logic to check if session is set or not!!
if(!isset($_SESSION['user_status']) == true){
    
    die(header('Location: ../index.php'));
}

//Array to Store errors and Display
$errors = array();

//Function to sanitiza data
    function clean($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

//Json Encode Request Data
function json_transform($request_name, $json_request){
    $name = clean($request_name);
    $request = json_encode(['name' => $name, 'param' => $json_request]);
    return $request;
}

//API request to server for data

function request_api($request_api_page ,$request_name, $json_request){

    $json_formed = json_transform($request_name, $json_request);
    $method = 'POST';
    $ch = curl_init();
    // Will return the response, if false it print the response
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Set the url
    curl_setopt($ch, CURLOPT_URL,$request_api_page);
    
    //Set authorization key
    $authorization_key = "Authorization: Bearer ".$_COOKIE['user_auth_key'];

    //curl_setopt($ch,CURLOPT_POST, $request);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json_formed);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        $authorization_key
        ));
    // Execute
    $result=curl_exec($ch);
    // Closing
    curl_close($ch);

    // Will dump a beauty json :3
    $myArray = json_decode($result, true);
    //var_dump($result);
    //Check If response exists or not
    if (array_key_exists("response",$myArray)) {
        
        $my_response = $myArray['response']['result'];
        return $my_response;
        //return "true";

    }  else if(array_key_exists("error",$myArray)) {
        $myStatus = $myArray['error']['message'];
        return "false";
    }else{
        $errors[] = 'Something Went wrong';
        return "Something Went wrong";
    }

    //var_dump($myArray);

}


//Function to genrate random string with n number of length
function genrateRandString($length){

    $binData = random_bytes($length);
    $genratedString = bin2hex($binData);

    return $genratedString;
}

function convertNumber($number){


    $number = $number;
   $no = round($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
  return $result . "Rupees  ";


}

//Success and Error response to user --------------------------------------------------------------------
function showResponseSuccess($message){
    $response = ' <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> '.$message.'.
                </div>';
    return $response;
}

function showResponseFail($message){
    $response = ' <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Fail!</strong> '.$message.'.
                </div>';
    return $response;
}

function showResponseWarning($message){
    $response = ' <div class="alert alert-warning alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning!</strong> '.$message.'.
                </div>';
    return $response;
}

function showResponseInfo($message){
    $response = ' <div class="alert alert-info alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Info!</strong> '.$message.'.
                </div>';
    return $response;
}

//Success and Error response End -------------------------------------------------------------------------------------------

//Encryption and decyption methods
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'gug&^&767^&jk';
    $secret_iv = 'sdfsff%^&#563';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

?>