<?php
//include_once('../res/genral.php');
?>

<div class="sidebar" id="myNav" onclick="myFunction()">
<a class="" href="index.php" style="padding:0px;"> <img src="<?php echo $appServer.'images/logo.jpg';?>" class="" alt="Cinque Terre" width="100%" height="45px"> </a>
  <a class="navLink " href="<?php echo $appServer.'index.php';?>"><i class="fas fa-home">&nbsp;&nbsp;&nbsp;</i> Dashboard</a>
  <a class="navLink" href="<?php echo $appServer.'_customer/customer.php';?>"><i class="fas fa-users">&nbsp;&nbsp;&nbsp;</i> Customers</a>
  <a class="navLink" href="<?php echo $appServer.'_product/product.php';?>"><i class="fas fa-layer-group">&nbsp;&nbsp;&nbsp;</i> Product</a>
  <a class="navLink" href="<?php echo $appServer.'_order/order.php';?>"><i class="fas fa-plus-square">&nbsp;&nbsp;&nbsp;</i> Orders</a>
  <a class="navLink" href="<?php echo $appServer.'_order/billing.php';?>"><i class="fas fa-file-invoice">&nbsp;&nbsp;&nbsp;</i> Billing</a>
  <a class="navLink" href="<?php echo $appServer.'_amc/service.php';?>"><i class="fas fa-tasks">&nbsp;&nbsp;&nbsp;</i> Services</a>
  <a class="navLink" href="<?php echo $appServer.'_amc/renew.php';?>"><i class="fas fa-recycle">&nbsp;&nbsp;&nbsp;</i> Renewal</a>
  <!-- <a class="navLink" href="requestMoney.php"><i class="fas fa-rupee-sign">&nbsp;&nbsp;&nbsp;</i> Request Money</a> -->
</div>
<div class="content">

  <ul class="nav justify-content-end navtop">
    <li class="nav-item">
    <a class="nav-link" href="<?php echo $appServer.'logout.php';?>" style="color:#fff;">Logout<span class="fas fa-sign-out-alt"> </span></a>
    </li>
  </ul>

