<?php
include 'res/genral.php';
include 'include/header.php'; 
include 'include/menu.php';

if(isset($_GET['mode']) && $_GET['mode'] === 'success_order'){

    if(isset($_SESSION['ivcCardno']) && isset($_SESSION['ivcOrderGrpno'])){
        //get values from 
        $cardno = $_SESSION['ivcCardno'];
        $orderGRP = $_SESSION['ivcOrderGrpno'];
        $billType = 'gst';

        //Requesting server for data about customer
        $json_request = array("cardno"=>"$cardno");
        $my_Response_Cust = request_api(CUSTOMER_API_PAGE ,"getCustDetail", $json_request); 

        //Requesting server for data about Order
        $json_request = array("groupno"=>"$orderGRP");
        $my_Response_Order = request_api(ORDER_API_PAGE ,"getModOrderWihGrpno", $json_request); 

        $myIVCBool = false;
        // echo '<br><br>';
        // var_dump($my_Response_Order);
        foreach ($my_Response_Order as $key => $value) {
          
            if($my_Response_Order[$key]['orderModBill'] != 0){
                $myIVCBool = true;
                //echo 'gst';
            }
        }

            //MOST IMPORTANT Actual Bill is genrated here
            if($myIVCBool == false){
            echo '
                    <script> 
                    var myWindow;  
                    myWindow = window.open("invoiceMod.php?val='.base64_encode($orderGRP).'", "myWindow", "width=200, height=200"); 
                    setTimeout(function () { myWindow.close();}, 1000);
                    </script>';
            }

        foreach ($my_Response_Order as $key => $value) {

            $json_request = array("oModNo"=>$my_Response_Order[$key]['oModNo'], "orderGRP" => "$orderGRP");
            $my_Response_invoice = request_api(ORDER_API_PAGE ,"updateModIvcGenrated", $json_request);

          }

    }else{
        header('Location: 404.php');
    }
} else if (isset($_GET['vals'])){

    $myValJSON = json_decode(base64_decode($_GET['vals']));
    $cardno = base64_decode($myValJSON[0]);
    $grpNo = base64_decode($myValJSON[1]);
    $billType = base64_decode($myValJSON[2]);

    $_SESSION['ivcCardno'] = $cardno;
    $_SESSION['ivcOrderGrpno'] = $grpNo;
    $_SESSION['billType'] = $billType;

    // echo $billType;
    // die();
    header('Location: modifyInvoice.php?mode=success_order');

}


?>

<!--TODO Need to finish and add invoiceMod page-->
<div class = "container">

    <div class = "container d-flex justify-content-center" style="padding-top:190px; color:#fff;">
        <!-- Print Status-->
        <div class="card card-1 bg-success" id="getPrint">
            
            <h3 class="text-center mt-4 pb-1"><p class="mt-5">Print</p></h3>
            <p style="text-align:center; margin-right:30px;"><i class="fas fa-print fa-5x mr-4" style="width:24px; height:24px"></i></p>
            <div class= "text-center mt-5">
                <!-- <button type="button" class="btn myCustButton"  data-toggle="modal" data-target="#printModel">Print Now</button> -->
                <?php echo '<a href="'.APP_SERVER.'invoice/invoice.php?val='.base64_encode($cardno.'='.$orderGRP.'='.$billType.'modBill').'.pdf"><button type="button" class="btn myCustButton" >Print Now</button></a>'; ?>
            </div>

        </div>
    </div>

</div>