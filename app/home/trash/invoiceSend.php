<?php
include 'res/genral.php';
include 'include/header.php'; 
include 'include/menu.php';

if(isset($_GET['mode']) && $_GET['mode'] === 'success_order'){

    if(isset($_SESSION['ivcCardno']) && isset($_SESSION['ivcOrderGrpno'])  && isset($_SESSION['billType'])){
        
        //get values from 
        $cardno = $_SESSION['ivcCardno'];
        $myGrpNo = $_SESSION['ivcOrderGrpno'];
        $billType = $_SESSION['billType'];
 
        $messageAttached = 'Thank You for choosing us, Please find invoice attachment here, '.APP_SERVER."invoice/invoice.php?val=".base64_encode($cardno.'='.$myGrpNo.'='.$billType).".pdf";

        //Requesting server for data about customer
        $json_request = array("cardno"=>"$cardno");
        $my_Response_Cust = request_api(CUSTOMER_API_PAGE ,"getCustDetail", $json_request); 

        //Requesting server for data about Order
        $json_request = array("groupno"=>"$myGrpNo");
        $my_Response_Order = request_api(ORDER_API_PAGE ,"getOrderWihGrpno", $json_request); 

        $myIVCBool = false;

        if ($billType === 'gst') {

          foreach ($my_Response_Order as $key => $value) {
          
            if($my_Response_Order[$key]['ivc_genrated'] != 0){
                $myIVCBool = true;
                //echo 'gst';
            }
  
          }
        } else if ($billType === 'nogst'){

          foreach ($my_Response_Order as $key => $value) {
          
            if($my_Response_Order[$key]['gst_ivc_genrated'] != 0){
                $myIVCBool = true;
                //echo 'nogst';
            }
  
          }
        } else {
          echo 'Invalid Bill Type';
        }
      
        //TODO : need to update nogst ivc genrated when ivc is genrated
        //genrating invoice with javascript 
        if($myIVCBool == false){
         

                  if($billType === 'gst'){

                    echo '
                    <script> 
                    var myWindow;  
                    myWindow = window.open("invoice.php?val='.base64_encode($myGrpNo).'", "myWindow", "width=200, height=200"); 
                    setTimeout(function () { myWindow.close();}, 1000);
                    </script>';

                  } else if ($billType === 'nogst'){
                      echo '
                      <script> 
                      var myWindow;  
                      myWindow = window.open("invoicenogst.php?val='.base64_encode($myGrpNo).'", "myWindow", "width=200, height=200"); 
                      setTimeout(function () { myWindow.close();}, 1000);
                      </script>';
                  }

                  if ($billType === 'gst') {
                    foreach ($my_Response_Order as $key => $value) {

                      $json_request = array("orderno"=>$my_Response_Order[$key]['orderno']);
                      $my_Response_invoice = request_api(ORDER_API_PAGE ,"updateIvcGenrated", $json_request);
  
                    }
                  } else if ($billType === 'nogst'){
                    foreach ($my_Response_Order as $key => $value) {

                      $json_request = array("orderno"=>$my_Response_Order[$key]['orderno']);
                      $my_Response_invoice = request_api(ORDER_API_PAGE ,"updateNoGstIvcGenrated", $json_request);
  
                    }
                  }
                  
        }
        
        // var_dump($my_Response_Cust);
        // echo "<br><br>";
        // var_dump($my_Response_Order);
        if($my_Response_Cust['email'] !== ''  && isset($_POST['formname']) == false){

            $email = $my_Response_Cust['email'];
            $json_request = null;
            $json_request = array("emailto"=>"$email", "message"=>"$messageAttached");
            $my_Response_email_Auto = request_api(ASSET_API_PAGE ,"sendInvoiceMail", $json_request);

            //Auto Genrated Email sent at the time of invoice genration
            if ($my_Response_email_Auto === 'true') {

              echo '
                    <script>
                    $(document).ready(function(){
                      $("#sendMailhead").text("Mail Sent!!");
                      $("#sendMailbody").html("<i class=\"fas fa-check-circle fa-5x mr-4\" style=\"width:24px; height:24px\"></i>");
                      $("#sendMailfooter").text("Send Again");
                    });
                    </script>';

            } else {

              echo '
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Fail!</strong> Something Went Wrong!!
                    </div>';

            }

        }

        if($my_Response_Cust['phone'] !== '' && isset($_POST['formname']) == false && $myIVCBool == false && $billType === 'nogst'){

            //TODO: Set Function to send SMS
            $phone_Auto = $my_Response_Cust['phone'];
            $json_request = array("phone"=>"$phone_Auto", "message"=>"$messageAttached");
            $my_Response_SMS_Auto = request_api(ASSET_API_PAGE ,"sendSMS", $json_request);
            //print_r($my_Response_SMS_Auto);

            $my_Response_SMS_Auto = json_decode($my_Response_SMS_Auto, true);
            //Auto sens SMS when invoice is genrated for order
            if ($my_Response_SMS_Auto['status'] === 'success') {

              echo '
                    <script>
                    $(document).ready(function(){
                      $("#sendSMShead").text("SMS Sent!!");
                      $("#sendSMSbody").html("<i class=\"fas fa-check-circle fa-5x mr-4\" style=\"width:24px; height:24px\"></i>");
                      $("#sendSMSfooter").text("Send Again");
                    });
                    </script>';

            } else {

              echo '
                      <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Fail!</strong> Something Went Wrong!!
                      </div>';
            }
                        
        } else {
          
          echo '
                    <script>
                    $(document).ready(function(){
                      $("#sendSMShead").text("SMS Sent!!");
                      $("#sendSMSbody").html("<i class=\"fas fa-check-circle fa-5x mr-4\" style=\"width:24px; height:24px\"></i>");
                      $("#sendSMSfooter").text("Send Again");
                    });
                    </script>';
                    
        } 

        //var_dump($_POST);
        //Function to check from where form data came from
        if(isset($_POST['formname'])){

            if($_POST['formname'] == 'smsform'){
              //TODO: Send SMS
              $phone = clean($_POST['phone']);
              $json_request = array("phone"=>"$phone", "message"=>"$messageAttached");
              $my_Response_SMS = request_api(ASSET_API_PAGE ,"sendSMS", $json_request);
              //print_r($my_Response_SMS);
              $my_Response_SMS = json_decode($my_Response_SMS, true);

              if ($my_Response_SMS['status'] === 'success') {
                echo '
                        <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> SMS Sent Successfully.
                      </div>';
              } else {
                echo '
                      <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Fail!</strong> Something Went Wrong!!
                      </div>';
              }
              
          } else if($_POST['formname'] == 'emailform'){
            //TODO: Send Email
            $email = clean($_POST['emailto']);
            $json_request = null;
            $json_request = array("emailto"=>"$email", "message"=>"$messageAttached");
            $my_Response_SMS = request_api(ASSET_API_PAGE ,"sendInvoiceMail", $json_request);

            if ($my_Response_SMS === 'true') {
              echo '
                      <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Success!</strong> Email Sent Successfully.
                    </div>';
            } else {
              echo '
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Fail!</strong> Something Went Wrong!!
                    </div>';
            }
            
        }  
      
        }
        
    } else{
        header('Location: 404.php');
    }

} else if (isset($_GET['vals'])){

    $myValJSON = json_decode(base64_decode($_GET['vals']));
    $cardno = base64_decode($myValJSON[0]);
    $grpNo = base64_decode($myValJSON[1]);
    $billType = base64_decode($myValJSON[2]);

    $_SESSION['ivcCardno'] = $cardno;
    $_SESSION['ivcOrderGrpno'] = $grpNo;
    $_SESSION['billType'] = $billType;

    // echo $billType;
    // die();
    header('Location: invoiceSend.php?mode=success_order');

} else{
    header('Location: 404.php');
}

?>

<div class = "container">

    <div class = "container" style="padding-top:190px; color:#fff;">
        <!-- Print Status-->
        <div class="card card-1 bg-success" id="getPrint">
            
            <h3 class="text-center mt-4 pb-1"><p class="mt-5">Print</p></h3>
            <p style="text-align:center; margin-right:30px;"><i class="fas fa-print fa-5x mr-4" style="width:24px; height:24px"></i></p>
            <div class= "text-center mt-5">
                <!-- <button type="button" class="btn myCustButton"  data-toggle="modal" data-target="#printModel">Print Now</button> -->

                <?php echo '<a href="'.APP_SERVER.'invoice/invoice.php?val='.base64_encode($cardno.'='.$myGrpNo.'='.$billType).'.pdf"><button type="button" class="btn myCustButton" >Print Now</button></a>'; ?>
            </div>

        </div>
        <!-- Email Status-->
        <div class="card card-1 bg-primary" id="sendMail">
        
            <h3 class="text-center mt-4 pb-1"><p class="mt-5" id="sendMailhead">Send Email</p></h3>
            <p style="text-align:center; margin-right:30px;" id="sendMailbody"><i class="fas fa-at fa-5x mr-4" style="width:24px; height:24px"></i></p>
            <div class= "text-center mt-5">
                <button type="button" class="btn myCustButton" id="sendMailfooter" data-toggle="modal" data-target="#emailModel">Mail Now</button>
            </div>

        </div>
        <!-- SMS Status-->
        <div class="card card-1 bg-warning" id="sendSMS">
        
            <h3 class="text-center mt-4 pb-1"><p class="mt-5" id="sendSMShead">Send SMS</p></h3>
            <p style="text-align:center; margin-right:30px;" id="sendSMSbody"><i class="fas fa-comments fa-5x mr-4" style="width:24px; height:24px"></i></p>
            <div class= "text-center mt-5">
                <button type="button" class="btn myCustButton" id="sendSMSfooter" data-toggle="modal" data-target="#smsModel">Send Now</button>
            </div>

        </div>
    </div>

</div>

<!--==============================================================   Print    ===============================================================================-->

<div class="modal" id="printModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Print Model</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!--==============================================================    Email   ============================================================================-->

<div class="modal" id="emailModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Email Model</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <div class="container">

          <form id="sendemail" action="" method="post">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"> Email </span>
            </div>
            <input type="email" class="form-control" name="emailto" required placeholder="Enter Email Address">
          </div>
          <div class="form-group">
            <label for="comment">Message:</label>
            <textarea class="form-control" name="emessage" rows="5" disabled id="comment"><?php echo $messageAttached; ?></textarea>
          </div>
          <input type="hidden" name="formname" value="emailform">

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" form="sendemail">Send</button>
      </div>
      </form>
      </div>
</div>
    </div>
  </div>
</div>

<!--=================================================================    SMS    ========================================================================-->

<div class="modal" id="smsModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">SMS Body</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <div class="container">

          <form id="sendsms" action="" method="post">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"> +91 </span>
            </div>
            <input type="text" class="form-control" name="phone" required pattern=".{10,10}" title="10 digit Phone Number" placeholder="Enter Phone No.">
          </div>
          <div class="form-group">
            <label for="comment">Message:</label>
            <textarea class="form-control" name="message" rows="5" disabled id="comment"><?php echo $messageAttached; ?></textarea>
          </div>
          <input type="hidden" name="formname" value="smsform">

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" form="sendsms">Send</button>
      </div>
      </form>
      </div>
</div>  
    </div>
  </div>
</div>

<!--=======================================================================================================================================================-->

<?php include 'include/footer.php'; ?>