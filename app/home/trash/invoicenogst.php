<?php

require 'res/fpdf/fpdf.php';
include 'res/genral.php';
include 'res/otherRes.php';

//Provide This page arguments for pdf
if(isset($_GET['val']) && $_GET['val'] !== ''){

    global $cust_detail_ivc, $order_detail_ivc, $orderGRP, $pDate, $billType, $totalAmt, $discount, $finalAmt;

    if (isset($_SESSION['billType'])) {
        $billType = $_SESSION['billType'];
    } else{
        die('unable to print invoice because billtype not set');
    }

    $orderGRP = clean(base64_decode($_GET['val']));
    $json_request = array("grpno"=>"$orderGRP");
    $cardno = request_api(ORDER_API_PAGE ,"getCardnoFromGrpNo", $json_request); 
    
    //Requesting server for data about customer
    $json_request = array("cardno"=>"$cardno");
    $cust_detail_ivc = request_api(CUSTOMER_API_PAGE ,"getCustDetail", $json_request); 
    

    //Requesting server for data about Order
    $json_request = array("groupno"=>"$orderGRP");
    $order_detail_ivc = request_api(ORDER_API_PAGE ,"getOrderWihGrpno", $json_request); 

    //var_dump($order_detail_ivc);

    $pdate;
    foreach ($order_detail_ivc as $key => $value) {
        $pDate = $order_detail_ivc[$key]['pdate'];
    }

    //Get initial vales of following variables
    foreach($order_detail_ivc as $key => $value){
        $totalAmt = $order_detail_ivc[$key]['total'];
        $discount = $order_detail_ivc[$key]['discount'];
        $finalAmt = $order_detail_ivc[$key]['finalAmt'];
    }
    
}

class myInvoice extends FPDF{
    
    
    function header(){

        global $cust_detail_ivc, $order_detail_ivc, $orderGRP, $pDate, $billType, $totalAmt, $discount, $finalAmt;
        // $custDetail;
        // $prod_bill_detail;

        $this->SetFont('Arial', 'B', 28);
        $this->Cell(200,10,ORGANIZATION_BILL_NAME,0,0,'C');
        $this->Line(10, 20, 200, 20);
        $this->Line(10, 28, 200, 28);
        $this->Line(10, 36, 200, 36);
        $this->Ln();
        $this->SetFont('Arial', '', 11);
        $this->Cell(15,9,ORGANIZATION_BILL_ADDRESS,0);
        $this->Cell(15,25,ORGANIZATION_PHONE_NUMBER,0);
        $this->setX(115);
        $this->Cell(50,25,ORGANIZATION_EMAIL_ADDRESS,0);
        //$this->Image('images/logoI.png', 140, 6, 60, 40);
        $this->Ln();
        $this->setY(55);
        $this->Cell(190,50,'' ,1,0,'');
        //$this->Cell(70,10,'To,' ,0,0,'C');
        $this->SetFont('Arial', 'B', 10);
        $this->setY(63);
        $this->Cell(70,10,'Name : '.$cust_detail_ivc['name'],0,0,'L');
        $this->SetFont('Arial', '', 10);
        $this->setY(73);
        $this->MultiCell(80,5,'Address : '.$cust_detail_ivc['address']." ".$cust_detail_ivc['city']." ".$cust_detail_ivc['zipcode'],0);
        $this->setY(83);
        if ($billType === 'gst') {
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(70,10,'GSTIN : '.$cust_detail_ivc['gstno'],0,0,'L');
        }
        
        $this->setY(55);
        $this->setX(150);
        $this->Cell(50,10,'Bill No.: #'.$orderGRP,1,0,'L');
        $this->setY(65);
        $this->setX(150);
        $tDate=date("d/m/Y", strtotime($pDate));
        $this->Cell(50,10,'Bill Date.: '.$tDate ,1,0,'L');
        $this->Ln();
        $this->SetFont('Arial','',20);
        $this->setY(120);
        $this->setX(90);
        $this->Cell(0,0,'Invoice',0,'C');
    
        //This is actual billing start
        $this->Line(10, 103+30, 200, 103+30);
        $this->Line(10, 103+37, 200, 103+37);
        $this->Line(130, 103+30, 130, 103+130);
        $this->Line(165, 103+30, 165, 103+130);
        
        //Line above gst
        if ($billType === 'gst' && $discount === 0) {
            $this->Line(130, 103+110, 200, 103+110);
        } else if ($billType === 'gst' && $discount !== 0){
            $this->Line(130, 103+100, 200, 103+100);
            $this->Line(130, 103+110, 200, 103+110);
        } else if($billType !== 'gst' && $discount !== 0){
            $this->Line(130, 103+110, 200, 103+110);
        }

        // $this->Line(130, 103+110, 200, 103+110);

        $this->Line(130, 103+120, 200, 103+120);
        $this->Line(10, 103+130, 200, 103+130);
        
        $this->setY(136);
        $this->setX(60);
        $this->SetFont('Arial','B',10);
        $this->Cell(50,0,'Particulars' ,'C');
        $this->setX(143);
        $this->Cell(50,0,'Qty' ,'C');
        $this->setX(176);
        $this->Cell(50,0,'Amount' ,'C');

        if ($billType === 'gst' && $discount === 0) {
            //GST Txt
            $this->setY(218);
            $this->setX(136);
            $this->SetFont('Arial','I',10);
            $this->Cell(50,0,'GST@18%' ,'C');
        } else if ($billType === 'gst' && $discount !== 0){

            $this->setY(208);
            $this->setX(136);
            $this->SetFont('Arial','I',10);
            $this->Cell(50,0,'Discount' ,'C');

            $this->setY(218);
            $this->setX(136);
            $this->SetFont('Arial','I',10);
            $this->Cell(50,0,'GST@18%' ,'C');

        } else if($billType !== 'gst' && $discount !== 0){

            $this->setY(218);
            $this->setX(136);
            $this->SetFont('Arial','I',10);
            $this->Cell(50,0,'Discount' ,'C');

        }

     
        $this->setY(228);
        $this->setX(143);
        $this->SetFont('Arial','I',12);
        $this->Cell(50,0,'Total' ,'C');

        $this->setY(240);
        $this->setX(10);
        $this->SetFont('Arial','BU',12);
        $this->Cell(50,0,'In Words : ' ,'C');

        $this->setY(265);
        $this->setX(10);
        $this->SetFont('Arial','BU',11);
        $this->Cell(50,0,'Note: ' ,'C');

        $this->setY(265);
        $this->setX(21);
        $this->SetFont('Arial','',9);
        $this->Cell(50,0,'Goods once sold will not be taken back. Warrenty will be given by brand company only. Cheque Bounces Rs. 500/- Charges' ,'C');        

        // $this->setY(270);
        // $this->setX(21);
        // $this->SetFont('Arial','',9);
        // $this->Cell(50,0,'Cheque Bounces Rs. 500/- Charges' ,'C');


        
    }

    function viewTable(){

        global $cust_detail_ivc, $order_detail_ivc, $orderGRP, $pDate, $billType, $totalAmt, $discount, $finalAmt;

        $json_request = array("None"=>"none");
        //var_dump($order_detail_ivc);



        // $var = 0;
        // $prod_name = array();
        // foreach($prod_bill_detail as $oneRow){
        //     $prod_name[$var] = $oneRow['prod_name']; //and all other stuff that you want.
        //     $var++;
        // }
        // $var1 = 0;
        // $prod_detl = array();
        // foreach($prod_bill_detail as $oneRow){
        //     $prod_detl[$var1] = $oneRow['prod_name']; //and all other stuff that you want.
        //     $var1++;
        // }

        $this->setY(140);
        //$this->setX(60);
        $this->SetFont('Arial', '', 10);
        $total = 0;
        foreach($order_detail_ivc as $key => $value){
            $this->Cell(135,10, $order_detail_ivc[$key]['prod_name'].": ".$order_detail_ivc[$key]['spec'] ,0,0,'L');
            $this->Cell(35,10, $order_detail_ivc[$key]['qty'] ,0,0,'L');
            $total += $order_detail_ivc[$key]['price'] * $order_detail_ivc[$key]['qty'];
            $this->Cell(50,10, $order_detail_ivc[$key]['price'] * $order_detail_ivc[$key]['qty'] ,0,0,'L');
            $this->Ln();
        }
        
        $tax = 0;
        if ($billType === 'gst' && $discount === 0) {
            $this->setY(218);
            $this->setX(180);
            $tax = 18/100*$total;
            $this->Cell(50,0, '- '.$tax ,'C');
        } else if ($billType === 'gst' && $discount !== 0){

            $this->setY(208);
            $this->setX(180);
            //$tax = 10/100*$total;
            $this->Cell(50,0, '- '.$discount ,'C');

            $this->setY(218);
            $this->setX(180);
            $tax = 18/100*$total;
            $this->Cell(50,0, '- '.$tax ,'C');
           
        } else if($billType !== 'gst' && $discount !== 0){

            $this->setY(218);
            $this->setX(180);
            //$tax = 10/100*$total;
            $this->Cell(50,0, '- '.$discount ,'C');

        }
        

        $this->setY(228);
        $this->setX(180);
        $this->Cell(50,0, $total-$tax-$discount."" ,'C');

        $this->setY(240);
        $this->setX(35);
        $this->SetFont('times', 'UI', 11);
        $this->Cell(50,0, strtoupper(convertNumber($total-$tax-$discount)) ,'C');
    }
    
}


 

$pdf = new myInvoice();
$pdf->AliasNbPages();
$pdf->AddPage('P', 'A4', 0);
$pdf->viewTable();
//$pdf->Output();

$name = base64_encode($cardno.'='.$orderGRP.'='.$billType);

$pdf->Output(INVOICE_GENRATE_OUTPUT.$name.'.pdf','F');

?>