<?php

require 'res/fpdf/fpdf.php';
include 'res/genral.php';
include 'res/otherRes.php';

//Provide This page arguments for pdf
if(isset($_GET['val']) && $_GET['val'] !== ''){

    global $cust_detail_ivc, $order_detail_ivc, $orderGRP, $pDate, $billType, $totalAmt, $discount, $finalAmt;

    if (isset($_SESSION['billType'])) {
        $billType = $_SESSION['billType'];
    } else{
        die('unable to print invoice because billtype not set');
    }

    $orderGRP = clean(base64_decode($_GET['val']));
    $json_request = array("grpno"=>"$orderGRP");
    $cardno = request_api(ORDER_API_PAGE ,"getCardnoFromGrpNo", $json_request); 
    
    //Requesting server for data about customer
    $json_request = array("cardno"=>"$cardno");
    $cust_detail_ivc = request_api(CUSTOMER_API_PAGE ,"getCustDetail", $json_request); 
    

    //Requesting server for data about Order
    $json_request = array("groupno"=>"$orderGRP");
    $order_detail_ivc = request_api(ORDER_API_PAGE ,"getOrderWihGrpno", $json_request); 

    //var_dump($order_detail_ivc);

    $pdate;
    foreach ($order_detail_ivc as $key => $value) {
        $pDate = $order_detail_ivc[$key]['pdate'];
    }

    //Get initial vales of following variables
    foreach($order_detail_ivc as $key => $value){
        $totalAmt = $order_detail_ivc[$key]['total'];
        $discount = $order_detail_ivc[$key]['discount'];
        $finalAmt = $order_detail_ivc[$key]['finalAmt'];
    }
    
}

class myInvoice extends FPDF{
    
    
    function header(){

        global $cust_detail_ivc, $order_detail_ivc, $orderGRP, $pDate, $billType, $totalAmt, $discount, $finalAmt;
        // $custDetail;
        // $prod_bill_detail;

        $this->SetFont('Arial', 'B', 28);
        $this->Cell(200,10,ORGANIZATION_BILL_NAME,0,0,'C');
        $this->Line(10, 20, 200, 20);
        $this->Line(10, 28, 200, 28);
        $this->Line(10, 36, 200, 36);
        $this->Ln();
        $this->SetFont('Arial', '', 11);
        $this->Cell(15,9,ORGANIZATION_BILL_ADDRESS,0);
        $this->Cell(15,25,ORGANIZATION_PHONE_NUMBER,0);
        $this->setX(115);
        $this->Cell(50,25,ORGANIZATION_EMAIL_ADDRESS,0);
        //$this->Image('images/logoI.png', 140, 6, 60, 40);
        $this->Ln();
        $this->setY(40);
        $this->Cell(190,40,'' ,1,0,'');
        //$this->Cell(70,10,'To,' ,0,0,'C');
        $this->SetFont('Arial', 'B', 11);
        $this->setY(43);
        $this->Cell(70,10,'Name : '.$cust_detail_ivc['name'],0,0,'L');
        $this->SetFont('Arial', '', 10);
        $this->setY(53);
        $this->MultiCell(110,5,'Address : '.$cust_detail_ivc['address']." ".$cust_detail_ivc['city']." ".$cust_detail_ivc['zipcode'],0);
        $this->setY(66);
        if ($billType === 'gst') {
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(70,10,'GSTIN : '.$cust_detail_ivc['gstno'],0,0,'L');
        }
        
        $this->setY(40);
        $this->setX(150);
        $this->Cell(50,10,'Bill No.: #'.$orderGRP,1,0,'L');
        $this->setY(50);
        $this->setX(150);
        $tDate=date("d/m/Y", strtotime($pDate));
        $this->Cell(50,10,'Bill Date.: '.$tDate ,1,0,'L');
        $this->Ln();
        $this->SetFont('Arial','',20);
        $this->setY(90);
        $this->setX(90);
        $this->Cell(0,0,'Invoice',0,'C');
    
        //This is actual billing start
        $this->Line(10, 70+30, 200, 70+30);
        $this->Line(10, 70+37, 200, 70+37);

        $this->Line(100, 70+30, 100, 70+130); //Line before HSN
        $this->Line(120, 70+30, 120, 70+130); //Line after HSN
        $this->Line(133, 70+30, 133, 70+130); //Line after qty
        $this->Line(146, 70+30, 146, 70+130); //Line after igst
        $this->Line(159, 70+30, 159, 70+110); //Line after cgst
        $this->Line(172, 70+30, 172, 70+130); //Line after sgst
        //$this->Line(130, 70+30, 130, 70+130);
        //$this->Line(165, 70+30, 165, 70+130);
        
        //Line above gst
        if ($discount !== 0) {
            $this->Line(146, 70+110, 200, 70+110);
        }
        //  else if ($billType === 'gst' && $discount !== 0){
        //     $this->Line(146, 70+100, 200, 70+100);
        //     $this->Line(146, 70+110, 200, 70+110);
        // } else if($billType !== 'gst' && $discount !== 0){
        //     $this->Line(146, 70+110, 200, 70+110);
        // }

        // $this->Line(146, 70+110, 200, 70+110);

        $this->Line(146, 70+120, 200, 70+120);
        $this->Line(10, 70+130, 200, 70+130);
        
        $this->setY(103);
        $this->setX(45);
        $this->SetFont('Arial','B',10);
        $this->Cell(50,0,'Particulars' ,'C');
        $this->setX(105);
        $this->Cell(50,0,'HSN' ,'C');
        $this->setX(123);
        $this->Cell(50,0,'Qty' ,'C');
        $this->setX(135);
        $this->Cell(50,0,'Price' ,'C');
        $this->setX(147);
        $this->Cell(50,0,'CGST' ,'C');
        $this->setX(160);
        $this->Cell(50,0,'SGST' ,'C');
        $this->setX(178);
        $this->Cell(50,0,'Amount' ,'C');

        if ($discount !== 0) {
            //GST Txt
            $this->setY(185);
            $this->setX(150);
            $this->SetFont('Arial','I',10);
            $this->Cell(50,0,'Discount' ,'C');
        } 
        // else if ($billType === 'gst' && $discount !== 0){

        //     $this->setY(175);
        //     $this->setX(136);
        //     $this->SetFont('Arial','I',10);
        //     $this->Cell(50,0,'Discount' ,'C');

        //     $this->setY(185);
        //     $this->setX(136);
        //     $this->SetFont('Arial','I',10);
        //     $this->Cell(50,0,'GST@18%' ,'C');

        // } else if($billType !== 'gst' && $discount !== 0){

        //     $this->setY(185);
        //     $this->setX(136);
        //     $this->SetFont('Arial','I',10);
        //     $this->Cell(50,0,'Discount' ,'C');

        // }

     
        $this->setY(195);
        $this->setX(152);
        $this->SetFont('Arial','I',12);
        $this->Cell(50,0,'Total' ,'C');

        $this->setY(245);
        $this->setX(10);
        $this->SetFont('Arial','BU',12);
        $this->Cell(50,0,'In Words : ' ,'C');

        $this->setY(280);
        $this->setX(10);
        $this->SetFont('Arial','BU',11);
        $this->Cell(50,0,'Note: ' ,'C');

        $this->setY(280);
        $this->setX(21);
        $this->SetFont('Arial','',9);
        $this->Cell(50,0,'Goods once sold will not be taken back. Warrenty will be given by brand company only. Cheque Bounces Rs. 500/- Charges' ,'C');     
        
        //vendor detail
        //$this->setY(220);
        $this->Line(10, 205, 200, 205);
        $this->Line(10, 215, 200, 215);
        $this->Line(10, 235, 200, 235);

        $this->setY(210);
        $this->setX(10);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,'GSTIN No.: ' ,'C');

        $this->setY(210);
        $this->setX(37);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0, VENDORGSTIN,'C');

        $this->setY(220);
        $this->setX(10);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,'A/c Name : ' ,'C');

        $this->setY(220);
        $this->setX(35);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0, VENDORACNAME,'C');

        $this->setY(220);
        $this->setX(110);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,'A/c No : ' ,'C');

        $this->setY(220);
        $this->setX(130);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,VENDORACNO,'C');

        $this->setY(230);
        $this->setX(10);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,'Bank : ' ,'C');

        $this->setY(230);
        $this->setX(27);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,VENDORBANKNAME,'C');

        $this->setY(230);
        $this->setX(110);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,'IFSC Code : ' ,'C');

        $this->setY(230);
        $this->setX(137);
        $this->SetFont('Arial','B',12);
        $this->Cell(50,0,VENDORIFSC,'C');

        // $this->setY(270);
        // $this->setX(21);
        // $this->SetFont('Arial','',9);
        // $this->Cell(50,0,'Cheque Bounces Rs. 500/- Charges' ,'C');


        
    }

    function viewTable(){

        global $cust_detail_ivc, $order_detail_ivc, $orderGRP, $pDate, $billType, $totalAmt, $discount, $finalAmt;

        $json_request = array("None"=>"none");
        //var_dump($order_detail_ivc);



        // $var = 0;
        // $prod_name = array();
        // foreach($prod_bill_detail as $oneRow){
        //     $prod_name[$var] = $oneRow['prod_name']; //and all other stuff that you want.
        //     $var++;
        // }
        // $var1 = 0;
        // $prod_detl = array();
        // foreach($prod_bill_detail as $oneRow){
        //     $prod_detl[$var1] = $oneRow['prod_name']; //and all other stuff that you want.
        //     $var1++;
        // }

        $this->setY(107);
        //$this->setX(60);
        $this->SetFont('Arial', '', 10);
        $total = $taxValue = $tax = 0;
        foreach($order_detail_ivc as $key => $value){
            $this->Cell(95,10, $order_detail_ivc[$key]['prod_name'].": ".$order_detail_ivc[$key]['spec'] ,0,0,'L');
            $this->Cell(18,10, $order_detail_ivc[$key]['hsn_no'] ,0,0,'L');
            $this->Cell(11,10, $order_detail_ivc[$key]['qty'] ,0,0,'L');

            $taxRate = $order_detail_ivc[$key]['gstrate'];
            $taxValue = $taxRate/100 * $order_detail_ivc[$key]['price'];
            $price = $order_detail_ivc[$key]['price'] - $taxValue;
            $total += $order_detail_ivc[$key]['price'] * $order_detail_ivc[$key]['qty'];
            
            //$taxValue = $taxRate/100*$total;
            $tax = $tax + $taxValue;
            
            $this->Cell(12,10, $price ,0,0,'L');
            $this->Cell(13,10, $taxValue/2 ,0,0,'L');
            $this->Cell(20,10, $taxValue/2 ,0,0,'L');
            
            $this->Cell(13,10, $order_detail_ivc[$key]['price'] * $order_detail_ivc[$key]['qty'] ,0,0,'L');
            $this->Ln();
        }
        
        
        if ($discount !== 0) {
            $this->setY(185);
            $this->setX(180);
            $this->Cell(50,0, '- '.$discount ,'C');
        } 
        // else if ($billType === 'gst' && $discount !== 0){

        //     $this->setY(175);
        //     $this->setX(180);
        //     //$tax = 10/100*$total;
        //     $this->Cell(50,0, '- '.$discount ,'C');

        //     $this->setY(185);
        //     $this->setX(180);
        //     $tax = 18/100*$total;
        //     $this->Cell(50,0, '- '.$tax ,'C');
           
        // } else if($billType !== 'gst' && $discount !== 0){

        //     $this->setY(185);
        //     $this->setX(180);
        //     //$tax = 10/100*$total;
        //     $this->Cell(50,0, '- '.$discount ,'C');

        // }
        

        $this->setY(195);
        $this->setX(180);
        $this->Cell(50,0, $total-$discount."" ,'C');

        $this->setY(245);
        $this->setX(35);
        $this->SetFont('times', 'UI', 11);
        $this->Cell(50,0, strtoupper(convertNumber($total-$discount)) ,'C');
    }
    
}


 

$pdf = new myInvoice();
$pdf->AliasNbPages();
$pdf->AddPage('P', 'A4', 0);
$pdf->viewTable();
//$pdf->Output();

$name = base64_encode($cardno.'='.$orderGRP.'='.$billType);

$pdf->Output(INVOICE_GENRATE_OUTPUT.$name.'.pdf','F');

?>