<?php
include 'res/genral.php';
include 'include/header.php'; 
include 'include/menu.php';

$request_api_page = CUSTOMER_API_PAGE;
$request_name = "getAddressBookCount";
$json_request = array("None"=>"none");

?>

<div class="container">
  
  <div class="row four-grids">
    
    <div class="col-md-3 four-grid mouseOver" id = "allCust">
						<div class="four-agileits">
							<div class="icon">
								<i class="glyphicon glyphicon-user" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Customers</h3>
								<h4> <?php echo request_api($request_api_page ,$request_name, $json_request); ?>  </h4>
								
							</div>
							
						</div>
					</div>
          <div class="col-md-3 four-grid mouseOver" id = "penServ">
						<div class="four-agileinfo">
							<div class="icon">
								<i class="glyphicon glyphicon-user" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Pending Service</h3>
								<h4> <?php echo request_api(SERVICE_API_PAGE ,"pendingServiceCount", $json_request); ?>  </h4>
								
							</div>
							
						</div>
					</div>
          <div class="col-md-3 four-grid mouseOver" id = "penRenew">
						<div class="four-succ">
							<div class="icon">
								<i class="glyphicon glyphicon-user" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Pending Renewal</h3>
								<h4> <?php echo request_api(SERVICE_API_PAGE ,"pendingRenewalCount", $json_request); ?>  </h4>
								
							</div>
							
						</div>
					</div>
          <div class="col-md-3 four-grid mouseOver" id = "penServRenew">
						<div class="four-wthree">
							<div class="icon">
								<i class="glyphicon glyphicon-user" aria-hidden="true"></i>
							</div>
							<div class="four-text">
								<h3>Pending Serv+Renew</h3>
								<h4> <?php echo request_api(SERVICE_API_PAGE ,"pendingServiceRenewalCount", $json_request); ?> </h4>
								
							</div>
							
						</div>
					</div>
    

  </div>

<div class="container">
<?php

echo '<hr><h4 align="center" class="m-4 "><i>Pending Services</i></h4>';
echo '<div class="table-responsive">
    <table class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>Card No.</th>
        <th>Name</th>
        <th>Product</th>
        <th>Service Date</th>
      </tr>
    </thead>
 <tbody>';

 $my_Response_Service = request_api(SERVICE_API_PAGE ,"pendingService", $json_request); 

 foreach ($my_Response_Service as $key => $value) {

	$serv_date = date("d/m/Y", strtotime($my_Response_Service[$key]['serv_date']));

      echo  '<tr class="table-danger"><td class="getCNo userCardNoService">'.$my_Response_Service[$key]['cardno'].'</td><td>'.$my_Response_Service[$key]['name'].'</td><td>'.$my_Response_Service[$key]['prod_name'].'</td><td>'.$serv_date.'</td></tr>';
 }

 echo '</tbody></table></div>';


?>


</div>

<div class="container">
	
	<?php
	echo '<hr><h4 align="center" class="m-4"><i>Pending Renewal</i></h4>';
	
	echo '<div class="table-responsive">
		<table class="table table-hover">
		<thead class="thead-dark">
		  <tr>
			<th>Card No.</th>
			<th>Name</th>
			<th>Product</th>
			<th>Renew Date</th>
		  </tr>
		</thead>
	 <tbody>';
	 
	 $my_Response_Renewal = request_api(SERVICE_API_PAGE ,"pendingRenewal", $json_request); 
	 
	 foreach ($my_Response_Renewal as $key => $value) {

		$renew_date = date("d/m/Y", strtotime($my_Response_Renewal[$key]['renew_date']));
		echo  '<tr class="table-danger"><td class = "getCNo userCardNoRenew">'.$my_Response_Renewal[$key]['cardno'].'</td><td>'.$my_Response_Renewal[$key]['name'].'</td><td>'.$my_Response_Renewal[$key]['prod_name'].'</td><td>'.$renew_date.'</td></tr>';
	}
	
	echo '</tbody></table></div>';
 ?>
 

 </div>
</div>

<script>

	$(".userCardNoService").click(function() {
	
	var $row = $(this).closest("tr");    // Find the row
	var text = $row.find(".getCNo").text(); // Find the text
	var text = window.btoa(text);
	window.location.href = "_customer/cust_detail.php?cardno="+text;
	
	});

	$(".userCardNoRenew").click(function() {

	var $row = $(this).closest("tr");    // Find the row
	var text = $row.find(".getCNo").text(); // Find the text
	var text = window.btoa(text);
	window.location.href = "cust_detail.php?cardno="+text;

	});

	$(".getCNo").hover(function() {
    $(this).css('cursor','pointer');
	}, function() {
		$(this).css('cursor','auto');
	});

	$(".mouseOver").hover(function() {
    $(this).css('cursor','pointer');
	}, function() {
		$(this).css('cursor','auto');
	});

	document.getElementById("allCust").onclick = function () {
        location.href = "_customer/customer.php";
    };

	document.getElementById("penServ").onclick = function () {
        location.href = "_amc/service.php";
    };

	document.getElementById("penRenew").onclick = function () {
        location.href = "_amc/renew.php";
    };

	document.getElementById("penServRenew").onclick = function () {
        location.href = "";
    };
</script>

<?php include 'include/footer.php'; ?>
