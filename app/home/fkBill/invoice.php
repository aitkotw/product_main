<?php

require '../res/dompdf/vendor/autoload.php';
include '../res/genral.php';
 
if(isset($_GET['val']) && $_GET['val'] !== ''){

    $ivcID = clean(base64_decode($_GET['val']));
    $cardno = $_SESSION['ivcCardno'];
    $billType = $_SESSION['billType'];
    $json_request = array("ivcID"=>"$ivcID", "cardno" => "$cardno");
    $custBillDtl = request_api(FKBILL_API_PAGE ,"getCustDetailForIVC", $json_request); 

    $billDate = strtotime($custBillDtl['date']);
    $billDate = date('d/m/Y', $billDate);

    $json_request = array("ivcID"=>"$ivcID");
    $getIvcItemDtl = request_api(FKBILL_API_PAGE ,"getIvcItemDtl", $json_request); 
    
//    foreach($getIvcItemDtl as $key => $value){
//     echo $getIvcItemDtl[$key]['prodDesc'];
//    }
//    die();
} else {
    die('Something Went Wrong');
}
// echo $_GET['val'];

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();

ob_start();
?>


<!--Html Source to write start-------------------------------------------------------------------------------------------------------------->
<html>
<head>
<link type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
<style>
    .page-break{
        page-break-before: always;
    }
    .default-header{
        position : fixed;
        top : 0cm;
        /* color : #fff;
        background-color : #007bff; */
        color : #000;
        background-color : #fff;
        font-family: Arial, Helvetica, sans-serif;
    }
    .default-footer{
        position : fixed;
        bottom : 2cm;
        color : #000;
        background-color : #e6e6e6;
        font-family: Arial, Helvetica, sans-serif;
        padding : 0.5cm;
    }
    .header-name{
        text-align: center;
        font-size: 0.8cm;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        padding-top: 0.2cm;
        margin-bottom: 0px;
    }
    .line{
        width: 90%;
        margin-left: 5%;
        margin-right:5%;
        border-bottom: 1.3px solid #000;
        position: absolute;
        align: center;
    }
    .header-body {
        margin: 0px;
        width: 90%;
        margin-left: 6%;
        margin-right:4%;
        margin-top:0.1cm;
    }
    html {
        margin : 0cm;
    }
    body{
        margin-top : 3.5cm;
        font-family: Arial, Helvetica, sans-serif;
    }

    .customer-info{
        background-color: #fff;
        /* background-color: #e5f2ff; */
        width:100%;
        margin-left:1.5cm;
        margin-right:1.5cm;
        padding:0.5cm;
        border: 0.5px solid #000;
        border-radius:8px;
    }
    .vendor-info{
        /* background-color: #dbffdb; */
        background-color: #fff;
        border:0.5px solid #000;
        width:100%;
        margin-left:1cm;
        margin-right:1cm;
        padding:0.5cm;
        border-radius:8px;
        font-size:17px;
    }
    .tableClassTotal{
        background-color: #e5f2ff;
    }


</style>
</head>
<body>

<div class = "default-header">

    <p class = "header-name"><?php echo ORGANIZATION_BILL_NAME; ?></p> 
    <div class="line" style = "height: 1.4cm;"></div>
    <p class = "header-body"> <?php echo ORGANIZATION_BILL_ADDRESS; ?></p>
    <div class="line" style = "height: 2.3cm;"></div>

    <div style = "margin-top:5px; margin-bottom:5px;"><table width = "90%" align = "center">
    <tr>
        <td align = "center" width ="50%"><?php echo ORGANIZATION_PHONE_NUMBER; ?></td>
        <td align = "center" width ="50%"><?php echo ORGANIZATION_EMAIL_ADDRESS; ?></td>
    </tr></table></div>
    <div class="line" style = "height: 3.1cm;"></div>
</div>
<!----------------------------     Header Close    --------------------------------->

<div style = "text-align: right; margin-right:1cm; margin-top:0cm;margin-bottom:0.3cm"><b><u>Date : <?php echo $billDate; ?></u></b></div>

<div class = "customer-info">

    <table style="width:100%">
        <tr>
        <td width = "70%"><b>Name :</b> <?php echo $custBillDtl['name'] ?></td>
        <td width="30%" align = "center" style = "border: 0.1px solid #000; background-color:#fff;"><b>Bill No : <?php echo $custBillDtl['id'] ?></b></td>
        </tr>
        <?php 
        if($billType == 'gst'){ 
            echo ' <tr>
            <td colspan = "2"><b>GSTIN : </b>'.$custBillDtl['gstno'].'</td></tr>'; }
        ?>
       
        <tr>
        <td colspan = "2"><b>Phone :</b> +91 <?php echo $custBillDtl['phone'] ?></td></tr>
    </table>


    <table>
        <tr>
        <td width = "10%"><b>Address : </b></td>
        <td width = "90%"> <?php echo $custBillDtl['address'] ?></td></tr>

    </table>
</div>

<div style = "text-align:center; margin:0.5cm">
    <h2>Invoice</h2>

    <?php

    if($billType == 'gst'){
        ?>
        <table class="table table-bordered">
        <thead class = "thead-light" style = "">
        <tr>
            <th width = "50%" style = "text-align:center;">Particulars</th>
            <th>HSN</th>
            <th>Qty</th>
            <th>Value</th>
            <th>Cgst</th>
            <th>Sgst</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php

            foreach($getIvcItemDtl as $key => $value){
                // if($getIvcItemDtl[$key]['prodID'] == 'customOrder'){
                //     $getIvcItemDtl[$key]['hsn_no'] = "";
                // }
                //(pprice * pQty - ((pprice* pQty) * (100/(100 + pGSTRate)))).toFixed(2)
                $gstValue = ($getIvcItemDtl[$key]['prodAmt'] * $getIvcItemDtl[$key]['qty'] - (($getIvcItemDtl[$key]['prodAmt'] * $getIvcItemDtl[$key]['qty']) * (100/(100 + $getIvcItemDtl[$key]['gstRate']))));
                //$gstValue = number_format((float)$gstValue, 2, '.', '');
                $gstValue = round($gstValue, 2);
                //$gstValue = $getIvcItemDtl[$key]['gstrate']/100 * $getIvcItemDtl[$key]['prodAmt'];
                $prodVal = (($getIvcItemDtl[$key]['prodAmt'] * $getIvcItemDtl[$key]['qty']) - $gstValue)/$getIvcItemDtl[$key]['qty'];
                $prodVal = round($prodVal, 2);
                echo '<tr>
                            <td width = "50%">'.$getIvcItemDtl[$key]['prodName'].' '.$getIvcItemDtl[$key]['prodDesc'].'</td>
                            <td>'.$getIvcItemDtl[$key]['hsn_no'].'</td>
                            <td>'.$getIvcItemDtl[$key]['qty'].'</td>
                            <td>'.$prodVal.'</td>
                            <td>'. round($gstValue/2, 2) .'</td>
                            <td>'. round($gstValue/2, 2) .'</td>
                            <td>'.$getIvcItemDtl[$key]['qty']*$getIvcItemDtl[$key]['prodAmt'].'</td>
                        </tr>';
            }
        ?>
        
        </tbody>
    </table>

    <?php 
    } else if ($billType == 'nogst'){
        ?>
        <table class="table table-bordered">
        <thead class = "thead-light" style = "">
        <tr>
            <th width = "60%" style = "text-align:center;">Particulars</th>
            <th>Qty</th>
            <th>Value</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php

            foreach($getIvcItemDtl as $key => $value){
                // if($getIvcItemDtl[$key]['prodID'] == 'customOrder'){
                //     $getIvcItemDtl[$key]['hsn_no'] = "";
                // }

                //$gstValue = $getIvcItemDtl[$key]['gstrate']/100 * $getIvcItemDtl[$key]['prodAmt'];
                $prodVal = $getIvcItemDtl[$key]['prodAmt'];
                echo '<tr>
                            <td width = "60%">'.$getIvcItemDtl[$key]['prodName'].' '.$getIvcItemDtl[$key]['prodDesc'].'</td>
                            <td>'.$getIvcItemDtl[$key]['qty'].'</td>
                            <td>'.$prodVal.'</td>
                            <td>'.$getIvcItemDtl[$key]['qty']*$getIvcItemDtl[$key]['prodAmt'].'</td>
                        </tr>';
            }
        ?>
        
        </tbody>
    </table>

    <?php } ?>

    <!-- <table class="table table-bordered">
        <thead class = "thead-light" style = "">
        <tr>
            <th width = "50%" style = "text-align:center;">Particulars</th>
            <th>HSN</th>
            <th>Qty</th>
            <th>Value</th>
            <th>Cgst</th>
            <th>Sgst</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php

            // foreach($getIvcItemDtl as $key => $value){
            //     // if($getIvcItemDtl[$key]['prodID'] == 'customOrder'){
            //     //     $getIvcItemDtl[$key]['hsn_no'] = "";
            //     // }

            //     $gstValue = $getIvcItemDtl[$key]['gstrate']/100 * $getIvcItemDtl[$key]['prodAmt'];
            //     $stuff = "myVal";
            //     $prodVal = $getIvcItemDtl[$key]['prodAmt'] - $gstValue;
            //     echo '<tr>
            //                 <td width = "50%">'.$getIvcItemDtl[$key]['prodDesc'].'</td>
            //                 <td>'.$getIvcItemDtl[$key]['hsn_no'].'</td>
            //                 <td>'.$getIvcItemDtl[$key]['qty'].'</td>
            //                 <td>'.$prodVal.'</td>
            //                 <td>'. $gstValue/2 .'</td>
            //                 <td>'. $gstValue/2 .'</td>
            //                 <td>'.$getIvcItemDtl[$key]['qty']*$getIvcItemDtl[$key]['prodAmt'].'</td>
            //             </tr>';
            // }
        ?>
        
        </tbody>
    </table> -->

</div>

<!-------------Floating Subtotal Div------------->
<div width = "100%" style = "display : block">
<div style = "float:right; margin-right: 1.5cm; background-color:#fff; border: 0.5px solid #000; border-radius:8px; padding:10px;">
    <?php

        if($custBillDtl['discount'] !== "0"){
            echo '<div style = "border-bottom: 1px solid #000; padding : 10px;"><b>Discount :</b> - '.$custBillDtl['discount'].'</div>';
        }

    ?>
    
    <div style = "padding:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total :</b> <?php echo $custBillDtl['ivcAmt']; ?></div>
</div>
</div>
<div style = "margin-left:1cm; text-align:left;"><p><b><u>In Words :</u> </b><br> <?php echo strtoupper(convertNumber($custBillDtl['ivcAmt'])); ?></p></div>

<br><br><br><br><hr>
<br>

<!-- <div>
this is my close div
</div> -->

<?php
if($billType == 'gst'){ ?>

<div class = "vendor-info">
    <table style="width:100%;">
        <tr>
        <td colspan = "2"><b>GSTIN : </b> <?php echo VENDORGSTIN; ?></td>
        </tr>
        <tr>
        <td width = "50%"><b>A/c Name :</b> <?php echo VENDORACNAME; ?></td>
        <td width="50%" align = "center"><b>A/c No :</b> <?php echo VENDORACNO; ?></td>
        </tr>
        <tr>
        <td width = "50%"><b>Bank :</b> <?php echo VENDORBANKNAME; ?></td>
        <td width="50%" align = "center"><b>IFSC Code :</b> <?php echo VENDORIFSC; ?></td>
        </tr>
    
    </table>
</div>

<?php } ?>


 <!-----------------------------------FOOTER----------------------------------------->

<div class = "default-footer"> 
Note : Goods once sold will not be taken back. Warrenty will be given by brand company only. Cheque Bounces Rs. 500/- Charges
</div>


</body></html>
<!--Html Source to write start->
<?php
$html = ob_get_clean();

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
//$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

$dompdf->set_base_path('https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
// Output the generated PDF to Browser

$name = base64_encode($cardno.'='.$ivcID.'='.$billType);

$output = $dompdf->output();
file_put_contents(INVOICE_GENRATE_OUTPUT.$name.'.pdf', $output);

$dompdf->stream('invoice.pdf', Array('Attachment' => 0));
?>