<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php'; 
?>
<!----------------------------------------------------------------------------------------------------------------------------------------------->

<div class = "container-fluid row mt-4">

  <div class = "col-sm-6">
    <button type="button" class="btn btn-warning btn-block" id="upcomingRenew" ><i class="fas fa-sync">&nbsp;&nbsp;&nbsp;</i>Upcoming Services</button>
  </div>
  <div class = "col-sm-6">
    <button type="button" class="btn btn-primary btn-block" id="myrenewBtn" ><i class="fas fa-check-double">&nbsp;&nbsp;&nbsp;</i>Completed Services</button>
  </div>

</div>

<!----------------------------------------------------------------------------------------------------------------------------------------------->

<!-- <div class = "container-fluid m-2 row">
        <div class = "col-sm-10">
          <h4 align="center" class=""><i>Pending/Upcoming Renewal</i></h4>
        </div>
        <div class = "col-sm-2">
          <button type="button" id="myrenewBtn" class="btn btn-warning use-address">Show Previous Renewals</button>
        </div>
      </div> -->

<?php
if (isset($_GET['mode'])) {
  if($_GET['mode'] == 'success'){
    echo showResponseSuccess('Service Renewed Successfully.');
  } else if($_GET['mode'] == 'fail'){
    echo showResponseFail('Something Went Wrong.');
  }
}
?>

<div class="mt-4 table-responsive">
    <table class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>Card No.</th>
        <th>Order No.</th>
        <th>First Name</th>
        <th>Product</th>
        <th>Purchase Date</th>
        <th>Renew Date</th>
        <th>City</th>
        <th>Add Order</th>
      </tr>
    </thead>
 <tbody>
 <?php
 $json_request = array("None"=>"none");
 $my_Response_Renewal = request_api(SERVICE_API_PAGE ,"pendingRenewal", $json_request); 
	 
 foreach ($my_Response_Renewal as $key => $value) {

    $date = date("d/m/Y", strtotime($my_Response_Renewal[$key]['pdate']));
    $renew_date = date("d/m/Y", strtotime($my_Response_Renewal[$key]['renew_date']));
    echo  '<tr class="table-danger"><td class = "getCNo userCardNo">'.$my_Response_Renewal[$key]['cardno'].'</td><td class="getONo">'.$my_Response_Renewal[$key]['orderno'].'</td><td>'.$my_Response_Renewal[$key]['name'].'</td><td>'.$my_Response_Renewal[$key]['prod_name'].'</td><td>'.$date.'</td><td>'.$renew_date.'</td><td>'.$my_Response_Renewal[$key]['city'].'</td><td><button type="button" class="btn btn-success use-address">Renew</button></td></tr>';
}
?>
</tbody></table></div>

<script>

$(".userCardNo").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var text = $row.find(".getCNo").text(); // Find the text
  var text = window.btoa(text);
  window.location.href = "cust_detail.php?cardno="+text;

});


$(".use-address").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var $text = $row.find(".getONo").text(); // Find the text

  window.location.href = "update_renew.php?orderno="+$text;

});


// Redirect to completed renew page

document.getElementById("myrenewBtn").onclick = function () {
 location.href = "renew_complete.php";
};

document.getElementById("upcomingRenew").onclick = function () {
 //location.href = "renew_complete.php";
 alert('upcomingRenew');
};
</script>

<?php include '../include/footer.php'; ?>