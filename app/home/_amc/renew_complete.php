<?php
include '../res/genral.php';
?>
<?php include '../include/header.php'; ?>
<?php include '../include/menu.php';

echo '<h4 align="center" class="m-2"><i>Completed Renewals</i></h4>';

echo '<div class="table-responsive">
    <table class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Cardno</th>
        <th>Orderno</th>
        <th>Product</th>
        <th>Renew Date</th>
        <th>Renew Done on</th>
        <th>Invoice</th>
      </tr>
    </thead>
 <tbody>';

 $json_request = array("None"=>"none");
 $my_Response_Renew_Completed = request_api(SERVICE_API_PAGE ,"getCompletedRenew", $json_request); 

 foreach ($my_Response_Renew_Completed as $key => $value) {

    //$date = date("d/m/Y", strtotime($my_Response_Renew_Completed[$key]['pdate']));
    $renew_date = date("d/m/Y", strtotime($my_Response_Renew_Completed[$key]['renew_date']));
    $renew_done_date = date("d/m/Y", strtotime($my_Response_Renew_Completed[$key]['renew_done_date']));
    echo  '<tr class="table-warning"><td class="">'.$my_Response_Renew_Completed[$key]['renewid'].'</td><td class="">'.$my_Response_Renew_Completed[$key]['cardno'].'</td><td>'.$my_Response_Renew_Completed[$key]['orderno'].'</td><td>'.$my_Response_Renew_Completed[$key]['prod_name'].'</td><td>'.$renew_date.'</td><td>'.$renew_done_date.'</td><td><button type="button" class="btn btn-success use-address">Print</button></td></tr>';
 }
?>

<?php include '../include/footer.php'; ?>