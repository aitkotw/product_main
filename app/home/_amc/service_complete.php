<?php
include '../res/genral.php';
?>
<?php include '../include/header.php'; ?>
<?php include '../include/menu.php';

echo '<h4 align="center" class="m-2"><i>Completed services</i></h4>';

echo '<div class="table-responsive">
    <table class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Cardno</th>
        <th>Orderno</th>
        <th>Product</th>
        <th>Service Date</th>
        <th>Service Done on</th>
        <th>Invoice</th>
      </tr>
    </thead>
 <tbody>';

 $json_request = array("None"=>"none");
 $my_Response_Service_completed = request_api(SERVICE_API_PAGE ,"getCompletedService", $json_request); 

 foreach ($my_Response_Service_completed as $key => $value) {

    //$date = date("d/m/Y", strtotime($my_Response_Service_completed[$key]['pdate']));
    $serv_date = date("d/m/Y", strtotime($my_Response_Service_completed[$key]['serv_date']));
    $serv_done_date = date("d/m/Y", strtotime($my_Response_Service_completed[$key]['serv_done_date']));
    echo  '<tr class="table-warning"><td class="">'.$my_Response_Service_completed[$key]['servid'].'</td><td class="">'.$my_Response_Service_completed[$key]['cardno'].'</td><td>'.$my_Response_Service_completed[$key]['orderno'].'</td><td>'.$my_Response_Service_completed[$key]['prod_name'].'</td><td>'.$serv_date.'</td><td>'.$serv_done_date.'</td><td><button type="button" class="btn btn-success use-address">Print</button></td></tr>';
 }
?>

<?php include '../include/footer.php'; ?>