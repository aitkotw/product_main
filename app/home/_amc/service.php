<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php'; 
?>
<!----------------------------------------------------------------------------------------------------------------------------------------------->

<div class = "container-fluid row mt-4">

  <div class = "col-sm-6">
    <button type="button" class="btn btn-warning btn-block" id="upcomingServices" ><i class="fas fa-business-time">&nbsp;&nbsp;&nbsp;</i>Upcoming Services</button>
  </div>
  <div class = "col-sm-6">
    <button type="button" class="btn btn-primary btn-block" id="myServiceBtn" ><i class="fas fa-user-check ">&nbsp;&nbsp;&nbsp;</i>Completed Services</button>
  </div>

</div>

<!----------------------------------------------------------------------------------------------------------------------------------------------->

<!-- <div class = "container-fluid m-2 row">
    <div class = "col-sm-10">
        <h4 align="center" class=""><i>Pending/Upcoming Services</i></h4>
    </div>
    <div class = "col-sm-2">
        <button type="button" id="myServiceBtn" class="btn btn-warning use-address">Show Previous Services</button>
    </div>
</div> -->

<?php
if (isset($_GET['mode'])) {
    if($_GET['mode'] == 'success'){
      echo showResponseSuccess('Service Updated Successfully.');
    } else if($_GET['mode'] == 'fail'){
      echo showResponseFail('Something Went Wrong.');
    }
}
?>
<div class="mt-4 table-responsive">
    <table class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>Card No.</th>
        <th>Order No.</th>
        <th>First Name</th>
        <th>Product</th>
        <th>Purchase Date</th>
        <th>Service Date</th>
        <th>City</th>
        <th>Add Order</th>
      </tr>
    </thead>
 <tbody>
<?php
 $json_request = array("None"=>"none");
 $my_Response_Service = request_api(SERVICE_API_PAGE ,"pendingService", $json_request); 

 foreach ($my_Response_Service as $key => $value) {

    $date = date("d/m/Y", strtotime($my_Response_Service[$key]['pdate']));
    $serv_date = date("d/m/Y", strtotime($my_Response_Service[$key]['serv_date']));
    echo  '<tr class="table-danger"><td class="getCNo userCardNo">'.$my_Response_Service[$key]['cardno'].'</td><td class="getONo">'.$my_Response_Service[$key]['orderno'].'</td><td>'.$my_Response_Service[$key]['name'].'</td><td>'.$my_Response_Service[$key]['prod_name'].'</td><td>'.$date.'</td><td>'.$serv_date.'</td><td>'.$my_Response_Service[$key]['city'].'</td><td><button type="button" class="btn btn-success use-address">Completed</button></td></tr>';
 }
?>
 </tbody></table></div>;

 <script>

 $(".userCardNo").click(function() {
 
   var $row = $(this).closest("tr");    // Find the row
   var text = $row.find(".getCNo").text(); // Find the text
   var text = window.btoa(text);
   window.location.href = "cust_detail.php?cardno="+text;
 
 });
 
 
 $(".use-address").click(function() {
 
   var $row = $(this).closest("tr");    // Find the row
   var $text = $row.find(".getONo").text(); // Find the text
 
   window.location.href = "update_service.php?orderno="+$text;
 
 });

 // Redirect to completed service page

 document.getElementById("myServiceBtn").onclick = function () {
  location.href = "service_complete.php";
};

document.getElementById("upcomingServices").onclick = function () {
  //location.href = "service_complete.php";
  alert('upcoming Service');
};
 </script>;

<?php include '../include/footer.php'; ?>