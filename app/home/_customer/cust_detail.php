<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';

if(isset($_GET['cardno'])){
    $cust_detail;
    $cardno = clean(base64_decode($_GET['cardno']));
    
    //check wheather cardno exists or not
    $json_request = array("cardno"=>"$cardno");
    $my_Response_Cust = request_api(CUSTOMER_API_PAGE ,"cardNoExists", $json_request); 
    //echo $my_Response_Cust;
    if($my_Response_Cust === 'true'){
        //echo 'valid customer';
        $json_request = array("cardno"=>"$cardno");
        $cust_detail = request_api(CUSTOMER_API_PAGE ,"getCustDetail", $json_request); 
        //print_r($cust_detail);
    }else{
        echo 'Invalid Cardno provided';
        die();
    }



}else{
    header('Location: ../404.php');
}
?>

<div class="row">
   
   <div class="container">

      <div class="col-sm-12 shadow-lg">

         <div class="row paneladdorder">
            <h2 class="mx-auto"><?php echo $cust_detail['fname']." ".$cust_detail['lname']; ?></h2>
         </div>   
         <div class="row">
            <div class="container px-3 pt-5 pb-3">
               <form name="loginForm" method="post" action="">
                  

                  <?php
                    if(isset($_GET['mode']) && $_GET['mode'] == 'invalid'){
                        echo showResponseFail('Invalid Card No, Please Provide Valid Customer Card No!!');                  
                    }
                  ?>


                  <div class="form-group">
                     <div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;">
                        <input class="form-control" type="text" pattern=".{6,}" title="Please Enter 6 digit card No." name="cardno" disabled value="<?php echo $cardno; ?>" >
                     </div>
                  </div>

                  <div class="form-group form-row">
                     <div class="col-sm-6">
                        <label for="inputAddress">First Name</label>
                        <input type="text" class="form-control" name="fname" placeholder="Enter First Name" disabled value="<?php echo $cust_detail['fname']; ?>">
                     </div>

                     <div class="col-sm-6">
                        <label for="inputAddress">Last Name</label>
                        <input type="text" class="form-control" name="lname" placeholder="Enter Last Name" disabled value="<?php echo $cust_detail['lname']; ?>">
                     </div>
                  
                  </div>

                  <div class="form-group form-row">
                     <div class="col-sm-6">
                        <label for="inputAddress">Email</label>
                        <input type="text" class="form-control" name="fname" placeholder="Enter First Name" disabled value="<?php echo $cust_detail['email']; ?>">
                     </div>

                     <div class="col-sm-6">
                        <label for="inputAddress">Phone</label>
                        <input type="text" class="form-control" name="lname" placeholder="Enter Last Name" disabled value="<?php echo $cust_detail['phone']; ?>">
                     </div>
                  
                  </div>

                  <div class="form-group">
                     <label for="inputAddress">Address</label>
                     <input type="text" class="form-control" name="add" placeholder="1234 Main St" disabled value="<?php echo $cust_detail['add1']." ".$cust_detail['add2']; ?>">
                  </div>

                  <div class="form-group form-row">
                     <div class="col">
                        <label for="city">City</label>
                        <input type="text" class="form-control" name="city" placeholder="City" disabled value="<?php echo $cust_detail['city']; ?>">
                     </div>
                     <div class="col">
                     <label for="city">State</label>
                        <input type="text" name="state" class="form-control" placeholder="Maharashtra" value="Maharashtra" disabled value="<?php echo $cust_detail['state'] ?>">
                     </div>
                     <div class="col">
                     <label for="city">Pincode</label>
                        <input type="text" class="form-control" placeholder="Zip Code" pattern=".{6,6}" name="zip" title="Please Enter Valid zip code" disabled value="<?php echo $cust_detail['zipcode'] ?>">
                     </div>
                  </div>

                  <h3 align="center" class="m-3 mt-5">Order Details</h3>
                  
                  <?php
                        
                        $json_request = array("cardno"=>"$cardno");
                        $cust_order = request_api(ORDER_API_PAGE ,"getCustOrder", $json_request); 
                        //print_r($cust_order);
                        echo '<div class="table-responsive">
                        <table class="table table-hover">
                        <thead class="thead-dark">
                          <tr>
                            <th>Order No</th>
                            <th>Product</th>
                            <th>Purchased On</th>
                            <th>Service</th>
                            <th>Renewal</th>
                          </tr>
                        </thead>
                     <tbody>';
                     
                     foreach ($cust_order as $key => $value) {

                        $pdate = date("d/m/Y", strtotime($cust_order[$key]['pdate']));
                        $serv_date = date("d/m/Y", strtotime($cust_order[$key]['serv_date']));
                        $renew_date = date("d/m/Y", strtotime($cust_order[$key]['renew_date']));

                        echo  '<tr class="table-info"><td class="getONo">'.$cust_order[$key]['orderno'].'</td><td class="getCNo userCardNo">'.$cust_order[$key]['prod_name'].'</td><td>'.$pdate.'</td><td>'.$serv_date.'</td><td>'.$renew_date.'</td></tr>';
                    }                     
                     echo '</tbody>
                     </table>
                    </div>';

                  ?>

                  <h3 align="center" class="m-3 mt-5">Prevoius Service Details</h3>

                  <?php
                        
                        $json_request = array("cardno"=>"$cardno");
                        $cust_serv_completed = request_api(SERVICE_API_PAGE ,"custServCompleted", $json_request); 
                        //print_r($cust_serv_completed);
                        echo '<div class="table-responsive">
                        <table class="table table-hover">
                        <thead class="thead-dark">
                          <tr>
                            <th>Service No</th>
                            <th>Orderno</th>
                            <th>Product</th>
                            <th>Service Date</th>
                            <th>Service Done On</th>
                          </tr>
                        </thead>
                     <tbody>';
                     
                     foreach ($cust_serv_completed as $key => $value) {

                        $serv_date = date("d/m/Y", strtotime($cust_serv_completed[$key]['serv_date']));
                        $serv_done_date = date("d/m/Y", strtotime($cust_serv_completed[$key]['serv_done_date']));
                        echo  '<tr class="table-warning"><td class="getONo">'.$cust_serv_completed[$key]['servid'].'</td><td class="getCNo userCardNo">'.$cust_serv_completed[$key]['orderno'].'</td><td>'.$cust_serv_completed[$key]['prod_name'].'</td><td>'.$serv_date.'</td><td>'.$serv_done_date.'</td></tr>';
                    }                     
                     echo '</tbody>
                     </table>
                    </div>';

                  ?>


                  <h3 align="center" class="m-3 mt-5">Prevoius Renew Details</h3>

                  <?php
                        
                        $json_request = array("cardno"=>"$cardno");
                        $cust_renew_completed = request_api(SERVICE_API_PAGE ,"custRenewCompleted", $json_request); 
                        //print_r($cust_serv_completed);
                        echo '<div class="table-responsive">
                        <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                           <th>Renew ID</th>
                           <th>Orderno</th>
                           <th>Product</th>
                           <th>Renew Date</th>
                           <th>Renew Done On</th>
                        </tr>
                        </thead>
                     <tbody>';
                     
                     foreach ($cust_renew_completed as $key => $value) {

                        $renew_date = date("d/m/Y", strtotime($cust_renew_completed[$key]['renew_date']));
                        $renew_done_date = date("d/m/Y", strtotime($cust_renew_completed[$key]['renew_done_date']));
                        echo  '<tr class="table-warning"><td class="getONo">'.$cust_renew_completed[$key]['renewid'].'</td><td class="getCNo userCardNo">'.$cust_renew_completed[$key]['orderno'].'</td><td>'.$cust_renew_completed[$key]['prod_name'].'</td><td>'.$renew_date.'</td><td>'.$renew_done_date.'</td></tr>';
                  }                     
                     echo '</tbody>
                     </table>
                  </div>';

                  ?>

               </form>
            </div>

            

         </div>
      </div>

   </div>

</div>

<?php include '../include/footer.php'; ?>
