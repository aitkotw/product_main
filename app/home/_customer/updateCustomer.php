<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';

if(isset($_GET['cardno'])){
    $cust_detail;
    $cardno = clean(base64_decode($_GET['cardno']));
    
    //check wheather cardno exists or not
    $json_request = array("cardno"=>"$cardno");
    $my_Response_Cust = request_api(CUSTOMER_API_PAGE ,"cardNoExists", $json_request); 
    //echo $my_Response_Cust;
    if($my_Response_Cust === 'true'){
        //echo 'valid customer';
        $json_request = array("cardno"=>"$cardno");
        $cust_detail = request_api(CUSTOMER_API_PAGE ,"getCustDetail", $json_request); 
        //print_r($cust_detail);

        //Update new Delete and Updation 
        if(isset($_GET['action']) && $_GET['action'] === 'delete'){

            $json_request = array("cardno"=>"$cardno");
            $delete_customer = request_api(CUSTOMER_API_PAGE ,"deleteCustomerWithCardNo", $json_request); 
            var_dump($delete_customer);
            if($delete_customer === "true"){
                
                header('Location: customer.php?mode=delCustSuccess');

            } else {
                header('Location: updateCustomer.php?cardno='.base64_encode($cardno).'&mode=delFail');
            }
            //echo 'User deleted';
        }else if(isset($_GET['action']) && $_GET['action'] === 'edit'){
            
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                //TODO: update record

                $fname = clean($_POST['fname']);
                $dob = clean($_POST['dob']);
                $email = clean($_POST['email']);
                $phone = clean($_POST['phone']);
                $address = clean($_POST['address']);
                $city = clean($_POST['city']);
                $state = clean($_POST['state']);
                $gstno = clean($_POST['gstno']);
                $zip = clean($_POST['zip']);

                $json_request = array("cardno"=>"$cardno", "name"=>"$name", "dob"=>"$dob", "email"=>"$email", "phone"=>"$phone", "address"=>"$address", "gstno" => "$gstno", "city"=>"$city","state"=>"$state","zip"=>"$zip");
                $updateCustomerDtl = request_api(CUSTOMER_API_PAGE ,"updateCustomerDtl", $json_request); 

                if($updateCustomerDtl == "true"){
                    header('Location: updateCustomer.php?cardno='.base64_encode($cardno).'&mode=updateSuccess');
                } else {
                    header('Location: updateCustomer.php?cardno='.base64_encode($cardno).'&mode=updateFail');
                }

            }            
        }

    }else{
        echo 'Invalid Cardno provided';
        die();
    }

} else{
    header('Location: 404.php');
}

if(isset($_GET['mode']) && $_GET['mode'] == 'delFail'){
     echo showResponseWarning(' Can not delete this customer. Customer has Orders.'); 
  } else if(isset($_GET['mode']) && $_GET['mode'] == 'updateSuccess'){
     echo showResponseSuccess('Customer Updated Successfully.'); 
  }else if(isset($_GET['mode']) && $_GET['mode'] == 'updateFail'){
     echo showResponseFail('Unable to Update Customer details at the moment.');
  }
?>


<div class="row">
   
   <div class="container">

      <div class="col-sm-12 shadow-lg">

         <div class="row paneladdorder">
            <h2 class="mx-auto"><?php echo $cust_detail['name']; ?></h2>
         </div>   
         <div class="row">
            <div class="container px-3 pt-5 pb-3">
               <form name="loginForm" method="post" action="">


                  <div class="form-group form-row">
                     <div class="col-sm-4">
                        <label for="cardno">Cardno</label>
                        <input class="form-control" type="text" pattern=".{6,6}" id = "cardno" title="Please Enter 6 digit card No." name="cardno" disabled value="<?php echo $cardno; ?>" >
                     </div>

                     <div class="col-sm-4">
                        <label for="inputAddress">First Name</label>
                        <input type="text" class="form-control cngDisabled" name="name" id = "name" placeholder="Enter First Name" disabled value="<?php echo $cust_detail['name']; ?>">
                     </div>

                     <div class="col-sm-4">
                        <label for="gst">Date of Birth</label>
                        <input type="date" class="form-control cngDisabled" disabled name="dob" value="<?php echo $cust_detail['dob']; ?>">
                     </div>
                  </div>

                  <div class="form-group form-row">
                     <div class="col-sm-4">
                        <label for="gst">GSTIN No.</label>
                        <input type="text" class="form-control cngDisabled" disabled name="gstno" placeholder="Enter GST No." value="<?php echo $cust_detail['gstno']; ?>">
                     </div>

                     <div class="col-sm-4">
                        <label for="inputAddress">Email</label>
                        <input type="text" class="form-control cngDisabled" name="email" id = "email" placeholder="Enter First Name" disabled value="<?php echo $cust_detail['email']; ?>">
                     </div>

                     <div class="col-sm-4">
                        <label for="inputAddress">Phone</label>
                        <input type="text" class="form-control cngDisabled" name="phone" id = "phone" placeholder="Enter Last Name" disabled value="<?php echo $cust_detail['phone']; ?>">
                     </div>
                  
                  </div>

                  <div class="form-group">
                     <label for="inputAddress">Address</label>
                     <input type="text" class="form-control cngDisabled" name="address" id = "add" placeholder="1234 Main St" disabled value="<?php echo $cust_detail['address']; ?>">
                  </div>

                  <div class="form-group form-row">
                     <div class="col">
                        <label for="city">City</label>
                        <input type="text" class="form-control cngDisabled" name="city" id = "city" placeholder="City" disabled value="<?php echo $cust_detail['city']; ?>">
                     </div>
                     <div class="col">
                     <label for="city">State</label>
                        <input type="text" name="state" class="form-control cngDisabled" placeholder="Maharashtra" value="Maharashtra" disabled value="<?php echo $cust_detail['state'] ?>">
                     </div>
                     <div class="col">
                     <label for="city">Pincode</label>
                        <input type="text" class="form-control cngDisabled" placeholder="Zip Code" id = "zip" pattern=".{6,6}" name="zip" title="Please Enter Valid zip code" disabled value="<?php echo $cust_detail['zipcode'] ?>">
                     </div>
                  </div>

                    <?php

                        if(isset($_GET['action']) && !empty($_GET['action']) && $_GET['action'] == 'edit'){
                            //TODO if set
                            echo '<script> var inputs = document.getElementsByClassName("cngDisabled");
                                    for(var i = 0; i < inputs.length; i++) {
                                        inputs[i].disabled = false;
                                    } </script>';

                            echo '<div class = "mt-5 mb-3 col-sm-12">
                                        <button type="submit" class="btn btn-block btn-success" id = "editCust">Save Changes</button>
                                    </div>';
                        
                        } else {
                            echo '<div class = "form-group mt-5 form-row">

                                            <div class = "col-sm-6">
                                                <button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#myModal">Delete</button>
                                            </div>
                                            <div class = "col-sm-6">
                                                <button type="button" class="btn btn-block btn-info" id = "editCust">Edit Customer</button>
                                            </div>
                    
                                    </div>';
                        }

                    ?>

               </form>

                <!-- The Modal -->
                <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Customer</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Are you sure you want to Delete customer?
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-info" id = "deleteCust" >Yes</button>
                    </div>

                    </div>
                </div>
                </div>
                <!-- Model End-->
            </div>


         </div>
      </div>

   </div>

</div>

<script>

$("#deleteCust").click(function(){
    var cardno = $('#cardno').val();
    cardno = window.btoa(cardno);
    window.location.href = "updateCustomer.php?cardno="+cardno+"&action=delete";
}); 

$("#editCust").click(function(){
    var cardno = $('#cardno').val();
    cardno = window.btoa(cardno);
    window.location.href = "updateCustomer.php?cardno="+cardno+"&action=edit";
});

function changeDisabledAttribute(){
    var inputs = document.getElementsByClassName('cngDisabled');
    for(var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = false;
    }
}

</script>
<?php include '../include/footer.php'; ?>