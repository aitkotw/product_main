<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';

if(isset($_GET['val']) && isset($_GET['opr']) && !empty($_GET['val']) && !empty($_GET['opr'])){
    $regNo = clean(base64_decode($_GET['val']));
    $opration = clean(base64_decode($_GET['opr']));
    //echo $regNo;

    if ($opration == 'add') {

        $json_request = array("regno"=>"$regNo");
        $my_Cust_RegEntry = request_api(CUSTOMER_API_PAGE ,"addRegisteredCustomer", $json_request); 
        //$cardno = $my_Cust_cardno + 1;

        
        if($my_Cust_RegEntry !== 'true'){

            $json_request = array("regno"=>"$regNo");
            $my_Cust_delete = request_api(CUSTOMER_API_PAGE ,"delNewCustRegistered", $json_request); 
            //$my_Cust_delete = 'true';
            if($my_Cust_delete == 'true'){

                $cardno = $my_Cust_RegEntry[0];
                $fname = $my_Cust_RegEntry[1];
                $lname = $my_Cust_RegEntry[2];

                header('Location: ../_order/new_order.php?cardno='.encrypt_decrypt('encrypt',$cardno).'&name='.encrypt_decrypt('encrypt',$fname).'&surname='.encrypt_decrypt('encrypt',$lname).'&mode=cust_success');

            } else {
                echo showResponseFail('Something Went Wrong!!');        
            }
        } else{
            echo showResponseFail('Something Went Wrong!!');
        }
        
    } else if ($opration == 'del'){

        $json_request = array("regno"=>"$regNo");
        $my_Cust_delete = request_api(CUSTOMER_API_PAGE ,"delNewCustRegistered", $json_request); 

        if($my_Cust_delete == 'true'){
            echo showResponseInfo('Record Deleted Successfully!!');
        } else {
            echo showResponseInfo('Something Went Wrong!!');
        }

    } else{

    }

}

?>


<div class="row">

    <div class="container" style = "max-width: 1440px;">

        <div class="col-sm-12 shadow-lg">

            <div class="row paneladd">
                <h2 class="mx-auto">Pending Review</h2>
            </div>   
            <div class="row">
                <div class="container px-3 pt-5 pb-3" style = "max-width: 1440px;">

                        <!-- Show values here with loop-->
                        <?php

                            $json_request = array("None"=>"none");
                            $my_Response_regCust = request_api(CUSTOMER_API_PAGE ,"newCustRegistered", $json_request); 

                            echo '<div class="table-responsive">
                                        <table id="myTable" class="table table-dark table-hover">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Add Customer</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                    <tbody>';

                            foreach ($my_Response_regCust as $key => $value) {
                                
                                echo  '<tr class="mx-auto"><td class = "getRegNo" style="display:none;"> '.$my_Response_regCust[$key]['regno'].' </td><td>'.$my_Response_regCust[$key]['name'].'</td><td>'.$my_Response_regCust[$key]['phone'].'</td><td>'.$my_Response_regCust[$key]['email'].'</td><td> <button type="button" class="btn btn-success previewRow" style = " border-radius:0px">Add Customer</button> </td><td> <button type="button" class="btn btn-danger deleteRow" style = " border-radius:0px">Delete</button> </td></tr>';
                            
                            }
                            echo '</tbody></table></div>';
                        ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(".previewRow").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var regNo = $row.find(".getRegNo").text(); // Find the text
  //alert(regNo);
  var regNo = window.btoa(regNo);
  var opr = window.btoa("add");
  window.location.href = "pendingReview.php?opr="+opr+"&val="+regNo;

});

$(".deleteRow").click(function() {

var $row = $(this).closest("tr");    // Find the row
var regNo = $row.find(".getRegNo").text(); // Find the text
//alert(regNo);
var regNo = window.btoa(regNo);
var opr = window.btoa("del");
window.location.href = "pendingReview.php?opr="+opr+"&val="+regNo;

});
</script>


<?php include '../include/footer.php'; ?>