<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';

if(isset($_GET['mode']) && $_GET['mode'] == 'delCustSuccess'){
    $message = 'Customer Deleted Successfully!!';
    echo showResponseSuccess($message);
  }

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  //sms send start
  if (isset($_POST['smsForm']) && $_POST['smsForm'] === 'yes') {
    //TODO : Need Change database connection for deployment
    $phone = clean($_POST['smsPhone']);

    //Add customer to custRegistration database and get message URL
    $json_request = array("phone"=>"$phone");
    $my_Response_SMS = request_api(ASSET_API_PAGE ,"sendRegistrationSMS", $json_request);

    if($my_Response_SMS !== 'false'){

      //var_dump($my_Response_SMS[0]);
      $phone = $my_Response_SMS[0];
      $message = $my_Response_SMS[1];

      //Actual SMS is being sent with registration URL
      $json_request = array("phone"=>"$phone", "message" => "$message");
      $my_Response_SMS_new = request_api(ASSET_API_PAGE ,"sendSMS", $json_request);

      $my_Response_SMS_new = json_decode($my_Response_SMS_new, true);

      //var_dump($my_Response_SMS_new);
      if ($my_Response_SMS_new['status'] === 'success') {
          header('Location: customer.php?mode=smsSuccess');
      } else{
          // print_r($message);
          // die();
          header('Location: customer.php?mode=smsFail');
      }

    }else {
      echo 'Something not right';
    }

  }
}

if (isset($_GET['mode'])) {

  if($_GET['mode'] == 'smsSuccess'){
     echo showResponseSuccess('SMS sent successfully!!');                     
  }else if($_GET['mode'] == 'smsFail'){
     echo showResponseFail('Unable to send SMS!!');                     
  }
}
?>

<!----------------------------------------------------------------------------------------------------------------------------------------------->

<div class = "container-fluid row mt-4">

  <div class = "col-sm-4">
    <button type="button" class="btn btn-info btn-block" id="addNewCustomer" ><i class="fas fa-user-plus">&nbsp;&nbsp;&nbsp;</i>Add New Customer</button>
  </div>
  <div class = "col-sm-4">
    <button type="button" class="btn btn-success btn-block" id="sendRegistrationSMS" data-toggle="modal" data-target="#myModal" ><i class="fas fa-mobile-alt ">&nbsp;&nbsp;&nbsp;</i>Send Registration SMS</button>
  </div>
  <div class = "col-sm-4">
    <button type="button" class="btn btn-warning btn-block" id="myPendingReviewutton" ><i class="fas fa-user-cog">&nbsp;&nbsp;&nbsp;</i>Pending Customer Review</button>
  </div>

</div>

<!----------------------------------------------------------------------------------------------------------------------------------------------->

 <!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Enter Phone number</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form name="smsForm" method="post" action="">
        <div class="form-group row">
          <label for="staticEmail" class="col-sm-2 col-form-label">Phone</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name = "smsPhone" pattern=".{10,10}" title="Must contain 10 Numbers" required placeholder="eg. 9876543210">
            <input type="hidden" name="smsForm" value="yes"/>
          </div>
        </div>
      </div>

      <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

    </div>
  </div>
</div>




<!------------------------------------------------------------------------------------------------------------------------------------------------->
<div class = "row mt-4">
  <div class = "col-sm-2 my-auto">
    <div class = "ml-3">
      <select id="mySelect" onchange="myFunction()">
        <option value="cardno">Cardno</option>
        <option value="name">Name</option>
        <option value="phone">Phone</option>
      </select>
    </div>
  </div>
  <div class = "col-sm-10">
    <div class=""> <input type="text" id="mySearchInput" onkeyup="mySearchFunctionNew()" placeholder="Search for names.."></div>
  </div>
</div>

<div class="table-responsive">
    <table id="myTable" class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>Card No.</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>City</th>
        <th>Add Order</th>
        <th>Update</th>
      </tr>
    </thead>
 <tbody>

 <?php
    $json_request = array("None"=>"none");
    $my_Response_Cust = request_api(CUSTOMER_API_PAGE ,"getAddressBook", $json_request); 
    
    foreach ($my_Response_Cust as $key => $value) {

        //$date = date("d/m/Y", strtotime($my_Response_Cust[$key]['pdate']));
        echo  '<tr class="table-success"><td class="getCNo userCardNo">'.$my_Response_Cust[$key]['cardno'].'</td><td class="getCname">'.$my_Response_Cust[$key]['name'].'</td><td>'.$my_Response_Cust[$key]['phone'].'</td><td>'.$my_Response_Cust[$key]['email'].'</td><td>'.$my_Response_Cust[$key]['city'].'</td><td><button type="button" class="btn btn-primary use-address">New Order</button></td><td><button type="button" class="btn btn-info edit_customer">Edit</button></td></tr>';
    }
 ?>

</tbody></table></div>

<script>

$(".userCardNo").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var text = $row.find(".getCNo").text(); // Find the text
  var text = window.btoa(text);
  window.location.href = "cust_detail.php?cardno="+text;

});

$(".edit_customer").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var cNo = $row.find(".getCNo").text(); // Find the text

  var cNo = window.btoa(cNo);

  window.location.href = "updateCustomer.php?cardno="+cNo;

});

$(".use-address").click(function() {

  var $row = $(this).closest("tr");    // Find the row
  var cNo = $row.find(".getCNo").text(); // Find the text
  var cName = $row.find(".getCname").text(); // Find the text

  var cNo = window.btoa(cNo);
  var cName = window.btoa(cName);

  window.location.href = "../_order/new_order.php?cardno="+cNo+"&name="+cName+"&mode=custOrder";

});

var mySearchArrayIndex = 0;

function myFunction() {
  var x = document.getElementById("mySelect").value;
  if(x == "name"){
    mySearchArrayIndex = 1;
  } else if(x == "phone"){
    mySearchArrayIndex = 4;
  } else if(x == "cardno"){
    mySearchArrayIndex = 0;
  }
}

function mySearchFunctionNew() {
  // Declare variables
  var input, filter, table, tr, td, txtValue;
  input = document.getElementById("mySearchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don\'t match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[mySearchArrayIndex];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

document.getElementById("addNewCustomer").onclick = function () {
        location.href = "add_new.php";
    };

document.getElementById("myPendingReviewutton").onclick = function () {
    location.href = "pendingReview.php";
};

</script>


<?php include '../include/footer.php'; ?>
