<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';

$currentCardno = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

   // var_dump($_POST);
   // die();
      $cardno = clean($_POST['cardno']);
      $name = clean($_POST['name']);
      $phone = clean($_POST['phone']);
      $dob = clean($_POST['dob']);
      $gstno = clean($_POST['gstno']);
      $email = clean($_POST['email']);
      $add = clean($_POST['add']);
      $state = clean($_POST['state']);
      $city = clean($_POST['city']);
      $zip = clean($_POST['zip']);

      if($dob == ''){
         $dob = "1-01-01";
      } 
      
      if($zip == ''){
         $zip = 000000;
      }

      $json_request = array(
      "cardno"=>"$cardno",
      "name"=>"$name",
      "dob"=>"$dob",
      "phone"=>"$phone",
      "gstno"=>"$gstno",
      "email"=>"$email",
      "add"=>"$add",
      "city"=>"$city",
      "state"=>"$state",
      "zip"=>"$zip"
      );

      $my_Response_Service = request_api(CUSTOMER_API_PAGE ,"AddNewCustomer", $json_request); 
      
      if($my_Response_Service === "true"){
         header('Location: ../_order/new_order.php?cardno='.encrypt_decrypt('encrypt',$cardno).'&name='.encrypt_decrypt('encrypt',$name).'&mode=cust_success');  
      }else{
         // var_dump($my_Response_Service);
         // die();
         header('Location: add_new.php?mode=fail');
      }

} 
else{
   $json_request = array("none"=>"none");
   $my_Response_Service = request_api(CUSTOMER_API_PAGE ,"getMaxCardNo", $json_request); 

   $currentCardno = $my_Response_Service+1;

}

?>



<div class="col-sm-12">
   
   <div class="row">
   
   <div class="container">

      <div class="col-sm-12 shadow-lg">

         <div class="row paneladd">
            <h2 class="mx-auto">Add Customer</h2>
         </div>   
         <div class="row">
            <div class="container px-3 pt-5 pb-3">
               <form name="loginForm" method="post" action="">
                  

                  <?php
                     if (isset($_GET['mode'])) {

                        if($_GET['mode'] == 'success'){
                           echo showResponseSuccess('Customer Added Successfully!!');                     
                        }else if($_GET['mode'] == 'fail'){
                           echo showResponseFail('Customer Might Exist with same Card No!!');                     
                        }
                     }
                  ?>


                  <div class="form-group">
                     <div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;">
                        <input class="form-control" type="text" pattern=".{6,6}" title="Please Enter 6 digit card No." name="cardno" required value = "<?php echo $currentCardno; ?>">
                        <!-- <input type="hidden" name="cardno" value="<?php //echo $currentCardno; ?>" /> -->
                     </div>
                  </div>

                  <div class="form-group form-row">
                     <div class="col-sm-6">
                        <label for="inputAddress">First Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter First Name" required>
                     </div>

                     <div class="col-sm-6">
                        <label for="inputAddress">Contact No.</label>
                        <input type="phone" class="form-control" name="phone" pattern=".{10,10}" title="Must contain 10 Numbers" required placeholder="Enter Phone No.">
                     </div>
                  
                  </div>

                  <div class="form-group form-row">
                     
                     <div class="col-sm-4">
                        <label for="inputAddress">Email Address</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter Email Address">
                     </div>

                     <div class="col-sm-4">
                        <label for="gst">Date of Birth</label>
                        <input type="date" class="form-control" name="dob" placeholder="Enter GST No.">
                     </div>

                     <div class="col-sm-4">
                        <label for="gst">GSTIN No.</label>
                        <input type="text" class="form-control" name="gstno" placeholder="Enter GST No.">
                     </div>
                  
                  </div>

                  <div class="form-group">
                     <label for="inputAddress">Address</label>
                     <input type="text" class="form-control" name="add" required title = "Address is requied" placeholder="1234 Main St">
                  </div>

                  <div class="form-group form-row">
                     <div class="col">
                        <input type="text" class="form-control" name="city" placeholder="City">
                     </div>

                     <div class="col">
                        <select class="form-control" name = "state" id="sel1">
                           <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                           <option value="Andhra Pradesh">Andhra Pradesh</option>
                           <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                           <option value="Assam">Assam</option>
                           <option value="Bihar">Bihar</option>
                           <option value="Chandigarh">Chandigarh</option>
                           <option value="Chhattisgarh">Chhattisgarh</option>
                           <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                           <option value="Daman and Diu">Daman and Diu</option>
                           <option value="Delhi">Delhi</option>
                           <option value="Goa">Goa</option>
                           <option value="Gujarat">Gujarat</option>
                           <option value="Haryana">Haryana</option>
                           <option value="Himachal Pradesh">Himachal Pradesh</option>
                           <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                           <option value="Jharkhand">Jharkhand</option>
                           <option value="Karnataka">Karnataka</option>
                           <option value="Kerala">Kerala</option>
                           <option value="Lakshadweep">Lakshadweep</option>
                           <option value="Madhya Pradesh">Madhya Pradesh</option>
                           <option value="Maharashtra" selected= "selected">Maharashtra</option>
                           <option value="Manipur">Manipur</option>
                           <option value="Meghalaya">Meghalaya</option>
                           <option value="Mizoram">Mizoram</option>
                           <option value="Nagaland">Nagaland</option>
                           <option value="Orissa">Orissa</option>
                           <option value="Pondicherry">Pondicherry</option>
                           <option value="Punjab">Punjab</option>
                           <option value="Rajasthan">Rajasthan</option>
                           <option value="Sikkim">Sikkim</option>
                           <option value="Tamil Nadu">Tamil Nadu</option>
                           <option value="Tripura">Tripura</option>
                           <option value="Uttaranchal">Uttaranchal</option>
                           <option value="Uttar Pradesh">Uttar Pradesh</option>
                           <option value="West Bengal">West Bengal</option>
                        </select>
                     </div>

                     <div class="col mb-3">
                        <input type="text" class="form-control" placeholder="Zip Code" pattern=".{6,6}" name="zip" title="Please Enter Valid zip code">
                     </div>
                  </div>

                  <div class="form-group d-flex justify-content-center">
                     <button type="submit" class="btn btn-primary col-sm-4 btn-block">Submit </button>
                  </div>
               </form>
            </div>

            

         </div>
      </div>

   </div>

</div>

   </div>

</div>
