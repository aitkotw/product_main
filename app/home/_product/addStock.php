<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

   var_dump($_POST);

   $vID = clean($_POST['vID']);
   $vName = clean($_POST['vName']);
   $vGST = clean($_POST['vGST']);
   $vdate = clean($_POST['vdate']);

   $myProductNoArray = json_decode($_POST['myArrayOrder'], true);
   $myProductNameArray = json_decode($_POST['myArrayOrderName'], true);

   $size = count($myProductNoArray);
   $qtyArray = array();
   $amtArray = array();
   for ($i=0; $i < $size; $i++) { 
       //$qtyArray = array();
       $qtyArray[] = $_POST['qty'.$i]; 
       $amtArray[] = $_POST['amt'.$i];         
   }

   $resultArray = array_combine($qtyArray, $amtArray);

   //Get Max Group No from Database 
   $json_request = array("None"=>"none");
   $myGrpNo = request_api(VENDOR_API_PAGE ,"getManvoGRPNo", $json_request);
   $myGrpNo = $myGrpNo+1;

   var_dump($myProductNoArray[0]);

    //=================================================================================
    $prodI = 0;
    foreach ($resultArray as $key => $value) {
      $prodQTY = clean($key);
      $prodAMT = clean($value);
      $productNo = $myProductNoArray[$prodI];
      $prod_name =  $myProductNameArray[$prodI];
      $json_request = array("vID"=>"$vID", "vName"=>"$vName", "productno"=>"$productNo", "prod_name"=>"$prod_name", "qty"=>"$prodQTY", "vpdate"=>"$vdate", "groupno"=>"$myGrpNo", "amt" => $prodAMT);
      $my_Response_Cust = request_api(VENDOR_API_PAGE ,"addVendorOrder", $json_request); 
      $prodI++;
    }

    if($my_Response_Cust === 'true'){
    header('Location: addStock.php?'.$_SERVER['QUERY_STRING'].'&mode=success');        
    }else{
        //var_dump($my_Response_Cust);
        header('Location: addStock.php?'.$_SERVER['QUERY_STRING'].'&mode=fail');
    }
     
 } else{
 
    $vID = clean(base64_decode($_GET['vendorid']));
    $vName = clean(base64_decode($_GET['vname']));
    $vGST = clean(base64_decode($_GET['vgst']));

    echo '<script> var serverValPost = new Array(); var serverValPostName = new Array(); </script>';
 
 }

?>

<div class="row">
   
    <div class="container">
        <div class="col-sm-12 shadow-lg">
            <div class="row paneladdorder">
                <h2 class="mx-auto">Purchase Stock</h2>
            </div>  
            <div class="row">
                <div class="container px-3 pt-5 pb-3">
                    <form name="loginForm" method="post" onsubmit="return validateMyForm();" action="">
                        
                        <?php
                            if (isset($_GET['mode'])) {

                                if($_GET['mode'] == 'success'){
                                    echo showResponseSuccess('Stock Added Successfully!!');
                                } else if($_GET['mode'] == 'fail'){
                                    echo showResponseFail('Something went wrong, Unable to add stocks!!');
                                }
                            } 
                        ?>

                        <div class="form-group form-row">
                            <div class="col-sm-4">
                                <label for="inputAddress">Vendor Name</label>
                                <input class="form-control" type="text" disabled value="<?php echo $vName; ?>" >
                                <input type="hidden" name="vID" value="<?php echo $vID; ?>" />
                                <input type="hidden" name="vName" value="<?php echo $vName; ?>" />
                                <input type="hidden" name="vGST" value="<?php echo $vGST; ?>" />
                            </div>

                            <div class="col-sm-4">
                                <label for="inputAddress">GST No.</label>
                                <input class="form-control" type="text" disabled value="<?php echo $vGST; ?>" >
                            </div>
                            <div class="col-sm-4">
                                <label for="inputAddress">Date of Order</label>
                                <div class="input-group">
                                    <input type="" class="form-control" name="vdate" id="datepicker" value="<?php echo date("d-m-Y", time()); ?>" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" onClick = "dateTimePicker()"><i class="fas fa-calendar-alt"></i></span>
                                    </div>
                                </div>  
                            </div>

                        </div>

    
                        
                        <!-- The Modal -->
                        <div class="modal" id="myModal">
                            <div class="modal-dialog" style="max-width: 1400px;">
                            <div class="modal-content">
                            
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Select Product</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                            <?php

                                $json_request = array("None"=>"none");
                                $my_Response_Prod = request_api(ORDER_API_PAGE ,"getAllProduct", $json_request); 

                                //var_dump($my_Response_Cust);
                                echo '<div class="table-responsive">
                                    <table id="myTable" class="table table-hover">
                                    <thead class="thead-dark">
                                        <tr>
                                        <th>Product ID</th>
                                        <th>Name</th>
                                        <th>Specs</th>
                                        <th>Desc</th>
                                        <th>stock</th>
                                        <th>Select</th>
                                        </tr>
                                    </thead>
                                <tbody>';

                                foreach ($my_Response_Prod as $key => $value) {
                                        
                                    echo  '<tr class="table-success"><td class="getPNo userCardNo">'.$my_Response_Prod[$key]['prod_id'].'</td><td class="getpname">'.$my_Response_Prod[$key]['prod_name'].'</td><td class="getpspec">'.$my_Response_Prod[$key]['spec'].'</td><td class="getpdesc">'.$my_Response_Prod[$key]['prod_desc'].'</td><td class="getpdesc">'.$my_Response_Prod[$key]['stock'].'</td><td><button type="button" class="btn btn-primary use-address">Select</button></td></tr>';

                                }

                                echo '</tbody></table></div>';

                            ?>
                            </div>
                            
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                            
                            </div>
                            </div>
                        </div>
                        <input type="hidden" name="myArrayOrder" id="myHiddenVal" value="">
                        <input type="hidden" name="myArrayOrderName" id="myHiddenValName" value="">
                        
                        <div id="addOrderField">

                                
                        </div>

                        <button type="button" class="btn btn-info m-2" data-toggle="modal" data-target="#myModal">Add Product</button>
                
                        <div class="form-group form-row">
                            <button type="reset" class="btn btn-default col-sm-6 p-1">Reset</button>
                            <button type="submit" class="btn btn-primary col-sm-6 p-1">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
//Date Time Picker for Form
function dateTimePicker(){
   $("#datepicker").click(function() {
        $(this).datepicker({ dateFormat: 'dd-mm-yy' }).datepicker( "show" )
    });
}

//Add New Order Entry
$i = 0;
function addOrderField(pNo, pName, pSpec, pDesc)
{  
   var d = document.getElementById('addOrderField');
   var innerDiv = document.createElement('div');
   innerDiv.className = 'form-group form-row';

   var divID = $("#addOrderField > div").length;
   divID = 'MyDivID'+divID;
   bivID = 'MyDivID'+divID;
   innerDiv.id = divID;
   
   innerDiv.innerHTML = '<div class = "form-group form-row m-1 p-3" style="background:#D2DDF0;"><div class="container form-group form-row"><div class = "col-sm-6"><i><u>Product Name</u> : </i>'+ pName +' </div><div class = "col-sm-6"><i><u>Product ID</u> :</i> '+ pNo +'</div></div><div class = "container form-group form-row"><div class = "col-sm-12 mb-2"><b>Product Specification :</b> '+ pSpec +'</div><div class = "col-sm-11 mb-2"><b>Description : </b> '+ pDesc +' </div><button type="button" class="btn btn-danger myDeleteRef float-right col-sm-1 p-1" onClick="myFuncPlz(this, '+pNo+');">Delete</button></div><div class="container form-row"> <div class="form-group col-md-6"> <label for="inputPassword4">Qty</label> <input type="text" class="form-control qtyDival" name="qty'+$i+'" id = "qtyDival" required title="Only Numbers" placeholder="Quantity of product"> </div><div class="form-group col-md-6"> <label for="inputPassword4">Amount</label> <input type="text" class="form-control" name="amt'+$i+'" required title="Only Numbers" placeholder="Actual amount of product"> </div></div></div>';
    $i = $i + 1;
   d.appendChild(innerDiv);

   serverValPost.push(pNo);
   serverValPostName.push(pName);
   //alert(serverValPost);
}



//Get Value from Model and Pass to Order page
$(".use-address").click(function() {
   
var $row = $(this).closest("tr");    // Find the row
var pNo = $row.find(".getPNo").text(); // Find the text
var pName = $row.find(".getpname").text(); // Find the text
var pSpec = $row.find(".getpspec").text(); // Find the text
var pDesc = $row.find(".getpdesc").text(); // Find the text

$('#myModal').modal('toggle');

if(serverValPost.includes(pNo)){
   alert('Selected Product Already Exists');
}else{
   addOrderField(pNo, pName, pSpec, pDesc);
}



});

function myFuncPlz(item, pNo){
   //alert(pNo);
   serverValPost.splice( serverValPost.indexOf(pNo), 1 );
   //alert(serverValPost);
   // if (index > -1) {
   // array.splice(index, 1);
   // }
   $(item).parent().parent().parent().remove();

}

function validateMyForm(){

   if($("#addOrderField > div").length === 0){
      alert('Please Select a Product');
      return false;
   } else if($("#addOrderField > div").length >= 0) {
      var val = document.getElementById('myHiddenVal');
      var valName = document.getElementById('myHiddenValName');

      var totalDiv = $("#addOrderField > div").length;

    //   for(i = 0; i < totalDiv ; i++){
    //     //alert($("#MyDivID"+i+" :input"));

    //   }

      //alert(totalDiv);

      var myJSON = JSON.stringify(serverValPost);
      var myJSONName = JSON.stringify(serverValPostName);

      val.value = myJSON;
      valName.value = myJSONName;
      return true;
   }
}

</script>

<?php include '../include/footer.php'; ?>