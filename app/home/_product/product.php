<?php
include '../res/genral.php';
if(isset($_SESSION['ivcCardno']) || isset($_SESSION['ivcOrderGrpno'])){
    unset($_SESSION['ivcCardno']); 
    unset($_SESSION['ivcOrderGrpno']); 
  }
include '../include/header.php'; 
include '../include/menu.php';

if($_SERVER["REQUEST_METHOD"] == "POST"){

    $name = clean($_POST['name']);
    $amount = clean($_POST['amount']);
    $spec = clean($_POST['spec']);
    $desc = clean($_POST['desc']);
    $hsn_no = clean($_POST['hsn_no']);
    $type = clean($_POST['gstRadioBtn']);
    if(isset($_POST['gstrate'])){
        $gstrate = clean($_POST['gstrate']);
    } else {
        $gstrate = 0;

    }

    $json_request = array("name"=>"$name", "amount"=>"$amount", "spec"=>"$spec", "desc"=>"$desc", "hsn_no" => $hsn_no, "type" => $type, "gstrate" => $gstrate);
    $my_Response_Product = request_api(PRODUCT_API_PAGE ,"addNewProduct", $json_request); 

    if($my_Response_Product === 'true'){
        header('Location: product.php?mode=success');
    } else if($my_Response_Product === 'exist'){
        header('Location: product.php?mode=exist');
    } else{
        header('Location: product.php?mode=fail');
    }
    
} else if(isset($_GET['prodID']) && !empty($_GET['prodID'])){

    $prodID = clean(base64_decode($_GET['prodID']));
    
    $json_request = array("prodID"=>"$prodID");
    $my_prod_delete = request_api(PRODUCT_API_PAGE ,"deleteProductByID", $json_request); 
    
    if($my_prod_delete == 'true'){
        header('Location: product.php?mode=succDel');
    } else{
        header('Location: product.php?mode=failDel');
    }

}

if(isset($_GET['mode'])){

    if($_GET['mode'] == 'success'){
        echo showResponseSuccess('Product added Successfully.');
    } else if($_GET['mode'] == 'exist'){
        echo showResponseWarning('Product with same name already exists.');
    } else if($_GET['mode'] == 'fail'){
        echo showResponseFail('Unable to add product.');
    } else if($_GET['mode'] == 'succDel'){
        echo showResponseSuccess('Product Deleted Successfully!!');
    } else if($_GET['mode'] == 'failDel'){
        echo showResponseWarning('Can not delete, Order with the product name exists!!');
    }
}

?>
<!----------------------------------------------------------------------------------------------------------------------------------------------->
<div class = "container-fluid row mt-4">

  <div class = "col-sm-6">
    <button type="button" class="btn btn-info btn-block" id="addNewProduct" data-toggle="modal" data-target="#myModal" ><i class="fas fa-plus-square ">&nbsp;&nbsp;&nbsp;</i>Add New Product</button>
  </div>
  <div class = "col-sm-6">
    <button type="button" class="btn btn-success btn-block" id="addNewStock" ><i class="fas fa-boxes ">&nbsp;&nbsp;&nbsp;</i>Add Stocks</button>
  </div>

</div>

<!----------------------------------------------------------------------------------------------------------------------------------------------->

<div class = "row mt-4">

    <div class = "col-sm-12">
        <div class="container"> <input type="text" id="mySearchInput" onkeyup="myProductSearchFunction()" placeholder="Search for Orders.."></div>
    </div>

</div>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog" style="max-width: 1200px;">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Product</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      
        <form method="post" action = "">
        
            <div class="form-row">
                <div class="form-group col-md-6">
                <label for="inputEmail4">Name</label>
                <input type="text" class="form-control" name="name" required placeholder="Product Name">
            </div>
            <div class="form-group col-md-6"> 
                <label for="inputPassword4">Price</label>
                <input type="text" class="form-control" name = "amount" pattern = "^[0-9]*$" title = "Only Numbers" required placeholder="Enter Amount">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">HSN No.</label>
                    <input type="text" class="form-control" name="hsn_no" placeholder="Product Name">
                </div>

                <div class="form-group col-md-4"> 
                    <label for="inputAddress">Product Type</label>
                    <div class = "container row">
                        <div class = "col-sm-6" style="padding-left: 0px;">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                    <input type="radio" name = "gstRadioBtn" onclick = "radioGSTSelect()" value = "gst">
                                    </div>
                                </div>
                                <input type="text" class="form-control" disabled placeholder="GST">
                            </div>
                        </div>

                        <div class = "col-sm-6" style="padding-left: 0px;">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                    <input type="radio" name = "gstRadioBtn" onClick = "radioGSTDeSelect()" value = "nogst" checked>
                                    </div>
                                </div>
                                <input type="text" class="form-control" disabled placeholder="No-GST">
                            </div>
                        </div>
                    </div>  
                </div>

                <div class="form-group col-md-4">
                    <label for="inputEmail4">GST Rate</label>
                    <input type="text" class="form-control" disabled name="gstrate" id = "gstrate" placeholder="GST Rate">
                </div>


            </div>

            <div class = "row">

                <div class="form-group col-sm-6">
                    <label for="comment">Specification:</label>
                    <textarea class="form-control" name="spec" required rows="2" id="comment"></textarea>
                </div> 

                <div class="form-group col-sm-6">
                    <label for="comment">Description:</label>
                    <textarea class="form-control" rows="2" name = "desc" id="comment"></textarea>
                </div> 

            </div>



      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" >Add Product</button>
      </div>

      </form> 

      </div>

    </div>
  </div>
</div>
<!-------------------------------------------------- Model End ------------------------------------------------------------------->

<div class="table-responsive">
    <table id="myTable" class="table table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Hsn No</th>
        <th>Stock</th>
        <th>Specification</th>
        <th>Description</th>
        <th>Price</th>
        <th>Action</th>
      </tr>
    </thead>
 <tbody>

 <?php
    $json_request = array("None"=>"none");
    $my_Response_product = request_api(PRODUCT_API_PAGE ,"getAllProduct", $json_request); 

    foreach ($my_Response_product as $key => $value) {
        echo  '<tr class="table-primary"><td class="getProdID">'.$my_Response_product[$key]['prod_id'].'</td><td class="prod_name">'.$my_Response_product[$key]['prod_name'].'</td><td class="hsnno">'.$my_Response_product[$key]['hsn_no'].'</td><td class="stock">'.$my_Response_product[$key]['stock'].'</td><td class="getCNo userCardNo">'.$my_Response_product[$key]['spec'].'</td><td>'.$my_Response_product[$key]['prod_desc'].'</td><td>'.$my_Response_product[$key]['price'].'</td></td><td><button type="button" class="btn btn-danger productDelete">Delete</button></td></tr>';
    }
 ?>

</tbody></table></div>

<script>

$(".productDelete").click(function() {

    var $row = $(this).closest("tr");    // Find the row
    var prodID = $row.find(".getProdID").text(); // Find the text
    var prodID = window.btoa(prodID);
    window.location.href = "product.php?prodID="+prodID;

    //alert(prodID);

    // alert(GlobCardNoGST+" "+GlobGrpNoGST);
  
    //  GlobCardNoGST = window.btoa(GlobCardNoGST);
    //  GlobGrpNoGST = window.btoa(GlobGrpNoGST);
    //  billType =  window.btoa("gst");
    // var data = [GlobCardNoGST, GlobGrpNoGST, billType];
    // var myJSONData = JSON.stringify(data); 
    // var myJSONData = window.btoa(myJSONData);
    // //alert(myJSONData);
    // window.location.href = "invoiceSend.php?vals="+myJSONData;
  
  });

  function radioGSTSelect(){
    document.getElementById("gstrate").disabled = false;
  }

  function radioGSTDeSelect(){
    document.getElementById("gstrate").disabled = true;
  }

  document.getElementById("addNewStock").onclick = function () {
    location.href = "stockVendor.php";
};

</script>

<?php include '../include/footer.php'; ?>