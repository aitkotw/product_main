<?php
include '../res/genral.php';
include '../include/header.php'; 
include '../include/menu.php'; 

if($_SERVER["REQUEST_METHOD"] == "POST"){

    $name = clean($_POST['name']);
    $phone = clean($_POST['phone']);
    $gst = clean($_POST['gst']);
    $address = clean($_POST['address']);

    $json_request = array("name"=>"$name", "phone"=>"$phone", "gst"=>"$gst", "address"=>"$address" );
    $my_Vendor_add = request_api(PRODUCT_API_PAGE ,"addNewVendor", $json_request); 

    if($my_Vendor_add === 'true'){
        header('Location: stockVendor.php?mode=success');
    } else if($my_Vendor_add === 'exist'){
        header('Location: stockVendor.php?mode=exist');
    } else{
        header('Location: stockVendor.php?mode=fail');
    }

}

if(isset($_GET['mode'])){

    if($_GET['mode'] == 'success'){
        echo showResponseSuccess('Product added Successfully.');
    } else if($_GET['mode'] == 'exist'){
        echo showResponseWarning('Product with same name already exists.');
    } else if($_GET['mode'] == 'fail'){
        echo showResponseFail(' Unable to add product.');
    } 
}

?>

<div class = "row m-2">

    <div class = "col-sm-10">
        <h3 align="center" class="m-2"><i>Stock Management Area</i></h3>
    </div>

    <div class = "col-sm-2">
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Add Vendor</button>
    </div>


</div>

<!-- The Modal ---------------------------------------------------------------------------------------------------------------->
<div class="modal" id="myModal">
  <div class="modal-dialog" style="max-width: 700px;">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Vendor</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      
        <form method="post" action = "">
        
            <div class="form-row">
                <div class="form-group col-md-6">
                <label for="inputEmail4">Vendor Name</label>
                <input type="text" class="form-control" name="name" required placeholder="Name of the Vendor">
            </div>
            <div class="form-group col-md-6"> 
                <label for="inputPassword4">Contact No</label>
                <input type="text" class="form-control" name = "phone" pattern = "^[0-9]*$" title = "Only Numbers" required placeholder="Enter Vendor Contact">
                </div>
            </div>

            <div class="form-group p-0 col-md-12">
                <label for="inputEmail4">Vendor GST No.</label>
                <input type="text" class="form-control" name="gst" placeholder="Enter Vendor GST No.">
            </div>

            <div class="form-group">
                <label for="comment">Address:</label>
                <textarea class="form-control" rows="3" name = "address" id="comment"></textarea>
            </div> 

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" >Add Vendor</button>
      </div>

      </form> 

      </div>

    </div>
  </div>
</div>
<!--  Model End ------------------------------------------------------------------------------------------------------------------------------->


 <!-- The Modal -->
<div class="modal" id="myModal1">
  <div class="modal-dialog" style ="max-width: 900px;">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Product Purchase from Vendor </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" name="vendorID" id = "vendorID" value="" />
        <div class="form-row">
                <div class="form-group col-md-6">
                <label for="inputEmail4">Vendor Name</label>
                <input type="text" class="form-control" name="name" id = "getVname" required disabled placeholder="Name of the Vendor">
            </div>
            <div class="form-group col-md-6"> 
                <label for="inputPassword4">GST No.</label>
                <input type="text" class="form-control" name = "phone" id = "getGSTNo" title = "Only Numbers" required disabled placeholder="Enter Vendor Contact">
                </div>
            </div>

            <button type="button" class="btn btn-info m-2" data-toggle="modal" data-target="#myModal2">Select Product</button>

            

            
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!--  Model End ------------------------------------------------------------------------------------------------------------------------------->



<div class = "shadow-lg p-4 mb-4 bg-white">

    <div class = "row">
        <div class= "col-sm-6 vendorStockLight" id = "vendorStock">

            <p align="center" class = "m-auto ">Vendors</p>

        </div>
        
        <div class= "col-sm-6 vendorStockDark" id = "pastOrderStock">

            <p align="center" class = "m-auto ">Past Orders</p>

        </div>
    </div>

    <!-- First Panel Data start : Panel Vender-->
    <div id = "vendorStockDiv" class = "row mt-5">

        <div class="table-responsive">
            <table id="myTable" class="table table-hover">
            <thead class="thead-dark">
            <tr>
                <th>VendorID</th>
                <th>Name</th>
                <th>Contact</th>
                <th>GST No</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            <?php
                $json_request = array("None"=>"none");
                $my_Response_Vendor = request_api(PRODUCT_API_PAGE ,"getAllVendor", $json_request); 
                
                foreach ($my_Response_Vendor as $key => $value) {

                    echo  '<tr class="table-info"><td class="getVID">'.$my_Response_Vendor[$key]['vid'].'</td><td class="getVname">'.$my_Response_Vendor[$key]['vname'].'</td><td class="getCNo userCardNo">'.$my_Response_Vendor[$key]['vphone'].'</td><td class = "getGSTNo">'.$my_Response_Vendor[$key]['vgst'].'</td><td>'.$my_Response_Vendor[$key]['vaddress'].'</td></td><td><button type="button" class="btn btn-success addNewStockBtn">Add Stock</button></td></tr>';
                }
                echo '</tbody></table></div>';

            ?>
        
    </div>

    <!-- Second Panel Data start : Panel Past Stocks-->
    <div class = "row mt-5" id = "pastOrderStockDiv" style="display: none;">
        
        <div class="table-responsive">
        <table id="myTable" class="table table-hover">
        <thead class="thead-dark">
        <tr>
            <th>VoID</th>
            <th>vOGrp</th>
            <th>Name</th>
            <th>product</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <?php
            $json_request = array("None"=>"none");
            $my_Response_product = request_api(VENDOR_API_PAGE ,"getAllOrderVendor", $json_request); 
            
            foreach ($my_Response_product as $key => $value) {

                echo  '<tr class="table-primary"><td class="voID">'.$my_Response_product[$key]['voID'].'</td><td class="grpNo">'.$my_Response_product[$key]['vogrpNo'].'</td><td class="getCNo userCardNo">'.$my_Response_product[$key]['vName'].'</td><td>'.$my_Response_product[$key]['prodName'].'</td><td>'.$my_Response_product[$key]['amt'].'</td></td><td><button type="button" class="btn btn-info">Print</button></td></tr>';
            }
            echo '</tbody></table></div>';

        ?>

    </div>

   

</div>

<script>

$('#vendorStock').click(function() {
    $('#pastOrderStockDiv').hide();
    $('#vendorStockDiv').show();

    $('#vendorStock').addClass("vendorStockLight");
    $('#pastOrderStock').removeClass("vendorStockLight");
    $('#vendorStock').removeClass("vendorStockDark");
    $('#pastOrderStock').addClass("vendorStockDark");
});

$('#pastOrderStock').click(function() {
    $('#vendorStockDiv').hide();
    $('#pastOrderStockDiv').show();

    $('#vendorStock').addClass("vendorStockDark");
    $('#pastOrderStock').removeClass("vendorStockDark");
    $('#vendorStock').removeClass("vendorStockLight");
    $('#pastOrderStock').addClass("vendorStockLight");
});


$(".addNewStockBtn").click(function() {

var $row = $(this).closest("tr");    // Find the row
var vNo = $row.find(".getVID").text(); // Find the text  
var vName = $row.find(".getVname").text();
var vGSTNo = $row.find(".getGSTNo").text();
//alert(vNo);
var vNo = window.btoa(vNo);
var vName = window.btoa(vName);
var vGSTNo = window.btoa(vGSTNo);

window.location.href = "addStock.php?vendorid="+vNo+"&vname="+vName+"&vgst="+vGSTNo;


});

</script>

<?php include '../include/footer.php'; ?>