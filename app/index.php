<?php
include 'genral.php';
include 'constants.php';


if($_SERVER["REQUEST_METHOD"] == "POST"){
    $email = clean($_POST['email']);
    $password = clean($_POST['password']);
    $client_IP = get_client_ip();

    $browser_dtl = "";
    if(isset($_SERVER['HTTP_USER_AGENT'])){
        $browser_dtl = $_SERVER['HTTP_USER_AGENT'];
    }
    $request = json_encode(['name' => 'login_user', 'param' => ['email'=>$email, 'password'=>$password, 'client_ip' => $client_IP, 'browser_dtl' => $browser_dtl]]);

        //  Initiate curl
        $url = LOGIN_PAGE_API;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
            ));
        // Execute
        $result=curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        $myArray = json_decode($result, true);
        //var_dump($result);
        if (array_key_exists("response",$myArray)) {
            $myStatus = $myArray['response']['result'];
            //Set Cookie and store JWT Token for user reference
            $cookie_name = "user_auth_key";
            $cookie_value = $myStatus;
            $myDir = $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "home/", "", false, true ); // 86400 = 1 day
            //echo $myStatus; 
            $_SESSION["user_status"] = "success";
            header('Location: home/');

        } else if(array_key_exists("error",$myArray)) {
            $myStatus = $myArray['error']['message'];
            if($myStatus === 'Email Does Not Exist'){
                header('Location: index.php?mode=emailErr');
            } else if($myStatus === 'User Password is Invalid'){
                header('Location: index.php?mode=passErr');
            } else if ($myStatus === 'Connection Error'){
                header('Location: index.php?mode=connErr');
            } else if ($myStatus === 'unable to store logs'){
                header('Location: index.php?mode=logErr');
            }
        }else{
            $errors[] = 'Something Went wrong'; 
            die('Something Went Wrong');
        }
}
function clean($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>User LogIn</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/logreg.css">
</head>
<body>
<div class="container loginpanel shadow-lg">
    <div class="row login">
            <h2 class="mx-auto">LogIn</h2>
    </div>
    <div class="row">
        <div class="container px-5 pt-5 pb-3">
            <form name="loginForm" method="post" action="">
                <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
                </div>
                <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password" name="password" pattern=".{8,}" title="At least 8 or more characters" required>
                </div>
                <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="remember"> Remember me
                </label>
                </div>
                    <button type="submit" class="btn my-3 btn-primary container" >Submit</button>
            </form>
            
        </div>

        <?php

            if(isset($_GET['mode']) && !empty($_GET['mode'])){

                if($_GET['mode'] === 'emailErr'){
                    echo '<div class="alert container d-block alert-danger" id="error_div">
                            Entered email combination is incorrect. Please try again
                        </div>';    
                } else if($_GET['mode'] === 'passErr'){
                    echo '<div class="alert container d-block alert-danger" id="error_div">
                            Entered Password combination is incorrect. Please try again
                        </div>';    
                } else if($_GET['mode'] === 'connErr'){
                    echo '<div class="alert container d-block alert-danger" id="error_div">
                            Connection Error, Please Contact Administrator!!
                        </div>';    
                }else if($_GET['mode'] === 'logErr'){
                    echo '<div class="alert container d-block alert-danger" id="error_div">
                            Unable to record Logs, maybe Network Problem!!
                        </div>';    
                }
                          
            } 
            
            ?>

        
    </div>
</div>
</body>
</html> 